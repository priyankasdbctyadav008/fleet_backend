<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\OrganizationsController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AgencyController;
use App\Http\Controllers\AgencyVehiclesController;
use App\Http\Controllers\VehiclesCategoryMstController;
use App\Http\Controllers\VehiclesTypeMstController;
use App\Http\Controllers\VehiclesBrandMstController;
use App\Http\Controllers\SubscriptionPlanController;
use App\Http\Controllers\OrganizationBookingSettingController;
use App\Http\Controllers\AgencyTariffPackageController;
use App\Http\Controllers\CustomerQuotationController;
use App\Http\Controllers\CommonController;
use App\Http\Controllers\CustomerBookingsController;
use App\Http\Controllers\ModulesMstController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\CustomerCompanyController;
use App\Http\Controllers\ReservationSettingsController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
// Route::controller(AuthController::class)->group(function () {
//     Route::post('login', 'login');
//     Route::post('register', 'register');
//     Route::post('logout', 'logout');
//     Route::post('refresh', 'refresh');

// });

Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);
//Route::post('/logout', [AuthController::class, 'logout']);
Route::post('/forgot_password',[AuthController::class,'forgot']);
Route::post('/reset_password', [AuthController::class,'reset']);
Route::post('/resendOtp',[AuthController::class,'requestOtp']);
Route::any('/verifyOtp', [AuthController::class,'verifyOtp']);
Route::get('/list_currency', [AuthController::class,'currency_list']);

Route::post('/create_organization', [OrganizationsController::class,'create']);
Route::get('/list_organizations', [OrganizationsController::class,'list']);
Route::get('/organization/{id}', [OrganizationsController::class,'find']);
Route::post('/update_organization/{id}',[OrganizationsController::class,'update']);
Route::delete('/delete_organization/{id}',[OrganizationsController::class,'delete']);
Route::get('/get_user_organization/{userId}', [OrganizationsController::class,'getUserOrganization']);
Route::post('/update_user_org/{id}',[OrganizationsController::class,'update_user_org']);




Route::get('/users_list',[UserController::class,'list']);
Route::get('/user/{id}',[UserController::class,'find']);
Route::post('/update_user/{id}',[UserController::class,'update']);
Route::delete('/delete_user/{id}',[UserController::class,'delete']);
//Route::get('/user_by_email/{email}',[UserController::class,'getUserByEmail']);


Route::post('/create_agency', [AgencyController::class,'create']);
Route::get('/list_agency', [AgencyController::class,'list']);
Route::get('/agency/{id}', [AgencyController::class,'find']);
Route::get('/agency_by_organizationId/{id}', [AgencyController::class,'agency_by_organizationId']);
Route::post('/update_agency/{id}',[AgencyController::class,'update']);
Route::delete('/delete_agency/{id}',[AgencyController::class,'delete']);

Route::get('/agency_dropDown/{id}', [AgencyController::class,'find_dropDown']);
Route::post('/upload_files',[AgencyController::class,'upload_agency_files']);


Route::post('/create_agencyVehicle', [AgencyVehiclesController::class,'create']);
Route::get('/list_agencyVehicle', [AgencyVehiclesController::class,'list']);
Route::get('/agencyVehicle/{id}', [AgencyVehiclesController::class,'find']);
Route::get('/vehicles_by_organizationId', [AgencyVehiclesController::class,'vehicles_by_organizationId']);
Route::post('/update_agencyVhicle/{id}',[AgencyVehiclesController::class,'update']);
Route::delete('/delete_agencyVehicle/{id}',[AgencyVehiclesController::class,'delete']);
Route::post('/upload_vehicles_agency',[AgencyVehiclesController::class,'upload_vehicles_agency_files']);



Route::post('/create_vehiclesCategory', [VehiclesCategoryMstController::class,'create']);
Route::get('/list_vehiclesCategory', [VehiclesCategoryMstController::class,'list']);
Route::get('/vehiclesCategory/{id}', [VehiclesCategoryMstController::class,'find']);
Route::post('/update_vehiclesCategory/{id}',[VehiclesCategoryMstController::class,'update']);
Route::delete('/delete_vehiclesCategory/{id}',[VehiclesCategoryMstController::class,'delete']);
Route::get('/dropDown_vehiclesCategory', [VehiclesCategoryMstController::class,'list_dropDown']);

Route::post('/create_vehiclesType', [VehiclesTypeMstController::class,'create']);
Route::get('/list_vehiclesType', [VehiclesTypeMstController::class,'list']);
Route::get('/vehiclesType/{id}', [VehiclesTypeMstController::class,'find']);
Route::post('/update_vehiclesType/{id}',[VehiclesTypeMstController::class,'update']);
Route::delete('/delete_vehiclesType/{id}',[VehiclesTypeMstController::class,'delete']);
Route::get('/dropDown_vehiclesType', [VehiclesTypeMstController::class,'list_dropDown']);

Route::post('/create_vehiclesBrand', [VehiclesBrandMstController::class,'create']);
Route::get('/list_vehiclesBrand', [VehiclesBrandMstController::class,'list']);
Route::get('/vehiclesBrand/{id}', [VehiclesBrandMstController::class,'find']);
Route::post('/update_vehiclesBrand/{id}',[VehiclesBrandMstController::class,'update']);
Route::delete('/delete_vehiclesBrand/{id}',[VehiclesBrandMstController::class,'delete']);
Route::get('/dropDown_vehiclesBrand', [VehiclesBrandMstController::class,'list_dropDown']);

Route::post('/create_subscriptionPlan', [SubscriptionPlanController::class,'create']);
Route::get('/list_subscriptionPlan', [SubscriptionPlanController::class,'list']);
Route::get('/subscriptionPlan/{id}', [SubscriptionPlanController::class,'find']);
Route::post('/update_subscriptionPlan/{id}',[SubscriptionPlanController::class,'update']);
Route::delete('/delete_subscriptionPlan/{id}',[SubscriptionPlanController::class,'delete']);


Route::post('/create_organizationBookingSetting', [OrganizationBookingSettingController::class,'create']);
Route::get('/list_organizationBookingSetting', [OrganizationBookingSettingController::class,'list']);
Route::get('/organizationBookingSetting/{id}', [OrganizationBookingSettingController::class,'find']);
Route::post('/update_organizationBookingSetting/{id}',[OrganizationBookingSettingController::class,'update']);
Route::delete('/delete_organizationBookingSetting/{id}',[OrganizationBookingSettingController::class,'delete']);




Route::post('/create_agencyTariffPackage', [AgencyTariffPackageController::class,'create']);
Route::get('/list_agencyTariffPackage', [AgencyTariffPackageController::class,'list']);
Route::get('/agencyTariffPackage/{id}', [AgencyTariffPackageController::class,'find']);
Route::post('/update_agencyTariffPackage/{id}',[AgencyTariffPackageController::class,'update']);
Route::delete('/delete_agencyTariffPackage/{id}',[AgencyTariffPackageController::class,'delete']);


Route::post('/create_customerQuotation', [CustomerQuotationController::class,'create']);
Route::get('/list_customerQuotation', [CustomerQuotationController::class,'list']);
Route::get('/customerQuotation/{id}', [CustomerQuotationController::class,'find']);
Route::post('/update_customerQuotation/{id}',[CustomerQuotationController::class,'update']);
Route::delete('/delete_customerQuotation/{id}',[CustomerQuotationController::class,'delete']);

Route::post('/logout', [CommonController::class, 'logout']);
Route::get('/country', [AuthController::class,'country_list']);
Route::get('/states/{id}', [AuthController::class,'state_list']);
Route::get('/user_by_email/{email}',[AuthController::class,'getUserByEmail']);

Route::post('/create_customerBooking', [CustomerBookingsController::class,'create']);
Route::get('/list_customerBooking', [CustomerBookingsController::class,'list']);
Route::get('/customerBooking/{id}', [CustomerBookingsController::class,'find']);
Route::post('/update_customerBooking/{id}',[CustomerBookingsController::class,'update']);
Route::delete('/delete_customerBooking/{id}',[CustomerBookingsController::class,'delete']);


Route::post('/create_moduleMst', [ModulesMstController::class,'create']);
Route::get('/list_moduleMst', [ModulesMstController::class,'list']);
Route::get('/moduleMst/{id}', [ModulesMstController::class,'find']);
Route::post('/update_moduleMst/{id}',[ModulesMstController::class,'update']);
Route::delete('/delete_moduleMst/{id}',[ModulesMstController::class,'delete']);

Route::post('/create_customer', [CustomerController::class,'create']);
Route::get('/list_customers', [CustomerController::class,'list']);
Route::get('/customer/{id}', [CustomerController::class,'find']);
Route::post('/update_customer/{id}',[CustomerController::class,'update']);
Route::delete('/delete_customer/{id}',[CustomerController::class,'delete']);

Route::post('/create_company', [CustomerCompanyController::class,'create']);
Route::get('/list_company', [CustomerCompanyController::class,'list']);
Route::get('/company/{id}', [CustomerCompanyController::class,'find']);
Route::post('/update_company/{id}',[CustomerCompanyController::class,'update']);
Route::delete('/delete_company/{id}',[CustomerCompanyController::class,'delete']);


Route::post('/create_reservationSetting', [ReservationSettingsController::class,'create']);
Route::get('/list_reservationSetting', [ReservationSettingsController::class,'list']);
Route::get('/reservationSetting/{id}', [ReservationSettingsController::class,'find']);
Route::post('/update_reservationSetting/{id}',[ReservationSettingsController::class,'update']);
Route::delete('/delete_reservationSetting/{id}',[ReservationSettingsController::class,'delete']);
Route::get('/user_reservation_settings/{userId}', [ReservationSettingsController::class,'find_user_reservation_settings']);


