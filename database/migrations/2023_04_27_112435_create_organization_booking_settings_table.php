<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organization_booking_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigIncrements('organizationId')->unsigned();
            $table->integer('informAboutRentalBeforeDays');
            $table->integer('minRentalPeriodInDays');
            $table->integer('rentalReserveBeforeMonths');
            $table->integer('allowOverBooking');
            $table->integer('allowEditPriceManually');
            $table->integer('allowPastDateReservation');
            $table->integer('isAutoGenerateBookingRef');
            $table->integer('authorizeOutsideReservationArea');
            $table->bigInteger('createdBy')->unsigned()->default('1');
            $table->bigInteger('updatedBy')->unsigned()->default('1');
            $table->foreign('createdBy')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreign('updatedBy')
                ->references('id')
                ->on('users')
                ->onDelete('cascade'); 
            $table->foreign('organizationId')
                ->references('id')
                ->on('organizations')
                ->onDelete('cascade');     
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organization_booking_settings');
    }
};
