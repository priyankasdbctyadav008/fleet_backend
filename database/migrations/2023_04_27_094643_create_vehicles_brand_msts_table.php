<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles_brand_msts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('vehiclesTypeId');
            $table->string('name');	
            $table->text('description');
            $table->enum('status',['0', '1'])->default('1')->comment('active=1, inactive=0');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles_brand_msts');
    }
};
