<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('country_msts', function (Blueprint $table) {
           $table->bigIncrements('id')->nullable(false);
           $table->string('sortname')->nullable(false);
           $table->string('name')->nullable(false);
           $table->integer('phonecode')->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('country_msts');
    }
};
