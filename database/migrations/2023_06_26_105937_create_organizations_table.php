<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('userId');
            $table->string('LogoUrl');
            $table->string('name');
            $table->string('address');
            $table->string('city');
            $table->string('postalCode');
            $table->string('themeColor')->default(null);
            $table->json('prefferedCurrencies');
            $table->string('PageContentId_TnC')->default(null);
            $table->string('pageContent_PrivacyPolicy')->default(null);
            $table->integer('stateId');
            $table->integer('countryId');
            $table->enum('status',['0','1'])->default('1')->comment('active=1, inactive=0');
           // $table->integer('status')->default('1');
            $table->bigInteger('createdBy')->unsigned()->default('1');
            $table->bigInteger('updatedBy')->unsigned()->default('1');
            $table->foreign('createdBy')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreign('updatedBy')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
    }
};
