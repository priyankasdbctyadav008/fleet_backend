<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_quotations', function (Blueprint $table) {
           $table->bigIncrements('id');
            $table->integer('customerId');
            $table->timestamp('startDate')->useCurrent();	
            $table->timestamp('endDate')->useCurrent();	
            $table->string('source');
            $table->integer('vehicleCategoryId');
         //   $table->integer('vehicleTypeId')->nullable();
            $table->text('amount');		
            $table->integer('departureAgencyId');
            $table->integer('returnAgencyId');
            $table->timestamp('quotationExpiryDate')->useCurrent();
            $table->enum('status',['confirmed','pending','cancelled','deleted'])->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_quotations');
    }
};
