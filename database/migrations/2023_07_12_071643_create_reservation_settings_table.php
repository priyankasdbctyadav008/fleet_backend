<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('userId');
            $table->integer('informAboutRentalBeforeDays');
            $table->integer('rentalReserveBeforeMonths');
            $table->integer('minRentalPeriodInDays');
            
            $table->integer('allowOverBooking');
            $table->integer('authorizeOutsideReservationArea');
            $table->integer('allowPastDateReservation');
            $table->integer('isAutoGenerateBookingRef');

            $table->enum('allowOverBooking',['1', '2','3'])->default('3')->comment('Everyone=1, Administrators=2,No=3');
            $table->enum('authorizeOutsideReservationArea',['1', '2','3'])->default('3')->comment('Everyone=1, Administrators=2,No=3');
            $table->enum('allowPastDateReservation',['1', '2','3'])->default('3')->comment('Everyone=1, Administrators=2,No=3');
            $table->enum('isAutoGenerateBookingRef',['1', '2','3'])->default('3')->comment('Everyone=1, Administrators=2,No=3');
            $table->enum('status',['0', '1'])->default('1')->comment('active=1, inactive=0');
            //$table->integer('allowEditPriceManually');
            $table->bigInteger('createdBy')->unsigned()->default('1');
            $table->bigInteger('updatedBy')->unsigned()->default('1');
            $table->foreign('createdBy')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreign('updatedBy')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservation_settings');
    }
};
