<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
             $table->bigIncrements('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('phone');
            $table->string('name');
            $table->string('address');
            $table->string('city');
            $table->integer('postal_code');
            $table->string('preffered_currencies');
            $table->enum('role',['0','1'])->default('0');
            $table->integer('state_id');
            $table->integer('country_id');
            $table->integer('theme_color')->nullable();
            $table->string('pagecontent_TnC')->nullable();
            $table->string('pagecontent_privacypolicy')->nullable();
            $table->enum('created_by',['1','2','3'])->default('1')->comment('(Admin=1, Users=2,Customer=3')->nullable();
            $table->enum('updated_by',['1','2','3'])->default('1')->comment('(Admin=1, Users=2,Customer=3')->nullable();
            $table->rememberToken();
            $table->string('email_otp')->nullable();
            $table->enum('Isemail_verified',['0','1'])->default('0');
            $table->integer('status')->default('0')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
