<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('userId')->unsigned();
            $table->string('email');
            $table->string('phone');
            $table->string('address1');
            //$table->string('address2');
            $table->string('city');
            $table->integer('stateId');
            $table->integer('countryId');
            $table->integer('postalCode');
          //  $table->string('nationality');
            $table->string('licenseNo');
            $table->date('dob');
            $table->date('licenseCreationDate');
           // $table->date('licenseExpiryDate');
            $table->string('licenseImgFrontUrl');
            $table->string('licenseImgBackUrl');
          //  $table->enum('isProfessional',['true', 'false'])->default('false');
        //  $table->string('permit');
            $table->bigInteger('customerCompanyId')->unsigned();
            $table->enum('status',['0', '1'])->default('1')->comment('active=1, inactive=0');
            $table->bigInteger('createdBy')->unsigned()->default('1');
            $table->bigInteger('updatedBy')->unsigned()->default('1');
            $table->foreign('createdBy')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreign('updatedBy')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreign('UserId')
                ->references('id')
                ->on('users')
                ->onDelete('cascade'); 
            $table->foreign('customerCompanyId')
                ->references('id')
                ->on('customer_company')
                ->onDelete('cascade');         
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer');
    }
};
