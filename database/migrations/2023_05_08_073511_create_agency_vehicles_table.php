<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agency_vehicles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("vehicleCategoryId")->unsigned();;
            $table->integer("vehicleTypeId")->unsigned();;
            $table->integer("vehicleBrandId")->unsigned();;
            $table->string("model");
            $table->integer("noOfDoors");
            $table->integer("agencyId")->unsigned();;
            $table->string("yearOfRegistration");
            $table->json("motorization");
            $table->string("registrationNo");
            $table->json("gearBox");
            $table->integer("noOfSeats");
            $table->string("mileage");
            $table->json("vehicleFeatures");
            $table->string("ratePerDay");
            $table->integer("miniMumDay");
            $table->integer("maxiMumDay");
            $table->string("lastInspectionDate");
            $table->string("lastRevisionDate");
            $table->string("image_url");
            $table->enum('status',['0', '1'])->default('1')->comment('active=1, inactive=0');
            $table->bigInteger('createdBy')->unsigned()->default('1');
            $table->bigInteger('updatedBy')->unsigned()->default('1');
            $table->foreign('createdBy')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreign('updatedBy')
                ->references('id')
                ->on('users')
                ->onDelete('cascade'); 
            $table->foreign('agencyId')
                ->references('id')
                ->on('agencies')
                ->onDelete('cascade'); 
            $table->foreign('vehicleCategoryId')
                ->references('id')
                ->on('vehicles_category_msts')
                ->onDelete('cascade'); 
            $table->foreign('vehicleTypeId')
                ->references('id')
                ->on('vehicles_type_msts')
                ->onDelete('cascade'); 
            $table->foreign('vehicleBrandId')
                ->references('id')
                ->on('vehicles_brand_msts')
                ->onDelete('cascade');  
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agency_vehicles');
    }
};
