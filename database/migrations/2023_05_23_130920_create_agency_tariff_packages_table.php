<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agency_tariff_packages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('packageName');
            $table->enum('isStartMonday',['true','false'])->default('false');
            $table->enum('isStartTuesday',['true','false'])->default('false');
            $table->enum('isStartWednesday',['true','false'])->default('false');
            $table->enum('isStartThursday',['true','false'])->default('false');
            $table->enum('isStartFriday',['true','false'])->default('false');
            $table->enum('isStartSaturday',['true','false'])->default('false');
            $table->enum('isStartSunday',['true','false'])->default('false');	
            $table->integer('packageDurationInDays');
            $table->decimal('mileageIncluded');	
            $table->string('mileageUnit');	
            $table->enum('isUnlimitedMileage',['true','false'])->default('false');
            $table->text('amount');
            $table->bigint('vehiclesCategoryId')->unsigned();
            $table->bigint('vehicleTypeId')->unsigned();
            $table->enum('status',['Active','Inactive','Deleted'])->default('Active')->nullable();
            $table->bigInteger('createdBy')->unsigned()->default('1');
            $table->bigInteger('updatedBy')->unsigned()->default('1');
            $table->foreign('createdBy')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreign('updatedBy')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreign('vehiclesCategoryId')
                ->references('id')
                ->on('vehicles_category_msts')
                ->onDelete('cascade');
            $table->foreign('vehicleTypeId')
                ->references('id')
                ->on('vehicles_type_msts')
                ->onDelete('cascade');         
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agency_tariff_packages');
    }
};
