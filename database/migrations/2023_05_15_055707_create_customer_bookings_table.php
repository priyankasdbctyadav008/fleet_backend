<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_booking', function (Blueprint $table) {
            $table->bigIncrements('Id');
            $table->integer('UserId');
            $table->integer('AgencyVehicleId');
            $table->integer('VehicleCategoryId');
            $table->integer('VehicleTypeId');
            $table->integer('AgencyId');
            $table->integer('OrganizationId');
            $table->integer('AgencyTariffPackageId');
            $table->timestamp('BookedOn')->useCurrent();
            $table->timestamp('BookingstartDate')->useCurrent();
            $table->timestamp('BookingendDate')->useCurrent();
            $table->enum('Status',['Reserved', 'Confirmed', 'In progress', 'Completed'])->nullable();
            $table->text('Amount');
            $table->integer('CurrencyId');	
            $table->integer('Source');
            $table->integer('DepartureAgencyId');
            $table->string('DepartureLocation');	
            $table->integer('ReturnAgencyId');
            $table->bigInteger('createdBy')->unsigned()->default('1');
            $table->bigInteger('updatedBy')->unsigned()->default('1');
            $table->foreign('createdBy')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreign('updatedBy')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
             $table->foreign('agencyId')
                ->references('id')
                ->on('agencies')
                ->onDelete('cascade'); 
            $table->foreign('vehicleCategoryId')
                ->references('id')
                ->on('vehicles_category_msts')
                ->onDelete('cascade'); 
            $table->foreign('vehicleTypeId')
                ->references('id')
                ->on('vehicles_type_msts')
                ->onDelete('cascade'); 
            $table->foreign('AgencyVehicleId')
                ->references('id')
                ->on('agency_vehicles')
                ->onDelete('cascade'); 
            $table->foreign('OrganizationId')
                ->references('id')
                ->on('organizations')
                ->onDelete('cascade'); 
            $table->foreign('UserId')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');   
            $table->foreign('ReturnAgencyId')
                ->references('id')
                ->on('agencies')
                ->onDelete('cascade');   
            $table->foreign('DepartureAgencyId')
                ->references('id')
                ->on('agencies')
                ->onDelete('cascade');                     
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_bookings');
    }
};
