<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agencies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('noOfRegisteredVehicles');
            $table->string('agencyCode');
            $table->string('postalCode');
            $table->string('address1');
            $table->string('city');
            $table->string('paymentGatewaySecretKey')->nullable();;
            $table->string('paymentGatewayPublickey')->nullable();;
            $table->json('agencyAvailbility');
            $table->integer('countryId');
            $table->integer('stateId');
            $table->string('publicName');
            // $table->string('stripeSecretKey');
            // $table->string('stripePublickey');
            $table->boolean('ispaymentgatewayEnabled');
            $table->bigIncrements('organizationId');
            $table->enum('status',['0', '1'])->default('1')->comment('active=1, inactive=0');
            $table->bigInteger('createdBy')->unsigned()->default('1');
            $table->bigInteger('updatedBy')->unsigned()->default('1');
            $table->foreign('createdBy')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreign('updatedBy')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreign('organizationId')->references('id')->on('organizations')->onDelete('cascade');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agencies');
    }
};
