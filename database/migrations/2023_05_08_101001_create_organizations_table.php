<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
             $table->bigIncrements("id");
            $table->string("name");
            $table->string("address");
            $table->string("city");
            $table->string("postalCode");
            $table->string("themeColor");
            $table->string("pageContent_TnC");
            $table->string("pageContent_PrivacyPolicy");
            $table->string("prefferedCurrencies");
            $table->enum('createdBy',['1','2','3'])->default('1')->comment('(Admin=1, Users=2,Customer=3')->nullable();
            $table->enum('updatedBy',['1','2','3'])->default('1')->comment('(Admin=1, Users=2,Customer=3')->nullable();
            $table->string("firstName");
            $table->string("lastName");
            $table->string("phone");
            $table->string("email");
            $table->string("password");
            $table->integer("countryId");
            $table->integer("stateId");
            $table->enum('status',['0', '1'])->default('1')->comment('active=1, inactive=0');
            $table->timestamp("created_at")->useCurrent();
            $table->timestamp("updated_at")->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
    }
};
