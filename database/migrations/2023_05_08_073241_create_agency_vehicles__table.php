<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agency_vehicles_', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("vehicleCategoryId");
            $table->integer("vehicleTypeId");
            $table->integer("vehicleBrandId");
            $table->string("model");
            $table->integer("noOfDoors");
            $table->integer("agencyId");
            $table->string("yearOfRegistration");
            $table->json("motorization");
            $table->string("registrationNo");
            $table->json("gearBox");
            $table->integer("noOfSeats");
            $table->string("mileage");
            $table->json("vehicleFeatures");
            $table->string("ratePerDay");
            $table->integer("miniMumDay");
            $table->integer("maxiMumDay");
            $table->string("lastInspectionDate");
            $table->string("lastRevisionDate");
            $table->string("image_url1");
            $table->string("image_url2");
            $table->string("image_url3");
            $table->string("image_url4");
            $table->string("image_url5");
            $table->string("image_url6");
            $table->enum('status',['0', '1'])->default('1')->comment('active=1, inactive=0');
            $table->enum('createdBy',['1','2','3'])->default('1')->comment('(Admin=1, Users=2,Customer=3');
            $table->enum('updatedBy',['1','2','3'])->default('1')->comment('(Admin=1, Users=2,Customer=3');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agency_vehicles_');
    }
};
