Differences between die() and exit() Functions:
 die():-
1)The die() method is used to throw an exception 	
2)The die() function is used to print the message.	
3)This method is from die() in Perl.	
  exit():- 
1)The exit() method is only used to   exit the process.
2)The exit() method exits the script or it may be used to print alternate messages.
3)This method is from exit() in C.

php 8.2


error type:
warning 
notice
fatal

pop()
shift()
slice()

The break statement "jumps out" of a loop.

The continue statement "jumps over" one iteration in the loop.

*******************************************************************

$autoload['libraries'] = array('database','session','pagination','form_validation','Template', 'format','Firebase','Curl', 'Sms');


$autoload['helper'] = array('url','form', 'jwt', 'authorization', 'common');

********

attribute :- 
key attribute
composite attribute
multivalue attribute
derived attribute

relation :-
unary,binary,n ary

language 
structure 
non structure
objectbased

normalization:-
Database normalization is the process of organizing the attributes of the database to reduce or eliminate data redundancy (having the same data but at different places). 

join: -A JOIN clause is used to combine rows from two or more tables, based on a related column between them.

INNER JOIN: Returns records that have matching values in both tables
LEFT JOIN: Returns all records from the left table, and the matched records from the right table
RIGHT JOIN: Returns all records from the right table, and the matched records from the left table
CROSS JOIN: Returns all records from both tables
self join 

The UNION operator is used to combine the result-set of two or more SELECT statements.

CREATE PROCEDURE SelectAllCustomers
AS
SELECT * FROM Customers
GO;

EXEC SelectAllCustomers;


Super Key
A super key is a set of one or more than one key that can be used to identify a record uniquely in a table. Example: Primary key, Unique key, Alternate key are a subset of Super Keys.

The super key in SQL can be defined as a set of one or more than one key that can be used to identify a certain type of data in a table. This key includes only those fields which include a unique value.

Candidate Key
A Candidate Key is a set of one or more fields/columns that can identify a record uniquely in a table. There can be multiple Candidate Keys in one table. Each Candidate Key can work as a Primary Key.


A Candidate key is a subset of Super keys that we have discussed previously and it is lacking any unnecessary attributes that are not important to be used uniquely identify tuples of a table. The value for the Candidate key is always unique and non-null for all the tuples types. One thing to be remembered is that every table has to have at least one Candidate key, but there can be more than one candidate key can be there in a table.

Example: In the below diagram ID, RollNo and EnrollNo are Candidate Keys since all these three fields can work as Primary Key.

Primary Key
A primary key is a set of one or more fields/columns of a table that uniquely identify a record in a database table. It can not accept null, duplicate values. Only one Candidate Key can be Primary Key.

Out of all the candidate keys that can be possible or created for the specific table, there should be only one key that can be used to retrieve unique tuples from the table. This candidate key is called the "Primary Key". It is a thumb rule that there can be one Primary key that should be there for a table. Depending on how the candidate key is constructed, the primary key possibly can be a single attribute or a group of attributes. But the crucial point to remember is that the created Primary key should be unique and without the NULL attribute.

Alternate key
An Alternate key is a key that can work as a primary key. Basically, it is a candidate key that currently is not a primary key.

In other words, the alternate key is a column or collection of columns in a table that can uniquely identify each row in that table. Every table of the database table can have multiple options for a primary key to be configured but out of them, only one column can be set as the primary key. All the keys which are not primary keys are called the alternate keys of that table.

Example: In the below diagram RollNo and EnrollNo become Alternate Keys when we define ID as the Primary Key.


Composite/Compound Key
A composite Key is a combination of more than one field/column of a table. It can be a Candidate key, Primary key.

Unique Key
A unique key is a set of one or more fields/columns of a table that uniquely identify a record in a database table. It is like a Primary key but it can accept only one null value and it can not have duplicate values. For more help refer to the article Difference between primary key and unique key.

Foreign Key
Foreign Key is a field in a database table that is the Primary key in another table. It can accept multiple nulls and duplicate values. For more help refer to the article Difference between primary key and foreign key.

Example: We can have a DeptID column in the Employee table which is pointing to a DeptID column in a department table where it is a primary key.

**********
decorator:- 
runtime time out excuite
@NgModule => to define module
@Componet => to define Componet
@injectebale =>to define service
@input @output=> child to parent data pass

class decorator: @ngModule, @ngComponet
property decorator : @input, @output
method decorator: @hostlistener
perameter deocrator: @inject


directives:- 
change behavior and appireance of DOM
componet directive :- componet
structure directive :- *ngIf,*ngFor (index, first, last, even, odd),*ngSwitch
attribute directive :-[ngStyle]="{'color':'red'}", [ngClass]="" :-( array string object and componet)

Data Binding:-
1) One Way databinding  
   i) componet to view
       a)interpolation binding: {{}}
       b)property binding: [innerHTML]="dynamic value",[scr]="dynamic value",[value]
       c)style binding:  [ngStyle] ="dynamic value"
       d)class binding : [ngClass] ="dynamic value"
       e)attribute binding :[att.colspan]="dynamic value"
   ii) view to componet
       a)event binding
2) two way databinding :- [(ngModule)]

Pipe :-
1) builtIn pipe : 
   i) perameterrized Pipe {{dob|date :"dd/mm/yyyy"}}, {{salarry|currency :"USD" :true}}
   ii) chaning Pipe

  pipe : lowercase ,uppercase,date,currency,decimal,percent, json
2) custom pipe :


Routing: Navigate from one componet to other componet

router outlet :-router outlet is dynamic componet that the route use to display view based on router navigate 

redirectTo:"path"
loadchildren:''
child:''

serveice is bloc of code witch is creste to specific task.
code reusablity facility

form type: 
  template driven
   <form #testForm="ngForm" (ngSubmit) ="testmethod(testForm)" >
    testmethod(testForm :any){
      testForm.control.username.value
    }
  reactive form
  <form [formGroup]="formnane" (ngSubmit) ="testmethod(formnane)">
    testmethod(formnane:any){
        formnane.controls.name.value
    }
    formControlName='name'

  loding type: 
  eager loding
  pre loading
  lazy loading :loadchildren PROPERTY
  
  guard type:-
  canActive
  canDeactive
  canActiveChild
  Resolve
  load

  @viewChild :access child method using componet
  @viewChild :access child method using directive
  @viewChild :access child method using template
  
  
  view provider
 
  observable stream create : 
  user Inpute (click event) fromEvent('','click').subscribe(res=>{

  });
  http request
  array
  object


  handle observable :- 
  data
  error
  completion

  *nterval/timer
  interval(1000)
  timer(delay,timer)

  *of/ from
  of(1,2,3)
  from(array()), from(promis),from(string)

  observal to array
  toArray()

  create custom observable
  const demo  =observable.create(observer=>{
    observer.next('test');
    observer.error();
    oberver.complete();
  })
  demo.subscribe(res=>{
    
  })
  **************************************
restapi :- set of rules that define how to application or device connect and comunicate with each other 
stateless : state

trackBy:-


*****************************************************************************
Data structure is a technic to organize data and to  reduce the complexcity to searching ,sorting,insertion,deleteion, traversing and Reversing

TYPE OF DATA STRUC:

1Prinitive :string, double, float,integer

2NonPrimitive: 
array: pagination of book 
Selection Sort
Bubble Sort
Insertion Sort
Merge Sort
Quick Sort
Heap Sort

Linear Search
Sentinel Linear Search
Binary Search
Meta Binary Search | One-Sided Binary Search
Ternary Search
Jump Search
Interpolation Search
Exponential Search
Fibonacci Search
The Ubiquitous Binary Search

linckelist:song back pre , image viewer
stack: browser histroy, call log ,
quee :-message quee, job sheduling
graph : networking, facebook
tree: In real life, tree data structure helps in Game Development. 


***********************************
Try and Catch Block: The try statement allows you to check whether a specific block of code contains an error or not. The catch statement allows you to display the error if any are found in the try block.

*******************************************

Finding HTML Elements
document.getElementById(id)	
document.getElementsByTagName(name)	Fi
document.getElementsByClassName(name)
Finding HTML Elements by CSS Selectors : querySelectorAll()
Finding HTML Elements by HTML Object Collections : document.forms["frm1"]; document.body

Changing HTML Elements
element.innerHTML =  new html content	Change the inner HTML of an element
element.attribute = new value	Change the attribute value of an HTML element
element.style.property = new style	Change the style of an HTML element
Method	Description
element.setAttribute(attribute, value)

Adding and Deleting Elements
document.createElement(element)	
document.removeChild(element)
document.appendChild(element)	
document.replaceChild(new, old)	
document.write(text)


Navigating Between Nodes:
parentNode
childNodes[nodenumber]
firstChild
lastChild
nextSibling
previousSibling