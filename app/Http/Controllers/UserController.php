<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
     public function __construct()
    {
       // $this->middleware('auth:api');
        if(!auth()->check()){
            $data = response()->json([
                "success" => false,
                "message" => "Authentication Fail.",
            ])->getContent();; 
            header("Content-type:application/json");
            echo $data;
            die();
        }else{
            $user_role = Auth::user()->role;
            if($user_role !='1'){
                $data = response()->json([
                    "success" => false,
                    "message" => "UNAUTHORIZED.",
                ]); 
                header("Content-type:application/json");
                echo $data;
                return $data;
                die();
            }
        }
    }
    // public function create(Request $request){
    //     $input = $request->all();
        
    //     $validator = Validator::make($input, [
    //         "name"=> "required|string|max:255",
    //         "address"=> "required|string|max:255",
    //         "city"=> "required|string|max:255",
    //         "postalCode" => "required|string|max:255",
    //         "themeColor" => "required|string|max:255",
    //         "pageContent_TnC" => "required|string|max:255",
    //         "pageContent_PrivacyPolicy" => "required|string|max:255",
    //         "prefferedCurrencies" => "required|string|max:255",
    //         "createdBy" => "required|string|max:255",
    //         "updatedBy" => "required|string|max:255",
    //         "firstName" => "required|string|max:255",
    //         "lastName" => "required|string|max:255",
    //         "phone" => "required|string|max:255",
    //         "email" => "required|string|max:255",
    //         "password" => "required|string|max:255",
    //         "countryId" => "required|string|max:255",
    //         "stateId" => "required|string|max:255",
    //     ]);
    //     if($validator->fails()){
    //         return response()->json([
    //             "success" => false,
    //             "message" => $validator->errors(),
    //         ]); 
    //        // return $this->sendError('Validation Error.', $validator->errors());       
    //     }
    //     $user = User::create($input);
    //     return response()->json([
    //         "success" => true,
    //         "message" => "Organizations created successfully.",
    //         "data" => $user
    //     ]);
    // }
   
    /**
    * @OA\Get(
    * path="/api/users_list",
    * operationId="Users List",
    * tags={"Users"},
    * summary="Users List",
    * description="Users list here",
    *      @OA\Response(
    *          response=201,
    *          description="Users List.",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=200,
    *          description="Users List.",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=422,
    *          description="Unprocessable Entity",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    *     security={{"bearer_token":{}}} 
    * )
    */
    public function list(){
        try{
            // $user = User::all();
            // get Business Owner - Users list.
            $user=User::where('role', '2')->get();
            if(count($user)>0){
                return response()->json([
                    "success" => true,
                    "message" => "Users List.",
                    "data" => $user,
                    "total_count"=>count($user),
                ]); 
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }     
    } 

    /**
     * @OA\Get(
     *      path="/api/user/{id}",
     *      operationId="Users Find",
     *      tags={"Users"},
     *      summary="Get Users",
     *      description="Returns Users data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Users Id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Users Data",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Users Data"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={{"bearer_token":{}}} 
     * )
     */
    public function find($id){
        try{
            $user = User::find($id);
            $user = compact('user');
            if(!empty($user['user'])){
                return response()->json([
                    "success" => true,
                    "message" => "User Data",
                    "data" => $user,
                    "total_count"=>count($user),
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        } 
    }
    /**
        * @OA\Post(
        * path="/api/update_user/{id}",
        * operationId="update User",
        * tags={"Users"},
        * summary="Users Update",
        * description="User update here",
        *      @OA\Parameter(
     *          name="id",
     *          description="User Id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
        *     @OA\RequestBody(
        *         @OA\JsonContent(),
        *         @OA\MediaType(
        *            mediaType="multipart/form-data",
        *            @OA\Schema(
        *               type="object",
        *                      required={"first_name","last_name","phone","name","address","city","postal_code","preffered_currencies","state_id","country_id"},
                        *@OA\Property(property="first_name", type="string"),
                        *@OA\Property(property="last_name", type="string"),
                        *@OA\Property(property="phone", type="string"),
                        *@OA\Property(property="name", type="string"),
                        *@OA\Property(property="address", type="string"),
                        *@OA\Property(property="city", type="string"),
                        *@OA\Property(property="postal_code", type="string"),
                        *@OA\Property(property="preffered_currencies", type="json"),
                        *@OA\Property(property="state_id", type="integer"),
                        *@OA\Property(property="country_id", type="integer"),
                        *@OA\Property(property="theme_color", type="integer"),
                        *@OA\Property(property="pagecontent_TnC", type="string"),
                        *@OA\Property(property="pagecontent_privacypolicy", type="string"),
                        *@OA\Property(property="created_by", type="integer"),
                        *@OA\Property(property="updated_by", type="integer"),
                        *@OA\Property(property="status", type="integer")
        *            ),
        *        ),
        *    ),
        *      @OA\Response(
        *          response=201,
        *          description="User data has been updated successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=200,
        *          description="User data has been updated successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=422,
        *          description="Unprocessable Entity",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        *      security={{"bearer_token":{}}} 
        * )
        */
    public function update(Request $request, $id)  
    {   
        try{
            $request = $request->all();
            $validator=Validator::make($request, [
                'first_name'=>'required|string|max:255',
                'last_name'=>'required|string|max:255',
                //'email'=>'required|string|max:255',
            // 'email_verified_at'=>'required|string|max:255',
                //'password'=>'required|string|max:255',
                'phone'=>'required|string|max:255',
                'name'=>'required|string|max:255',
                'address'=>'required|string|max:255',
                'city'=>'required|string|max:255',
                'postal_code'=>'required|integer',
                'preffered_currencies'=>'required|string|max:255',
                //'role'=>'required|string|max:255',
                'state_id'=>'required|integer',
                'country_id'=>'required|integer|max:255',
                'theme_color'=>'integer|nullable',
                'pagecontent_TnC'=>'string|max:255|nullable',
                'pagecontent_privacypolicy'=>'string|nullable',
                'created_by'=>'integer|nullable',
                'updated_by'=>'integer|nullable',
                'status'=>'integer|nullable',
            ]);
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" => $validator->errors(),
                ]); 
                //return $this->sendError('Validation Error.', $validator->errors());       
            }
            $user=User::find($id);
        
            if(!empty($user)){
                $user->update($request);
                return response()->json([
                    "success" => true,
                    "message" => "User data has been updated successfully.",
                    "data" => $user,
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        
            // $organizations = Organizations::find();
            // $organizations->name=  $request->get('name');
            // $organizations->address=  $request->get('address');
            // $organizations->city=  $request->get('city');
            // $organizations->postalCode=  $request->get('postalCode');
            // $organizations->themeColor=  $request->get('themeColor');
            // $organizations->pageContent_TnC=  $request->get('pageContent_TnC');
            // $organizations->pageContent_PrivacyPolicy=  $request->get('');
            // $organizations->prefferedCurrencies=  $request->get('');
            // $organizations->createdBy=  $request->get('createdBy');
            // $organizations->updatedBy=  $request->get('updatedBy');
            // $organizations->firstName=  $request->get('firstName');
            // $organizations->lastName=  $request->get('lastName');
            // $organizations->phone=  $request->get('phone');
            // $organizations->email=  $request->get('email');
            // // $organizations->password=  $request->get('password');
            // $organizations->countryId=  $request->get('countryId');
            // $organizations->stateId=  $request->get('stateId');
            // $organizations->save();  
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }     
    }  

       /**
     * @OA\Delete(
     *      path="/api/delete_user/{id}",
     *      operationId="Delete User",
     *      tags={"Users"},
     *      summary="Delete User",
     *      description="Delete User",
     *      @OA\Parameter(
     *          name="id",
     *          description="User Id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="User data has been deleted successfully.",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={{"bearer_token":{}}} 
     * )
     */ 
    public function delete($id)  
    {   try{
            $user=User::find($id);
            if(!empty($user)){
                $user->status='0';
                $user->save();
                return response()->json([
                    "success" => true,
                    "message" => "User data has been deleted successfully.",
                    "data" => $user,
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }     
    }

    public function getUserByEmail($email){
        try{
            $user=User::where('email', $email)->get();
            if(count($user)>0){
                return response()->json([
                    "success" => true,
                    "message" => "User data",
                    "data" => $user,
                    "total_count"=>count($user),
                ]); 
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }     
    }
}
