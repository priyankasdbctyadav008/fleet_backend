<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\VehiclesTypeMst;
use Validator;
use Illuminate\Support\Facades\Auth;


class VehiclesTypeMstController extends Controller
{
     public function __construct()
    {
        //$this->middleware('auth:api');
        if(!auth()->check()){
            $data = response()->json([
                "success" => false,
                "message" => "Authentication Fail.",
            ])->getContent();; 
            header("Content-type:application/json");
            echo $data;
            die();
        }else{
            $user_role = Auth::user()->role;
            if($user_role !='1' && $user_role !='2'){
                $data = response()->json([
                    "success" => false,
                    "message" => "UNAUTHORIZED.",
                ]); 
                header("Content-type:application/json");
                echo $data;
                return $data;
                die();
            }
        }
    }
     /**
        * @OA\Post(
        * path="/api/create_vehiclesType",
        * operationId="Create Vehicles Type",
        * tags={"Vehicles-Type"},
        * summary="Create Vehicles Type",
        * description="Create vehicles type here",
        *     @OA\RequestBody(
        *         @OA\JsonContent(
        *               required={"name","vehiclesCategoryId","status"},
                        *@OA\Property(property="name", type="string"),
                        *@OA\Property(property="description", type="string"),
                        *@OA\Property(property="vehiclesCategoryId", type="string"),
                        *@OA\Property(property="status", type="integer"),
        *),
        *         @OA\MediaType(
        *            mediaType="multipart/form-data",
        *            @OA\Schema(
        *               type="object",
        *               required={"name","vehiclesCategoryId","status"},
                        *@OA\Property(property="name", type="string"),
                        *@OA\Property(property="description", type="string"),
                        *@OA\Property(property="vehiclesCategoryId", type="string"),
                        *@OA\Property(property="status", type="integer"),
                        
        *            ),
        *        ),
        *    ),
        *      @OA\Response(
        *          response=201,
        *          description="Vehicle type created successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=200,
        *          description="Vehicle type created successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=422,
        *          description="Unprocessable Entity",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        *      security={{"bearer_token":{}}} 
        * )
        */
    public function create(Request $request){
        try {
            $input = $request->all();
            $validator = Validator::make($input, [
                "name"=> "required|string|max:255",
                "description"=> "string|max:255",
                "vehiclesCategoryId"=> "required|integer|max:255",
                "status"=> "required|integer|max:255",
            ]);
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" => $validator->errors(),
                ]); 
            }
            $vehiclesTypeMst = VehiclesTypeMst::create($input);
            return response()->json([
                "success" => true,
                "message" => "Vehicle type created successfully.",
                "data" => $vehiclesTypeMst
            ]);
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
    
        }
    }
     /**
    * @OA\Get(
    * path="/api/list_vehiclesType",
    * operationId="Vehicles Type List",
    * tags={"Vehicles-Type"},
    * summary="Vehicles Type List",
    * description="Vehicles type list here",
     * @OA\Parameter(
     *          name="page",
     *          description="Page no.",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *  @OA\Parameter(
     *          name="limit",
     *          description="Limit",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *  @OA\Parameter(
     *          name="sortBy",
     *          description="SortBy",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *  @OA\Parameter(
     *          name="orderBy",
     *          description="OrderBy",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *   @OA\Parameter(
     *          name="searchKey",
     *          description="SearchKey",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *   @OA\Parameter(
     *          name="searchVal",
     *          description="SearchVal",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     
    *      @OA\Response(
    *          response=201,
    *          description="Vehicle Type List.",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=200,
    *          description="Vehicle Type List.",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=422,
    *          description="Unprocessable Entity",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    *     security={{"bearer_token":{}}} 
    * )
    */
    public function list(Request $request){
        try{
            $input = $request->all();
        
            $query = VehiclesTypeMst::query();
            if(isset($input['searchKey']) && isset($input['searchVal']) && !empty($input['searchKey']) && !empty($input['searchVal'])){
                $query->where($request->input('searchKey'), 'like', '%' . $request->input('searchVal') . '%');
            }
        
            if (isset($input['sortBy']) && !empty($input['sortBy'])) {
                $query->orderBy($input['sortBy'], $input['orderBy']);
            }
            if(isset($input['page']) && !empty($input['page']) && isset($input['limit']) && !empty($input['limit'])){
                $count=$query->count();
                $input['page']= (floor($count/$input['limit']))-1;
                $query->offset($input['page'])->limit($input['limit'])->get();
            }
            $vehiclesTypeMst= $query->get();
            //print_r($vehiclesCategoryMst); die();
            if(!$vehiclesTypeMst->isEmpty()){
                return response()->json([
                    "success" => true,
                    "message" => "Vehicle type List.",
                    "data" => $vehiclesTypeMst,
                    "total_count"=>count($vehiclesTypeMst),
                ]); 
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
            return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }     
    } 
     /**
     * @OA\Get(
     *      path="/api/vehiclesType/{id}",
     *      operationId="vehicles Type Find",
     *      tags={"Vehicles-Type"},
     *      summary="Get Vehicles Type",
     *      description="Returns vehicles type data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Vehicles Type Id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Vehicle Type Data",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Vehicle Type Data"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={{"bearer_token":{}}} 
     * )
     */
    public function find($id){
        try{
            $vehiclesTypeMst = VehiclesTypeMst::find($id);
            $vehiclesTypeMst = compact('vehiclesTypeMst');
            if(!empty($vehiclesTypeMst['vehiclesTypeMst'])){
                return response()->json([
                    "success" => true,
                    "message" => "Vehicle type Data",
                    "data" => $vehiclesTypeMst,
                    "total_count"=>count($vehiclesTypeMst),
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }     
    }
      /**
        * @OA\Post(
        * path="/api/update_vehiclesType/{id}",
        * operationId="Update Vehicles Type",
        * tags={"Vehicles-Type"},
        * summary="Update Vehicles Type",
        * description="Update vehicles type here",
        *      @OA\Parameter(
        *          name="id",
        *          description="Vehicle Type Id",
        *          required=true,
        *          in="path",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *     @OA\RequestBody(
        *         @OA\JsonContent(
        *               required={"name","vehiclesCategoryId","status"},
                        *@OA\Property(property="name", type="string"),
                        *@OA\Property(property="description", type="string"),
                        *@OA\Property(property="vehiclesCategoryId", type="string"),
                        *@OA\Property(property="status", type="integer"),
        *),
        *         @OA\MediaType(
        *            mediaType="multipart/form-data",
        *            @OA\Schema(
        *               type="object",
        *               required={"name","vehiclesCategoryId","status"},
                        *@OA\Property(property="name", type="string"),
                        *@OA\Property(property="description", type="string"),
                        *@OA\Property(property="vehiclesCategoryId", type="string"),
                        *@OA\Property(property="status", type="integer"),
                        
        *            ),
        *        ),
        *    ),
        *      @OA\Response(
        *          response=201,
        *          description="Vehicle type Update successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=200,
        *          description="Vehicle type Update successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=422,
        *          description="Unprocessable Entity",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        *      security={{"bearer_token":{}}} 
        * )
        */
    public function update(Request $request, $id)  
    {   
        try {
            $request = $request->all();
            $validator=Validator::make($request, [
                "name"=> "required|string|max:255",
                "description"=> "string|max:255",
                "vehiclesCategoryId"=> "required|integer|max:255",
                "status"=> "required|integer|max:255",
            ]);
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" => $validator->errors(),
                ]); 
            }
            $vehiclesTypeMst= VehiclesTypeMst::find($id);
        
            if(!empty($vehiclesTypeMst)){
                $vehiclesTypeMst->update($request);
                return response()->json([
                    "success" => true,
                    "message" => "Vehicle type data has been updated successfully.",
                    "data" => $vehiclesTypeMst,
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }
    }  

     /**
     * @OA\Delete(
     *      path="/api/delete_vehiclesType/{id}",
     *      operationId="Delete VehiclesType",
     *      tags={"Vehicles-Type"},
     *      summary="Delete vehicles Type",
     *      description="Delete vehicles Type",
     *      @OA\Parameter(
     *          name="id",
     *          description="VehiclesType Id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Vehicles Type data has been deleted successfully.",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={{"bearer_token":{}}} 
     * )
     */ 
    public function delete($id)  
    {   try {
            $vehiclesTypeMst=VehiclesTypeMst::find($id);
            if(!empty($vehiclesTypeMst)){
            // $vehiclesTypeMst->status='0';
                $vehiclesTypeMst->delete();
                return response()->json([
                    "success" => true,
                    "message" => "Vehicle type data has been deleted successfully.",
                    "data" => $vehiclesTypeMst,
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
    
        }
            
    }
    
    public function list_dropDown(Request $request){
        try{
            $input = $request->all();
        
            $query = VehiclesTypeMst::query();
            $query->where('status', '1');
            $vehiclesTypeMst= $query->get();
            
            if(!$vehiclesTypeMst->isEmpty()){
                return response()->json([
                    "success" => true,
                    "message" => "Vehicle type List.",
                    "data" => $vehiclesTypeMst,
                    "total_count"=>count($vehiclesTypeMst),
                ]); 
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }     
    } 
}
