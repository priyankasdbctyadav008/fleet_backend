<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\Customer;
use Validator;
use Illuminate\Support\Facades\Auth;

class CustomerController extends Controller
{   
    public function __construct()
    {     
        //$this->middleware('auth:api');
        if(!auth()->check()){
                 $data = response()->json([
                    "success" => false,
                    "message" => "Authentication Fail.",
                ])->getContent();; 
                header("Content-type:application/json");
                echo $data;
                die();
        }else{
            $user_role = Auth::user()->role;
            if($user_role !='1' && $user_role !='2'){
                $data = response()->json([
                    "success" => false,
                    "message" => "UNAUTHORIZED.",
                ]); 
                header("Content-type:application/json");
                echo $data;
                return $data;
                die();
            }
        }
    }
    public function create(Request $request){
        try {
            $input = $request->all();
           
            //$validated = $request->validated();
            $validator = Validator::make($input,[ 
                'firstName'=>"required|string|max:255",
                'lastName'=>"required|string|max:255",
                'email'=> "required|string|max:255",
                'phone'=> "required|string|max:18",
                'address1'=> "required|string|max:255",
                'city'=> "required|string|max:255",
                'stateId'=> "required|integer",
                'countryId'=> "required|integer",
                'postalCode'=>"required|string|max:6",
                'licenseNo'=>"required|string|max:255",
                'dob'=>"required|string|max:255",
                'licenseCreationDate'=>"required|string|max:255",
                'licenseImgFrontUrl'=>"required|string|max:255",
                'licenseImgBackUrl'=>"required|string|max:255",
                'customerCompanyId'=>"integer|max:255|nullable",
                'createdBy'=> "integer|nullable",
                'updatedBy'=> "integer|nullable",
            ]);
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" => $validator->errors(),
                ]); 
            }
            $customer = Customer::create($input);
            return response()->json([
                "success" => true,
                "message" => "Customer created successfully.",
                "data" => $customer
            ]);
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
    
        }
    }


    public function list(Request $request){
        try{
            $input = $request->all();
        
            $query = Customer::query()->where('status','1');
            if(isset($input['firstName'])  && !empty($input['firstName'])){
                $query->where('firstName', 'like', '%' . $input['firstName'] . '%');
            }
            if (isset($input['sortBy']) && !empty($input['sortBy'])) {
                $query->orderBy($input['sortBy'], $input['orderBy']);
            }
            if(isset($input['page']) && !empty($input['page']) && isset($input['limit']) && !empty($input['limit'])){
                $count=$query->count();
                $input['page']= (floor($count/$input['limit']))-1;
                $query->offset($input['page'])->limit($input['limit'])->get();
            }
            $customers= $query->get();
            if(!$customers->isEmpty()){
                return response()->json([
                    "success" => true,
                    "message" => "Customer List.",
                    "data" => $customers,
                    "total_count"=>count($customers),
                ]); 
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    } 

    public function find($id){
        try{
            $customer = Customer::find($id);
            $customer = compact('customer');
            if(!empty($customer['customer'])){
                return response()->json([
                    "success" => true,
                    "message" => "Agency Data",
                    "data" => $customer,
                    "total_count"=>count($customer),
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }        
    }

     public function update(Request $request, $id)  
    {   try{
            $request = $request->all();
            $validator=Validator::make($request, [
                'firstName'=>"required|string|max:255",
                'lastName'=>"required|string|max:255",
                'customer_email'=> "required|string|max:255",
                'customer_phone'=> "required|string|max:18",
                'address1'=> "required|string|max:255",
                'city'=> "required|string|max:255",
                'stateId'=> "required|integer",
                'countryId'=> "required|integer",
                'postalCode'=>"required|string|max:6",
                'licenseNo'=>"required|string|max:255",
                'dob'=>"required|string|max:255",
                'licenseCreationDate'=>"required|string|max:255",
                'licenseImgFrontUrl'=>"required|string|max:255",
                'licenseImgBackUrl'=>"required|string|max:255",
                'customerCompanyId'=>"integer|max:255|nullable"
            ]);
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" => $validator->errors(),
                ]); 
            }
            $customer=Customer::find($id);
        
            if(!empty($customer)){
                $customer->update($request);
                return response()->json([
                    "success" => true,
                    "message" => "Customer data has been updated successfully.",
                    "data" => $customer,
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }        
    }
    
    
     public function delete($id)  
    {   
        try{
            $customer=Customer::find($id);
            
            if(!empty($customer)){
                $customer->status='0';
                $customer->save();
                return response()->json([
                    "success" => true,
                    "message" => "Customer data has been deleted successfully.",
                    "data" => $customer,
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }        
    }
    function find_customer_company($user_id){
        try{
            $query = Customer::query();
            $query->select('customer_company.*')->crossJoin('customer_company', 'customer.customerCompanyId', '=', 'customer_company.id')->where('userId', '=', $user_id);
            $customer_company= $query->get();
            
            if(!empty($customer_company)){
                return array(
                "success" => true,
                "message" => "Customer Company Details",
                "data" => $customer_company);
            }else{
                return  array(
                    "success" => false,
                    "message" => "Data Not Found."); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }        
    }
}