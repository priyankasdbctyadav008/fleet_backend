<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Common;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class CommonController extends Controller
{
    public function __construct(Request $request)
    {    
        // $header = $request->header('Authorization');
        // if (!$header) {
		// 	$data = response()->json([
        //         "success" => false,
        //         "message" => "UNAUTHORIZED.",
        //     ]); 
        //    header("Content-type:application/json");
        //    echo $data;
        //    die();
		// }else{
            if(!auth()->check()){
                 $data = response()->json([
                    "success" => false,
                    "message" => "Authentication Fail.",
                ])->getContent();
                header("Content-type:application/json");
                echo $data;
                die();
                //$this->middleware('auth:api', ['except' => ['list']]);
            }
       // }
    }
    /**
    * @OA\POST(
    * path="/api/logout",
    * operationId="Logout",
    * tags={"Common"},
    * summary="User Logout",
    * description="Logout here",
    *      @OA\Response(
    *          response=201,
    *          description="Logout Successfully",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=200,
    *          description="Logout Successfully",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=422,
    *          description="Unprocessable Entity",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    *     security={{"bearer_token":{}}} 
    * )
    */
    public function logout()
    {   try{
            Auth::logout();
            return response()->json([
                'success' => true,
                'message' => 'Successfully logged out',
            ]);
        }catch (Exception $e) {
            return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    }

    
}
