<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\VehiclesCategoryMst;
use Validator;
use Illuminate\Support\Facades\Auth;

class VehiclesCategoryMstController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth:api');
        if(!auth()->check()){
            $data = response()->json([
                "success" => false,
                "message" => "Authentication Fail.",
            ])->getContent();; 
            header("Content-type:application/json");
            echo $data;
            die();
        }else{
            $user_role = Auth::user()->role;
            if($user_role !='1' && $user_role !='2'){
                $data = response()->json([
                    "success" => false,
                    "message" => "UNAUTHORIZED.",
                ]); 
                header("Content-type:application/json");
                echo $data;
                return $data;
                die();
            }
        }
    }

     /**
        * @OA\Post(
        * path="/api/create_vehiclesCategory",
        * operationId="Create Vehicles Category",
        * tags={"Vehicles-Category"},
        * summary="Create Vehicles Category",
        * description="Create vehicles category here",
        *     @OA\RequestBody(
        *         @OA\JsonContent(
        *               required={"name","status"},
                        *@OA\Property(property="name", type="string"),
                        *@OA\Property(property="status", type="integer"),
        *),
        *         @OA\MediaType(
        *            mediaType="multipart/form-data",
        *            @OA\Schema(
        *               type="object",
        *               required={"name","status"},
                        *@OA\Property(property="name", type="string"),
                        *@OA\Property(property="status", type="integer"),
                        
        *            ),
        *        ),
        *    ),
        *      @OA\Response(
        *          response=201,
        *          description="Vehicle category created successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=200,
        *          description="Vehicle category created successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=422,
        *          description="Unprocessable Entity",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        *      security={{"bearer_token":{}}} 
        * )
        */
    public function create(Request $request){
        try{
           
            $input = $request->all();
            $validator = Validator::make($input, [
                "name"=> "required|string|max:255",
                "status"=> "required|integer",
            ]);
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" => $validator->errors(),
                ]); 
            }
            $vehiclesCategoryMst = VehiclesCategoryMst::create($input);
            return response()->json([
                "success" => true,
                "message" => "Vehicle category created successfully.",
                "data" => $vehiclesCategoryMst
            ]);
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
    
        }
    }
     /**
    * @OA\Get(
    * path="/api/list_vehiclesCategory",
    * operationId="Vehicles Category List",
    * tags={"Vehicles-Category"},
    * summary="Vehicles Category List",
    * description="Vehicles category list here",
     * @OA\Parameter(
     *          name="page",
     *          description="Page no.",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *  @OA\Parameter(
     *          name="limit",
     *          description="Limit",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *  @OA\Parameter(
     *          name="sortBy",
     *          description="SortBy",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *  @OA\Parameter(
     *          name="orderBy",
     *          description="OrderBy",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *   @OA\Parameter(
     *          name="searchKey",
     *          description="SearchKey",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *   @OA\Parameter(
     *          name="searchVal",
     *          description="SearchVal",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     
    *      @OA\Response(
    *          response=201,
    *          description="Vehicle category List.",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=200,
    *          description="Vehicle category List.",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=422,
    *          description="Unprocessable Entity",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    *     security={{"bearer_token":{}}} 
    * )
    */
    public function list(Request $request){
        try{
            $input = $request->all();
        
            $query = VehiclesCategoryMst::query();
            if(isset($input['searchKey']) && isset($input['searchVal']) && !empty($input['searchKey']) && !empty($input['searchVal'])){
                $query->where($request->input('searchKey'), 'like', '%' . $request->input('searchVal') . '%');
            }
        
            if (isset($input['sortBy']) && !empty($input['sortBy'])) {
                $query->orderBy($input['sortBy'], $input['orderBy']);
            }
            if(isset($input['page']) && !empty($input['page']) && isset($input['limit']) && !empty($input['limit'])){
                $count=$query->count();
                $input['page']= (floor($count/$input['limit']))-1;
                $query->offset($input['page'])->limit($input['limit'])->get();
            }
            //$query->where('status', '1');
            $vehiclesCategoryMst= $query->get();
        
            //print_r($vehiclesCategoryMst); die();
            if(!$vehiclesCategoryMst->isEmpty()){
                return response()->json([
                    "success" => true,
                    "message" => "Vehicle category List.",
                    "data" => $vehiclesCategoryMst,
                    "total_count"=>count($vehiclesCategoryMst),
                ]); 
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }     
    } 
     /**
     * @OA\Get(
     *      path="/api/vehiclesCategory/{id}",
     *      operationId="vehiclesCategory",
     *      tags={"Vehicles-Category"},
     *      summary="Get Vehicles Category",
     *      description="Returns vehicles category data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Vehicles Category Id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Vehicle Category Data",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Vehicle Category Data"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={{"bearer_token":{}}} 
     * )
     */
    public function find($id){
        try{
            $vehiclesCategoryMst = VehiclesCategoryMst::find($id);
            $vehiclesCategoryMst = compact('vehiclesCategoryMst');
            if(!empty($vehiclesCategoryMst['vehiclesCategoryMst'])){
                return response()->json([
                    "success" => true,
                    "message" => "Vehicle Category Data",
                    "data" => $vehiclesCategoryMst,
                    "total_count"=>count($vehiclesCategoryMst),
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }     
    }
    /**
        * @OA\Post(
        * path="/api/update_vehiclesCategory/{id}",
        * operationId="Update Vehicles Category",
        * tags={"Vehicles-Category"},
        * summary="Update Vehicles Category",
        * description="Update vehicles category here",
        *      @OA\Parameter(
        *          name="id",
        *          description="Agency Id",
        *          required=true,
        *          in="path",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *     @OA\RequestBody(
        *         @OA\JsonContent(
        *               required={"name","status"},
                        *@OA\Property(property="name", type="string"),
                        *@OA\Property(property="status", type="integer"),
        *),
        *         @OA\MediaType(
        *            mediaType="multipart/form-data",
        *            @OA\Schema(
        *               type="object",
        *               required={"name","status"},
                        *@OA\Property(property="name", type="string"),
                        *@OA\Property(property="status", type="integer"),
                        
        *            ),
        *        ),
        *    ),
        *      @OA\Response(
        *          response=201,
        *          description="Vehicle category Update successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=200,
        *          description="Vehicle category Update successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=422,
        *          description="Unprocessable Entity",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        *      security={{"bearer_token":{}}} 
        * )
        */
    public function update(Request $request, $id)  
    {   try{
            $request = $request->all();
            $validator=Validator::make($request, [
                "name"=> "required|string|max:255",
                "status"=> "required|integer|max:255",
            ]);
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" => $validator->errors(),
                ]); 
            }
            $vehiclesCategoryMst= VehiclesCategoryMst::find($id);
        
            if(!empty($vehiclesCategoryMst)){
                $vehiclesCategoryMst->update($request);
                return response()->json([
                    "success" => true,
                    "message" => "Vehicle category data has been updated successfully.",
                    "data" => $vehiclesCategoryMst,
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }
    }  
     /**
     * @OA\Delete(
     *      path="/api/delete_vehiclesCategory/{id}",
     *      operationId="Delete vehiclesCategory",
     *      tags={"Vehicles-Category"},
     *      summary="Delete vehiclesCategory",
     *      description="Delete vehiclesCategory",
     *      @OA\Parameter(
     *          name="id",
     *          description="VehiclesCategory Id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Vehicles category data has been deleted successfully.",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={{"bearer_token":{}}} 
     * )
     */ 
    public function delete($id)  
    {   try{
            $vehiclesCategoryMst=VehiclesCategoryMst::find($id);
            if(!empty($vehiclesCategoryMst)){
                //$vehiclesCategoryMst->status='0';
                $vehiclesCategoryMst->delete();
                return response()->json([
                    "success" => true,
                    "message" => "Vehicle category data has been deleted successfully.",
                    "data" => $vehiclesCategoryMst,
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
    
        }
    }
    
    /**
     * @OA\Get(
     *      path="/api/dropDown_vehiclesCategory",
     *      operationId="Vehicles Category DropDown",
     *      tags={"Vehicles-Category"},
     *      summary="Get Vehicles Category DropDown",
     *      description="Returns vehicles category dropDown data",
    
     *      @OA\Response(
     *          response=200,
     *          description="Vehicles Category Data",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Vehicles Category Data Data"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={{"bearer_token":{}}} 
     * )
     */
    public function list_dropDown(Request $request){
        try{
            $input = $request->all();
        
            $query = VehiclesCategoryMst::query();
            $query->where('status', '1');
            $vehiclesCategoryMst= $query->get();
            
            if(!$vehiclesCategoryMst->isEmpty()){
                return response()->json([
                    "success" => true,
                    "message" => "Vehicle category List.",
                    "data" => $vehiclesCategoryMst,
                    "total_count"=>count($vehiclesCategoryMst),
                ]); 
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }     
    } 
    
}
