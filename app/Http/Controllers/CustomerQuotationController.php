<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\Customer;
use App\models\CustomerCompany;
use App\Models\CustomerQuotation;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\QuotationRequest;

class CustomerQuotationController extends Controller
{
    public function __construct()
    {     
        //$this->middleware('auth:api');
        if(!auth()->check()){
                 $data = response()->json([
                    "success" => false,
                    "message" => "Authentication Fail.",
                ])->getContent();; 
                header("Content-type:application/json");
                echo $data;
                die();
        }else{
            $user_role = Auth::user()->role;
            if($user_role !='1' && $user_role !='2'){
                $data = response()->json([
                    "success" => false,
                    "message" => "UNAUTHORIZED.",
                ]); 
                header("Content-type:application/json");
                echo $data;
                return $data;
                die();
            }
        }
    }
    /**
        * @OA\Post(
        * path="/api/create_customerQuotation",
        * operationId="Create Customer Quotation",
        * tags={"Customer-Quotation"},
        * summary="Create Customer Quotation",
        * description="Create Customer Quotation here",
        *     @OA\RequestBody(
        *         @OA\JsonContent(
        *               required={"firstName","lastName","email","phone","address1","city","stateId","countryId","postalCode","licenseNo","dob","licenseCreationDate","licenseImgFrontUrl","licenseImgBackUrl","name","company_email","company_phone","isProfessional","company_address1","company_city","company_stateId","company_countryId","company_postalCode","source","vehicleCategoryId","amount","departureAgencyId","returnAgencyId"},
                    *@OA\Property(property="firstName", type="string"),
                    *@OA\Property(property="lastName", type="string"),
                    *@OA\Property(property="email", type="string"),
                    *@OA\Property(property="phone", type="string"),
                    *@OA\Property(property="address1", type="string"),
                    *@OA\Property(property="city", type="string"),
                    *@OA\Property(property="stateId", type="integer"),
                    *@OA\Property(property="countryId", type="integer"),
                    *@OA\Property(property="postalCode", type="string"),
                    *@OA\Property(property="licenseNo", type="string"),
                    *@OA\Property(property="dob", type="string"),
                    *@OA\Property(property="licenseCreationDate", type="string"),
                    *@OA\Property(property="licenseImgFrontUrl", type="string"),
                    *@OA\Property(property="licenseImgBackUrl", type="string"),
                    *@OA\Property(property="customerCompanyId",type="integer"),
                    *@OA\Property(property="name", type="string"),
                    *@OA\Property(property="company_email", type="string"),
                    *@OA\Property(property="company_phone", type="string"),
                    *@OA\Property(property="sirenno", type= "integer"),
                    *@OA\Property(property="isProfessional", type= "string"),
                    *@OA\Property(property="createdBy", type= "integer"),
                    *@OA\Property(property="updatedBy", type= "integer"),
                    *@OA\Property(property="company_address1", type="string"),
                    *@OA\Property(property="company_city", type="string"),
                    *@OA\Property(property="company_stateId", type="integer"),
                    *@OA\Property(property="company_countryId", type="integer"),
                    *@OA\Property(property="company_postalCode", type="string"),
                    *@OA\Property(property="customerId", type="integer"),
                    *@OA\Property(property="startDate", type="string"),
                    *@OA\Property(property="endDate", type="string"),
                    *@OA\Property(property="source", type="integer"),
                    *@OA\Property(property="vehicleCategoryId", type="integer"),
                    *@OA\Property(property="amount",type="integer"),
                    *@OA\Property(property="departureAgencyId", type="integer"),
                    *@OA\Property(property="returnAgencyId", type="integer"),
                    *@OA\Property(property="quotationExpiryDate", type="string"),
                    *@OA\Property(property="status", type="integer")
        *),
        *         @OA\MediaType(
        *            mediaType="multipart/form-data",
        *            @OA\Schema(
        *               type="object",
        *               required={"firstName","lastName","email","phone","address1","city","stateId","countryId","postalCode","licenseNo","dob","licenseCreationDate","licenseImgFrontUrl","licenseImgBackUrl","name","company_email","company_phone","isProfessional","company_address1","company_city","company_stateId","company_countryId","company_postalCode","source","vehicleCategoryId","amount","departureAgencyId","returnAgencyId"},
                    *@OA\Property(property="firstName", type="string"),
                    *@OA\Property(property="lastName", type="string"),
                    *@OA\Property(property="email", type="string"),
                    *@OA\Property(property="phone", type="string"),
                    *@OA\Property(property="address1", type="string"),
                    *@OA\Property(property="city", type="string"),
                    *@OA\Property(property="stateId", type="integer"),
                    *@OA\Property(property="countryId", type="integer"),
                    *@OA\Property(property="postalCode", type="string"),
                    *@OA\Property(property="licenseNo", type="string"),
                    *@OA\Property(property="dob", type="string"),
                    *@OA\Property(property="licenseCreationDate", type="string"),
                    *@OA\Property(property="licenseImgFrontUrl", type="string"),
                    *@OA\Property(property="licenseImgBackUrl", type="string"),
                    *@OA\Property(property="customerCompanyId",type="integer"),
                    *@OA\Property(property="name", type="string"),
                    *@OA\Property(property="company_email", type="string"),
                    *@OA\Property(property="company_phone", type="string"),
                    *@OA\Property(property="sirenno", type="integer"),
                    *@OA\Property(property="isProfessional", type="string"),
                    *@OA\Property(property="createdBy", type="integer"),
                    *@OA\Property(property="updatedBy", type="integer"),
                    *@OA\Property(property="company_address1", type="string"),
                    *@OA\Property(property="company_city", type="string"),
                    *@OA\Property(property="company_stateId", type="integer"),
                    *@OA\Property(property="company_countryId", type="integer"),
                    *@OA\Property(property="company_postalCode", type="string"),
                    *@OA\Property(property="customerId", type="integer"),
                    *@OA\Property(property="startDate", type="string"),
                    *@OA\Property(property="endDate", type="string"),
                    *@OA\Property(property="source", type="integer"),
                    *@OA\Property(property="vehicleCategoryId", type="integer"),
                    *@OA\Property(property="amount",type="integer"),
                    *@OA\Property(property="departureAgencyId", type="integer"),
                    *@OA\Property(property="returnAgencyId", type="integer"),
                    *@OA\Property(property="quotationExpiryDate", type="string"),
                    *@OA\Property(property="status", type="integer")
        *            ),
        *        ),
        *    ),
        *      @OA\Response(
        *          response=201,
        *          description="Customer quotation created successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=200,
        *          description="Customer quotation created successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=422,
        *          description="Unprocessable Entity",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        *      security={{"bearer_token":{}}} 
        * )
        */
    public function create(Request $request){
        try {
            $input = $request->all();
            //$validated = $request->validated();
            $validator = Validator::make($input,[ 
                //companay details
                'name'=> "required|string|max:255",
                'company_email'=> "required|string|max:255",
                'company_phone'=> "required|string|max:18",
                'sirenno'=> "integer|max:255",
                'isProfessional'=>"required|string|max:255",
                'createdBy'=> "integer|nullable",
                'updatedBy'=> "integer|nullable",
                'company_address1'=> "required|string|max:255",
                //'address2'=> "required|string|max:255",
                'company_city'=> "required|string|max:255",
                'company_stateId'=> "required|integer",
                'company_countryId'=> "required|integer",
                'company_postalCode'=>"required|string|max:6",
               
                //customer details
                'firstName'=>"required|string|max:255",
                'lastName'=>"required|string|max:255",
                'email'=> "required|string|max:255",
                'phone'=> "required|string|max:18",
                'address1'=> "required|string|max:255",
                //'address2'=> "required|string|max:255",
                'city'=> "required|string|max:255",
                'stateId'=> "required|integer",
                'countryId'=> "required|integer",
                'postalCode'=>"required|string|max:6",
                //'nationality'=>"required|string|max:255",
                'licenseNo'=>"required|string|max:255",
                'dob'=>"required|string|max:255",
                'licenseCreationDate'=>"required|string|max:255",
               // 'licenseExpiryDate'=>"required|string|max:255",
                'licenseImgFrontUrl'=>"required|string|max:255",
                'licenseImgBackUrl'=>"required|string|max:255",
              //  'isProfessional'=>"required|string|max:255",
               // 'permit'=>"required|string|max:255",
                'customerCompanyId'=>"integer|max:255|nullable",
              
                
                //customer quotations 
                "customerId"=> "integer|max:255|nullable",
                "startDate"=> "string|nullable",
                "endDate"=> "string|nullable",
                "source"=> "required|integer|max:255",
                "vehicleCategoryId"=> "required|integer|max:255",
               // "vehicleTypeId"=> "required|integer|max:255",
                "amount"=> "required|integer",
                "departureAgencyId"=> "required|integer|max:255",
                "returnAgencyId"=> "required|integer|max:255",
                "quotationExpiryDate"=> "string|nullable",
                "status"=> "integer|max:255|nullable",
            ]);
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" => $validator->errors(),
                ]); 
            }
            //companay details
            if(!isset($request->customerCompanyId) || empty($request->customerCompanyId) ){
                 $company=CustomerCompany::create([
                    'name'=>$request->name,
                    'company_email'=>$request->company_email,
                    'company_phone'=>$request->company_phone,
                    'sirenno'=>$request->sirenno,
                    'company_address1'=> $request->company_address1,
                  //  'address2'=> $request->address2,
                    'company_city'=> $request->company_city,
                    'company_stateId'=> $request->company_stateId,
                    'company_countryId'=> $request->company_countryId,
                    'company_postalCode'=>$request->company_postalCode,
                    'isProfessional'=>$request->isProfessional,
                    'createdBy'=>isset($request->createdBy) && !empty($request->createdBy) ? $request->createdBy:'1',
                    'updatedBy'=>isset($request->updatedBy) && !empty($request->updatedBy)?$request->updatedBy:'1',
                 ]);
            }
            if(!isset($request->customerId) || empty($request->customerId)){
                $customer=Customer::create([
                    'userId'=> $request->userId,
                    'firstName'=>$request->firstName,
                    'lastName'=>$request->lastName,
                    'email'=>$request->customer_email,
                    'phone'=>$request->customer_phone,
                    'address1'=> $request->address1,
                  //  'address2'=> $request->address2,
                    'city'=> $request->city,
                    'stateId'=> $request->stateId,
                    'countryId'=> $request->countryId,
                    'postalCode'=>$request->postalCode,
                    //'nationality'=>$request->nationality,
                    'licenseNo'=>$request->licenseNo,
                    'dob'=>$request->dob,
                    'licenseCreationDate'=>$request->licenseCreationDate,
                   // 'licenseExpiryDate'=>$request->licenseExpiryDate,
                    'licenseImgFrontUrl'=>$request->licenseImgFrontUrl,
                    'licenseImgBackUrl'=>$request->licenseImgBackUrl,
                   // 'isProfessional'=>$request->isProfessional,
                    //'permit'=>$request->permit,
                    'customerCompanyId'=>isset($request->customerCompanyId) && !empty($request->customerCompanyId) ?$request->customerCompanyId :$company->id,
                    'createdBy'=>isset($request->createdBy) && !empty($request->createdBy) ? $request->createdBy:'1',
                    'updatedBy'=>isset($request->updatedBy) && !empty($request->updatedBy)?$request->updatedBy:'1',
                ]);
            }
           
            $customerQuotation = CustomerQuotation::create([
                "customerId"=> (isset($request->customerId) && !empty($request->customerId) ) ?  $request->customerId :$customer->id,
                "startDate"=> $request->startDate,
                "endDate"=> $request->endDate,
                "source"=> $request->source,
                "vehicleCategoryId"=> $request->vehicleCategoryId,
                //"vehicleTypeId"=> $request->vehicleTypeId,
                "amount"=> $request->amount,
                "departureAgencyId"=> $request->departureAgencyId,
                "returnAgencyId"=> $request->returnAgencyId,
                "quotationExpiryDate"=> $request->quotationExpiryDate,
                "status"=> $request->status,
            ]);
            return response()->json([
                "success" => true,
                "message" => "Customer quotation created successfully.",
                "data" => $customerQuotation
            ]);
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
    
        }
    }
      /**
    * @OA\Get(
    * path="/api/list_customerQuotation",
    * operationId="Customer quotation List.",
    * tags={"Customer-Quotation"},
    * summary="Customer Quotation List",
    * description="Customer quotation list here",
    *      @OA\Response(
    *          response=201,
    *          description="Customer quotation List.",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=200,
    *          description="Customer quotation List.",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=422,
    *          description="Unprocessable Entity",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    *     security={{"bearer_token":{}}} 
    * )
    */
    public function list(Request $request){
        try{
            $input = $request->all();
        
            $query = CustomerQuotation::query();

            $query->select('cusQuo.*', 'customer.firstName as customer_first_name','customer.lastName as customer_last_name','company.name')->leftJoin('customer', 'cusQuo.customerId', '=', 'customer.id')->from('customer_quotations as cusQuo')->leftJoin('customer_company as company','company.id', '=', 'customer.customerCompanyId');
            //->from('customer_quotations as cusQuo')
            
            if(isset($input['searchKey']) && isset($input['searchVal']) && !empty($input['searchKey']) && !empty($input['searchVal'])){
                $query->where('cusQuo.'. $request->input('searchKey'), 'like', '%' . $request->input('searchVal') . '%');
            }
        
            if (isset($input['sortBy']) && !empty($input['sortBy'])) {
                $query->orderBy('cusQuo.'. $input['sortBy'], $input['orderBy']);
            }
            if(isset($input['page']) && !empty($input['page']) && isset($input['limit']) && !empty($input['limit'])){
                $count=$query->count();
                $input['page']= (floor($count/$input['limit']))-1;
                $query->offset($input['page'])->limit($input['limit'])->get();
            }
            $customerQuotation= $query->get();
            if(!$customerQuotation->isEmpty()){
                return response()->json([
                    "success" => true,
                    "message" => "Customer quotation List.",
                    "data" => $customerQuotation,
                    "total_count"=>count($customerQuotation),
                ]); 
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }        
    } 
     /**
     * @OA\Get(
     *      path="/api/customerQuotation/{id}",
     *      operationId="Customer Quotation Find",
     *      tags={"Customer-Quotation"},
     *      summary="Get Customer Quotation",
     *      description="Returns customer quotation data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Customer Quotation Id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Customer quotation Data",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Customer quotation Data"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={{"bearer_token":{}}} 
     * )
     */
    public function find($id){
        try{
            $customerQuotation = CustomerQuotation::find($id);
            $customerQuotation = compact('customerQuotation');
            if(!empty($customerQuotation['customerQuotation'])){
                return response()->json([
                    "success" => true,
                    "message" => "Customer quotation Data",
                    "data" => $customerQuotation,
                    "total_count"=>count($customerQuotation),
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    }
     /**
        * @OA\Post(
        * path="/api/update_customerQuotation",
        * operationId="Update Customer Quotation",
        * tags={"Customer-Quotation"},
        * summary="Update Customer Quotation",
        * description="Update Customer Quotation here",
        *     @OA\RequestBody(
        *         @OA\JsonContent(
        *               required={"customerId","source","vehicleCategoryId","vehicleTypeId","amount","departureAgencyId","returnAgencyId"},
                        *@OA\Property(property="customerId", type="integer"),
                        *@OA\Property(property="startDate", type= "string"),
                        *@OA\Property(property="endDate", type= "string"),
                        *@OA\Property(property="source", type="integer"),
                        *@OA\Property(property="vehicleCategoryId", type="integer"),
                        *@OA\Property(property="vehicleTypeId", type="integer"),
                        *@OA\Property(property="amount", type="integer"),
                        *@OA\Property(property="departureAgencyId", type="integer"),
                        *@OA\Property(property="returnAgencyId", type="integer"),
                        *@OA\Property(property="quotationExpiryDate", type="string"),
                        *@OA\Property(property="status",type= "integer")
        *),
        *         @OA\MediaType(
        *            mediaType="multipart/form-data",
        *            @OA\Schema(
        *               type="object",
        *               required={"customerId","source","vehicleCategoryId","vehicleTypeId","amount","departureAgencyId","returnAgencyId"},
                        *@OA\Property(property="customerId", type="integer"),
                        *@OA\Property(property="startDate", type= "string"),
                        *@OA\Property(property="endDate", type= "string"),
                        *@OA\Property(property="source", type="integer"),
                        *@OA\Property(property="vehicleCategoryId", type="integer"),
                        *@OA\Property(property="vehicleTypeId", type="integer"),
                        *@OA\Property(property="amount", type="integer"),
                        *@OA\Property(property="departureAgencyId", type="integer"),
                        *@OA\Property(property="returnAgencyId", type="integer"),
                        *@OA\Property(property="quotationExpiryDate", type="string"),
                        *@OA\Property(property="status",type= "integer")
                        
        *            ),
        *        ),
        *    ),
        *      @OA\Response(
        *          response=201,
        *          description="Customer quotation has been updated successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=200,
        *          description="Customer quotation has been updated successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=422,
        *          description="Unprocessable Entity",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        *      security={{"bearer_token":{}}} 
        * )
        */
    public function update(Request $request, $id)  
    {   
        try{
            $request = $request->all();
            $validator=Validator::make($request, [
                "customerId"=> "required|integer|max:255",
                "startDate"=> "string|max:255",
                "endDate"=> "string|max:255",
                "source"=> "required|integer|max:255",
                "vehicleCategoryId"=> "required|integer|max:255",
                "vehicleTypeId"=> "required|integer|max:255",
                "amount"=> "required|integer",
                "departureAgencyId"=> "required|integer|max:255",
                "returnAgencyId"=> "required|integer|max:255",
                "quotationExpiryDate"=> "string",
                "status"=> "integer|max:255",
            ]);
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" => $validator->errors(),
                ]); 
            }
            $customerQuotation= CustomerQuotation::find($id);
        
            if(!empty($customerQuotation)){
                $customerQuotation->update($request);
                return response()->json([
                    "success" => true,
                    "message" => "Customer quotation has been updated successfully.",
                    "data" => $customerQuotation,
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    }  
     /**
     * @OA\Delete(
     *      path="/api/delete_customerQuotation/{id}",
     *      operationId="Delete CustomerQuotation",
     *      tags={"Customer-Quotation"},
     *      summary="Delete Customer Quotation",
     *      description="Delete customer quotation",
     *      @OA\Parameter(
     *          name="id",
     *          description="CustomerQuotation Id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Customer quotation data has been deleted successfully.",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={{"bearer_token":{}}} 
     * )
     */ 
    public function delete($id)  
    {   
        try{
            $customerQuotation=CustomerQuotation::find($id);
            if(!empty($customerQuotation)){
                $customerQuotation->status='deleted';
                $customerQuotation->save();
                return response()->json([
                    "success" => true,
                    "message" => "Customer quotation data has been deleted successfully.",
                    "data" => $customerQuotation,
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    }
}
