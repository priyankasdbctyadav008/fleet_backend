<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AgencyVehicles;
use Validator;
use Illuminate\Support\Facades\Auth;
class AgencyVehiclesController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth:api');
        if(!auth()->check()){
                 $data = response()->json([
                    "success" => false,
                    "message" => "Authentication Fail.",
                ])->getContent();; 
                header("Content-type:application/json");
                echo $data;
                die();
        }else{
            $user_role = Auth::user()->role;
            if($user_role !='1' && $user_role !='2'){
                $data = response()->json([
                    "success" => false,
                    "message" => "UNAUTHORIZED.",
                ]); 
                header("Content-type:application/json");
                echo $data;
                return $data;
                die();
            }
        }
    }
     /**
        * @OA\Post(
            * path="/api/create_agencyVehicle",
            * operationId="Create Agency Vehicle",
            * tags={"Agency-Vehicle"},
            * summary="Create Agency Vehicle",
            * description="Create Agency Vehicle here",
            *     @OA\RequestBody(
            *         @OA\JsonContent(
                       *               required={"vehicleCategoryId","vehicleTypeId","vehicleBrandId","model","noOfDoors","agencyId","yearOfRegistration","motorization","registrationNo","gearBox","noOfSeats","mileage","vehicleFeatures","ratePerDay","miniMumDay","maxiMumDay","image_url1","image_url2","image_url3","image_url4","image_url5","image_url6","status","lastInspectionDate","lastRevisionDate"},
            *         @OA\Property(property="vehicleCategoryId", type="integer"),
            *         @OA\Property(property="vehicleTypeId", type="integer"),
            *         @OA\Property(property="vehicleBrandId", type="integer"),
            *         @OA\Property(property="model", type="string"),
            *         @OA\Property(property="noOfDoors", type="integer"),
            *         @OA\Property(property="agencyId", type="integer"),
            *         @OA\Property(property="yearOfRegistration", type="string"),
            *         @OA\Property(property="motorization", type="json", format="json", example={ "diesel": "diesel","petrol": "petrol","electric": "electric","hybrid": "hybrid","SP_gasonline": "SP_gasonline","other": "other"} ),
            *         @OA\Property(property="registrationNo", type="string"),
            *         @OA\Property(property="gearBox", type="json", format="json", example={"Manual": "manual","Automatic": "automatic"}),
            *         @OA\Property(property="noOfSeats", type="integer"),
            *         @OA\Property(property="mileage", type="string"),
            *         @OA\Property(property="vehicleFeatures", type="json",format="json", example={"Gps": "no","Reversing_Radar": "no","Bluetooth": "no","Air_Conditioner": "no","isofix_Seat": "no"}),
            *         @OA\Property(property="ratePerDay", type="string"),
            *         @OA\Property(property="miniMumDay", type="integer"),
            *         @OA\Property(property="maxiMumDay", type="integer"),
            *         @OA\Property(property="image_url1", type="string"),
            *         @OA\Property(property="image_url2", type="string"),
            *         @OA\Property(property="image_url3", type="string"),
            *         @OA\Property(property="image_url4", type="string"),
            *         @OA\Property(property="image_url5", type="string"),
            *         @OA\Property(property="image_url6", type="string"),
             
            *         @OA\Property(property="status", type="integer"),
            *         @OA\Property(property="lastInspectionDate", type="string"),
            *         @OA\Property(property="lastRevisionDate", type="string"),
            *         @OA\Property(property="createdBy", type="integer"),
            *         @OA\Property(property="updatedBy", type="integer")
            *),
            *         @OA\MediaType(
            *            mediaType="multipart/form-data",
            *            @OA\Schema(
            *               type="object",
                 *               required={"vehicleCategoryId","vehicleTypeId","vehicleBrandId","model","noOfDoors","agencyId","yearOfRegistration","motorization","registrationNo","gearBox","noOfSeats","mileage","vehicleFeatures","ratePerDay","miniMumDay","maxiMumDay","image_url1","image_url2","image_url3","image_url4","image_url5","image_url6","status","lastInspectionDate","lastRevisionDate"},
            *         @OA\Property(property="vehicleCategoryId", type="integer"),
            *         @OA\Property(property="vehicleTypeId", type="integer"),
            *         @OA\Property(property="vehicleBrandId", type="integer"),
            *         @OA\Property(property="model", type="string"),
            *         @OA\Property(property="noOfDoors", type="integer"),
            *         @OA\Property(property="agencyId", type="integer"),
            *         @OA\Property(property="yearOfRegistration", type="string"),
            *         @OA\Property(property="motorization", type="json", format="json", example={ "diesel": "diesel","petrol": "petrol","electric": "electric","hybrid": "hybrid","SP_gasonline": "SP_gasonline","other": "other"} ),
            *         @OA\Property(property="registrationNo", type="string"),
            *         @OA\Property(property="gearBox", type="json", format="json", example={"Manual": "manual","Automatic": "automatic"}),
            *         @OA\Property(property="noOfSeats", type="integer"),
            *         @OA\Property(property="mileage", type="string"),
            *         @OA\Property(property="vehicleFeatures", type="json",format="json", example={"Gps": "no","Reversing_Radar": "no","Bluetooth": "no","Air_Conditioner": "no","isofix_Seat": "no"}),
            *         @OA\Property(property="ratePerDay", type="string"),
            *         @OA\Property(property="miniMumDay", type="integer"),
            *         @OA\Property(property="maxiMumDay", type="integer"),
            *         @OA\Property(property="image_url1", type="string"),
            *         @OA\Property(property="image_url2", type="string"),
            *         @OA\Property(property="image_url3", type="string"),
            *         @OA\Property(property="image_url4", type="string"),
            *         @OA\Property(property="image_url5", type="string"),
            *         @OA\Property(property="image_url6", type="string"),
            *         @OA\Property(property="status", type="integer"),
            *         @OA\Property(property="lastInspectionDate", type="string"),
            *         @OA\Property(property="lastRevisionDate", type="string"),
            *         @OA\Property(property="createdBy", type="integer"),
            *         @OA\Property(property="updatedBy", type="integer")
           
        *            ),
        *        ),
        *    ),
        *      @OA\Response(
        *          response=201,
        *          description="Register Successfully",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=200,
        *          description="Register Successfully",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=422,
        *          description="Unprocessable Entity",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        *      security={{"bearer_token":{}}} 
        * )
        */
    public function create(Request $request){
        try {
            $input = $request->all();
            
            $validator = Validator::make($input, [
                "vehicleCategoryId"=> "required|integer|max:255",
                "vehicleTypeId"=> "required|integer|max:255",
                "vehicleBrandId"=> "required|integer|max:255",
                "model"=> "required|string|max:255",
                "noOfDoors"=> "required|integer|max:255",
                "agencyId"=> "required|integer|max:255",
                "yearOfRegistration"=> "required|string|max:255",
                "motorization"=> "required|json|max:255",
                "registrationNo"=> "required|string|max:255",
                "gearBox"=> "required|json|max:255",
                "noOfSeats"=> "required|integer|max:255",
                "mileage"=> "required|string|max:255",
                "vehicleFeatures"=> "required|json|max:255",
                "ratePerDay"=> "required|string|max:255",
                "miniMumDay"=> "required|integer|max:255",
                "maxiMumDay"=> "required|integer|max:255",
                "status"=> "required|integer|max:255",
                "lastInspectionDate"=> "required|string|max:255",
                "lastRevisionDate"=> "required|string|max:255",
                "createdBy"=> "integer|max:255|nullable",
                "updatedBy"=>"integer|max:255|nullable",
                "image_url1" =>"string|required",
                "image_url2" =>"string|required",
                "image_url3" =>"string|required",
                "image_url4" =>"string|required",
                "image_url5" =>"string|required",
                "image_url6" =>"required",
            ]);
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" => $validator->errors(),
                ]); 
            }
            $agenciesVehicles = AgencyVehicles::create($input);
            return response()->json([
                "success" => true,
                "message" => "Agency Vehicle Created Successfully",
                "data" => $agenciesVehicles
            ]);
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
    
        }
    }
     /**
    * @OA\Get(
    * path="/api/list_agencyVehicle",
    * operationId="Agency Vehicle List",
    * tags={"Agency-Vehicle"},
    * summary="Agency Vehicle List",
    * description="Agency vehicle list here",
     * @OA\Parameter(
     *          name="page",
     *          description="Page no.",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *  @OA\Parameter(
     *          name="limit",
     *          description="Limit",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *  @OA\Parameter(
     *          name="sortBy",
     *          description="SortBy",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *  @OA\Parameter(
     *          name="orderBy",
     *          description="OrderBy",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *   @OA\Parameter(
     *          name="searchKey",
     *          description="SearchKey",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *   @OA\Parameter(
     *          name="searchVal",
     *          description="SearchVal",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     
    *      @OA\Response(
    *          response=201,
    *          description="Agency List.",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=200,
    *          description="Agency Vehicle List.",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=422,
    *          description="Unprocessable Entity",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    *     security={{"bearer_token":{}}} 
    * )
    */
    public function list(Request $request){
        $input = $request->all();
       
        $query = AgencyVehicles::query()->where('status','1');;
        if(isset($input['searchKey']) && isset($input['searchVal']) && !empty($input['searchKey']) && !empty($input['searchVal'])){
            $query->where($request->input('searchKey'), 'like', '%' . $request->input('searchVal') . '%');
        }
        if(isset($input['organizationId'])  && !empty($input['organizationId'])){
            $query->where('organizationId', 'like', '%' . $input['organizationId'] . '%');
        }
        if (isset($input['sortBy']) && !empty($input['sortBy'])) {
            $query->orderBy($input['sortBy'], $input['orderBy']);
        }
        if(isset($input['page']) && !empty($input['page']) && isset($input['limit']) && !empty($input['limit'])){
            $count=$query->count();
            $input['page']= (floor($count/$input['limit']))-1;
            $query->offset($input['page'])->limit($input['limit']);
        }
        $query->where('status', '1');
        $agenciesVehicles= $query->get();
        if(!empty($agenciesVehicles)){
            return response()->json([
                "success" => true,
                "message" => "Agency Vehicle List.",
                "data" => $agenciesVehicles,
                "total_count"=>count($agenciesVehicles),
            ]); 
        }else{
            return response()->json([
                "success" => false,
                "message" => "Data Not Found.",
            ]); 
        }
    } 

    /**
     * @OA\Get(
     *      path="/api/agencyVehicle/{id}",
     *      operationId="Agency Vehicle Data",
     *      tags={"Agency-Vehicle"},
     *      summary="Get Agency Vehicle",
     *      description="Returns Agency  Vehicle data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Agency Vehicle Id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Agency Vehicle Data",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Agency Vehicle Data"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={{"bearer_token":{}}} 
     * )
     */
    public function find($id){
        $agencyVehicles = AgencyVehicles::find($id);
        $agencyVehicles = compact('agencyVehicles');
        if(!empty($agencyVehicles['agencyVehicles'])){
            return response()->json([
                "success" => true,
                "message" => "Agency Vehicle Data",
                "data" => $agencyVehicles,
                "total_count"=>count($agencyVehicles),
            ]);
        }else{
            return response()->json([
                "success" => false,
                "message" => "Data Not Found.",
            ]); 
        }
    }
     /**
        * @OA\Post(
        * path="/api/update_agencyVhicle/{id}",
        * operationId="Update Agency Vhicle",
        * tags={"Agency-Vehicle"},
        * summary="Update Agency Vhicle",
        * description="Update Agency Vhicle here",
        *      @OA\Parameter(
        *          name="id",
        *          description="Agency Vhicle Id",
        *          required=true,
        *          in="path",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *     @OA\RequestBody(
        *         @OA\JsonContent(
                       * required={"vehicleCategoryId","vehicleTypeId","vehicleBrandId","model","noOfDoors","agencyId","yearOfRegistration","motorization","registrationNo","gearBox","noOfSeats","mileage","vehicleFeatures","ratePerDay","miniMumDay","maxiMumDay","image_url1","image_url2","image_url3","image_url4","image_url5","image_url6","status","lastInspectionDate","lastRevisionDate"},
            *         @OA\Property(property="vehicleCategoryId", type="integer"),
            *         @OA\Property(property="vehicleTypeId", type="integer"),
            *         @OA\Property(property="vehicleBrandId", type="integer"),
            *         @OA\Property(property="model", type="string"),
            *         @OA\Property(property="noOfDoors", type="integer"),
            *         @OA\Property(property="agencyId", type="integer"),
            *         @OA\Property(property="yearOfRegistration", type="string"),
            *         @OA\Property(property="motorization", type="json", format="json", example={ "diesel": "diesel","petrol": "petrol","electric": "electric","hybrid": "hybrid","SP_gasonline": "SP_gasonline","other": "other"} ),
            *         @OA\Property(property="registrationNo", type="string"),
            *         @OA\Property(property="gearBox", type="json", format="json", example={"Manual": "manual","Automatic": "automatic"}),
            *         @OA\Property(property="noOfSeats", type="integer"),
            *         @OA\Property(property="mileage", type="string"),
            *         @OA\Property(property="vehicleFeatures", type="json",format="json", example={"Gps": "no","Reversing_Radar": "no","Bluetooth": "no","Air_Conditioner": "no","isofix_Seat": "no"}),
            *         @OA\Property(property="ratePerDay", type="string"),
            *         @OA\Property(property="miniMumDay", type="integer"),
            *         @OA\Property(property="maxiMumDay", type="integer"),
             *         @OA\Property(property="image_url1", type="string"),
            *         @OA\Property(property="image_url2", type="string"),
            *         @OA\Property(property="image_url3", type="string"),
            *         @OA\Property(property="image_url4", type="string"),
            *         @OA\Property(property="image_url5", type="string"),
            *         @OA\Property(property="image_url6", type="string"),
            *         @OA\Property(property="status", type="integer"),
            *         @OA\Property(property="lastInspectionDate", type="string"),
            *         @OA\Property(property="lastRevisionDate", type="string"),
            *         @OA\Property(property="createdBy", type="integer"),
            *         @OA\Property(property="updatedBy", type="integer")
            *),
            *         @OA\MediaType(
            *            mediaType="multipart/form-data",
            *            @OA\Schema(
            *               type="object",
            *         required={"vehicleCategoryId","vehicleTypeId","vehicleBrandId","model","noOfDoors","agencyId","yearOfRegistration","motorization","registrationNo","gearBox","noOfSeats","mileage","vehicleFeatures","ratePerDay","miniMumDay","maxiMumDay","image_url1","image_url2","image_url3","image_url4","image_url5","image_url6","status","lastInspectionDate","lastRevisionDate"},
            *         @OA\Property(property="vehicleCategoryId", type="integer"),
            *         @OA\Property(property="vehicleTypeId", type="integer"),
            *         @OA\Property(property="vehicleBrandId", type="integer"),
            *         @OA\Property(property="model", type="string"),
            *         @OA\Property(property="noOfDoors", type="integer"),
            *         @OA\Property(property="agencyId", type="integer"),
            *         @OA\Property(property="yearOfRegistration", type="string"),
            *         @OA\Property(property="motorization", type="json", format="json", example={ "diesel": "diesel","petrol": "petrol","electric": "electric","hybrid": "hybrid","SP_gasonline": "SP_gasonline","other": "other"} ),
            *         @OA\Property(property="registrationNo", type="string"),
            *         @OA\Property(property="gearBox", type="json", format="json", example={"Manual": "manual","Automatic": "automatic"}),
            *         @OA\Property(property="noOfSeats", type="integer"),
            *         @OA\Property(property="mileage", type="string"),
            *         @OA\Property(property="vehicleFeatures", type="json",format="json", example={"Gps": "no","Reversing_Radar": "no","Bluetooth": "no","Air_Conditioner": "no","isofix_Seat": "no"}),
            *         @OA\Property(property="ratePerDay", type="string"),
            *         @OA\Property(property="miniMumDay", type="integer"),
            *         @OA\Property(property="maxiMumDay", type="integer"),
         
            *         @OA\Property(property="image_url1", type="string"),
            *         @OA\Property(property="image_url2", type="string"),
            *         @OA\Property(property="image_url3", type="string"),
            *         @OA\Property(property="image_url4", type="string"),
            *         @OA\Property(property="image_url5", type="string"),
            *         @OA\Property(property="image_url6", type="string"),
            *         @OA\Property(property="status", type="integer"),
            *         @OA\Property(property="lastInspectionDate", type="string"),
            *         @OA\Property(property="lastRevisionDate", type="string"),
            *         @OA\Property(property="createdBy", type="integer"),
            *         @OA\Property(property="updatedBy", type="integer")
           
        *            ),
        *        ),
       
        *    ),
        *      @OA\Response(
        *          response=201,
        *          description="Agency vehicle data has been updated successfully",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=200,
        *          description="Agency vehicle data has been updated successfully",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=422,
        *          description="Unprocessable Entity",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        *      security={{"bearer_token":{}}} 
        * )
        */
    public function update(Request $request, $id)  
    {   $request = $request->all();
        $validator=Validator::make($request, [
            "vehicleCategoryId"=> "required|integer|max:255",
            "vehicleTypeId"=> "required|integer|max:255",
            "vehicleBrandId"=> "required|integer|max:255",
            "model"=> "required|string|max:255",
            "noOfDoors"=> "required|integer|max:255",
            "agencyId"=> "required|integer|max:255",
            "yearOfRegistration"=> "required|string|max:255",
            "motorization"=> "required|json|max:255",
            "registrationNo"=> "required|string|max:255",
            "gearBox"=> "required|json|max:255",
            "noOfSeats"=> "required|integer|max:255",
            "mileage"=> "required|string|max:255",
            "vehicleFeatures"=> "required|json|max:255",
            "ratePerDay"=> "required|string|max:255",
            "miniMumDay"=> "required|integer|max:255",
            "maxiMumDay"=> "required|integer|max:255",
            "status"=> "required|string|max:255",
            "lastInspectionDate"=> "required|string|max:255",
            "lastRevisionDate"=> "required|string|max:255",
            "createdBy"=> "integer|max:255|nullable",
            "updatedBy"=>"integer|max:255|nullable",
            "image_url1" =>"string|required",
            "image_url2" =>"string|required",
            "image_url3" =>"string|required",
            "image_url4" =>"string|required",
            "image_url5" =>"string|required",
            "image_url6" =>"required",
        ]);
        if($validator->fails()){
            return response()->json([
                "success" => false,
                "message" => $validator->errors(),
            ]); 
        }
        $agencyVehicles= AgencyVehicles::find($id);
       
        if(!empty($agencyVehicles)){
            $agencyVehicles->update($request);
            return response()->json([
                "success" => true,
                "message" => "Agency vehicle data has been updated successfully.",
                "data" => $agencyVehicles,
            ]);
        }else{
            return response()->json([
                "success" => false,
                "message" => "Data Not Found.",
            ]); 
        }
    } 
    
     /**
     * @OA\Delete(
     *      path="/api/delete_agencyVehicle/{id}",
     *      operationId="Delete Agency Vehicle",
     *      tags={"Agency-Vehicle"},
     *      summary="Delete Agency Vehicle",
     *      description="Delete Agency Vehicle",
     *      @OA\Parameter(
     *          name="id",
     *          description="Agency Vehicle Id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Agency Vehicle data has been deleted successfully.",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={{"bearer_token":{}}} 
     * )
     */ 
    public function delete($id)  
    { 
        $agencyVehicles=AgencyVehicles::find($id);
        if(!empty($agencyVehicles)){
            $agencyVehicles->status='0';
            $agencyVehicles->save();
            return response()->json([
                "success" => true,
                "message" => "Agency Vehicle data has been deleted successfully.",
                "data" => $agencyVehicles,
            ]);
        }else{
            return response()->json([
                "success" => false,
                "message" => "Data Not Found.",
            ]); 
        }
    }

    public function upload_vehicles_agency_files(Request $request)
    {   
        try{
            //https://www.itsolutionstuff.com/post/laravel-10-multiple-file-upload-tutorial-exampleexample.html
            $request->validate([
                'files' => 'required',
                'files.*' => 'required|mimes:pdf,xlx,jpg,png,csv|max:2048',
            ]);
            
            $files = [];
        
            if ($request->file('files')){
                if(!is_array($request->file('files'))){
                    return response()->json([
                        "success" => false,
                        "message" => "File upload fail.",
                    ]); 

                }else{
                foreach($request->file('files') as $key => $file)
                    {   
                        $fileName = time().rand(1,99).'.'.$file->extension();  
                        $file->move(public_path('uploads/agency_vehicles'), $fileName);
                        $files[]['name'] = 'agency_vehicles/'. $fileName;
                    }
                }
                
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "File upload fail.",
                ]); 
            }
            return response()->json([
                    "success" => true,
                    "message" => "You have successfully upload file.",
                    "data"=>$files,
                
            ]); 
            // foreach ($files as $key => $file) {
            //     File::create($file);
            // }
        
            // return back()
            //         ->with('success','You have successfully upload file.');
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
     
    }


   /**
     * @OA\Get(
     *      path="/api/vehicle_by_organizationId/{id}",
     *      operationId="Agency Vehicle Data By OrganizationId",
     *      tags={"Agency-Vehicle"},
     *      summary="Get Agency Vehicle",
     *      description="Returns Agency  Vehicle data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Organization Id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Agency Vehicle Data",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Agency Vehicle Data"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={{"bearer_token":{}}} 
     * )
     */
    public function vehicles_by_organizationId(Request $request){
        try{
            $input = $request->all();
            $query = AgencyVehicles::query();
            $query->select('agencyVehicles.*')->Join('agencies', 'agencyVehicles.agencyId', '=', 'agencies.id')->from('agency_vehicles as agencyVehicles');
            if(isset($input['searchKey']) && isset($input['searchVal']) && !empty($input['searchKey']) && !empty($input['searchVal'])){
                $query->where('agencyVehicles.'.$request->input('searchKey'), 'like', '%' . $request->input('searchVal') . '%');
            }
            if (isset($input['sortBy']) && !empty($input['sortBy'])) {
                $query->orderBy($input['sortBy'], $input['orderBy']);
            }
            if(isset($input['page']) && !empty($input['page']) && isset($input['limit']) && !empty($input['limit'])){
                $count=$query->count();
                $input['page']= (floor($count/$input['limit']))-1;
                $query->offset($input['page'])->limit($input['limit']);
            }
            if (isset($input['organizationId']) && !empty($input['organizationId'])) {
                $query->where('agencies.organizationId', $input['organizationId']);
            }
            $query->where('agencyVehicles.status', '1');
            $agencyVehicles=$query->get();
            if(!$agencyVehicles->isEmpty()){
                return response()->json([
                    "success" => true,
                    "message" => "Agency Vehicles List.",
                    "data" => $agencyVehicles,
                    "total_count"=>count($agencyVehicles),
                ]); 
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }        
    } 
}    