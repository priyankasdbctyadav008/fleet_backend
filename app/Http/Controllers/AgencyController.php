<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Agencies;
use Validator;
use Illuminate\Support\Facades\Auth;
//use App\Models\User;

class AgencyController extends Controller
{
    public function __construct(){    
        //$this->middleware('auth:api');
        if(!auth()->check()){
                 $data = response()->json([
                    "success" => false,
                    "message" => "Authentication Fail.",
                ])->getContent();
                header("Content-type:application/json");
                echo $data;
                die();
        }else{
            $user_role = Auth::user()->role;
            if($user_role !='1' && $user_role !='2'){
                $data = response()->json([
                    "success" => false,
                    "message" => "UNAUTHORIZED.",
                ])->getContent(); 
                header("Content-type:application/json");
                echo $data;
                return $data;
                die();
            }
        }
    }
    /**
        * @OA\Post(
        * path="/api/create_agency",
        * operationId="Create Agency",
        * tags={"Agency"},
        * summary="Create Agency",
        * description="Create Agency here",
        *     @OA\RequestBody(
        *         @OA\JsonContent(),
        *         @OA\MediaType(
        *            mediaType="multipart/form-data",
        *            @OA\Schema(
        *               type="object",
        *                required={"name","noOfRegisteredVehicles","agencyCode","postalCode","address1","city","agencyAvailbility","countryId","stateId","ispaymentgatewayEnabled","organizationId"},
                        *@OA\Property(property="name", type="string"),
                        *@OA\Property(property="noOfRegisteredVehicles", type="integer"),
                        *@OA\Property(property="agencyCode", type="string"),
                        *@OA\Property(property="postalCode", type="string"),
                        *@OA\Property(property="address1", type="string"),
                        *@OA\Property(property="city", type="string"),
                        *@OA\Property(property="paymentGatewaySecretKey", type="string"),
                        *@OA\Property(property="paymentGatewayPublickey", type="string"),
                        *@OA\Property(property="agencyAvailbility", type="json", format="json", example={"mondayStartTime": "10:51:31","mondayEndTime": "12:51:31","tuesdayStartTime": "10:51:31","tuesdayEndTime": "","wednesdayStartTime": "10:51:31","wednesdayEndTime": "10:51:31","thursdayStartTime": "10:51:31","thursdayEndTime": "10:51:31","fridayStartTime": "","fridayEndTime": "10:51:31","saturdayStartTime": "10:51:31","saturdayEndTime": "10:51:31","sundayStartTime": "08:51:31","sundayEndTime": "13:51:31"}),
                        *@OA\Property(property="countryId", type="integer"),
                        *@OA\Property(property="stateId", type="integer"),
                        *@OA\Property(property="ispaymentgatewayEnabled", type="integer"),
                        *@OA\Property(property="organizationId", type="integer"),
                       
                        
        *            ),
        *        ),
        *    ),
        *      @OA\Response(
        *          response=201,
        *          description="Agency created successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=200,
        *          description="Agency created successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=422,
        *          description="Unprocessable Entity",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        *      security={{"bearer_token":{}}} 
        * )
        */
    public function create(Request $request){
        try{
            $input = $request->all();
            
            $validator = Validator::make($input, [
                'name'=> "required|string|max:255",
                'noOfRegisteredVehicles'=> "required|integer|max:255",
                'agencyCode'=> "required|string|max:255",
                'postalCode'=> "required|string|max:255",
                'address1'=> "required|string|max:255",
                'city'=> "required|string|max:255",
                'paymentGatewaySecretKey'=> "string|max:255|nullable",
                'paymentGatewayPublickey'=> "string|max:255|nullable",
                'agencyAvailbility'=> "required|json",
                'countryId'=> "required|integer",
                'stateId'=> "required|integer",
                //'publicName'=> "required|string|max:255",
                //'stripeSecretKey'=> "required|string|max:255",
                //'stripePublickey'=> "required|string|max:255",
                'ispaymentgatewayEnabled'=> "required|integer|max:255",
                'organizationId'=> "required|integer|max:255",
                //'status'=>  "required|string|max:255",
                //'createdBy'=> "integer|max:255|nullable",
                //'updatedBy'=> "integer|max:255|nullable ",
            ]);
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" => $validator->errors(),
                ]); 
            }
            $agencies = Agencies::create($input);
            return response()->json([
                "success" => true,
                "message" => "Agency created successfully.",
                "data" => $agencies
            ]);
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }
    }
    /**
    * @OA\Get(
    * path="/api/list_agency",
    * operationId="Agency List",
    * tags={"Agency"},
    * summary="Agency List",
    * description="Agency list here",
     * @OA\Parameter(
     *          name="page",
     *          description="Page no.",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *  @OA\Parameter(
     *          name="limit",
     *          description="Limit",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *  @OA\Parameter(
     *          name="sortBy",
     *          description="SortBy",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *  @OA\Parameter(
     *          name="orderBy",
     *          description="OrderBy",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *   @OA\Parameter(
     *          name="searchKey",
     *          description="SearchKey",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *   @OA\Parameter(
     *          name="searchVal",
     *          description="SearchVal",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     
    *      @OA\Response(
    *          response=201,
    *          description="Agency List.",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=200,
    *          description="Agency List.",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=422,
    *          description="Unprocessable Entity",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    *     security={{"bearer_token":{}}} 
    * )
    */
    public function list(Request $request){
        try{
            $input = $request->all();
        
            $query = Agencies::query()->where('status','1');
            // if(isset($input['name'])  && !empty($input['name'])){
            //     $query->where('name', 'like', '%' . $input['name'] . '%');
            // }
            if(isset($input['searchKey']) && isset($input['searchVal']) && !empty($input['searchKey']) && !empty($input['searchVal'])){
                $query->where($request->input('searchKey'), 'like', '%' . $request->input('searchVal') . '%');
            }
            if(isset($input['organizationId'])  && !empty($input['organizationId'])){
                $query->where('organizationId', 'like', '%' . $input['organizationId'] . '%');
            }
            if (isset($input['sortBy']) && !empty($input['sortBy'])) {
                $query->orderBy($input['sortBy'], $input['orderBy']);
            }
            if(isset($input['page']) && !empty($input['page']) && isset($input['limit']) && !empty($input['limit'])){
                $count=$query->count();
                $input['page']= (floor($count/$input['limit']))-1;
                $query->offset($input['page'])->limit($input['limit'])->get();
            }
            $agencies= $query->get();
            if(!$agencies->isEmpty()){
                return response()->json([
                    "success" => true,
                    "message" => "Agency List.",
                    "data" => $agencies,
                    "total_count"=>count($agencies),
                ]); 
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }
    } 
    /**
     * @OA\Get(
     *      path="/api/agency/{id}",
     *      operationId="Agency",
     *      tags={"Agency"},
     *      summary="Get Agency",
     *      description="Returns Agency data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Agency Id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Agency Data",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Agency Data"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={{"bearer_token":{}}} 
     * )
     */
    public function find($id){
        try{
            $agencies = Agencies::find($id);
            $agencies = compact('agencies');
            if(!empty($agencies['agencies'])){
                return response()->json([
                    "success" => true,
                    "message" => "Agency Data",
                    "data" => $agencies,
                    "total_count"=>count($agencies),
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }
    }
    /**
        * @OA\Post(
        * path="/api/update_agency/{id}",
        * operationId="Update Agency",
        * tags={"Agency"},
        * summary="Update Agency",
        * description="Update Agency here",
        *      @OA\Parameter(
        *          name="id",
        *          description="Agency Id",
        *          required=true,
        *          in="path",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *     @OA\RequestBody(
        *         @OA\JsonContent(
        *               required={"name","noOfRegisteredVehicles","agencyCode","postalCode","address1","city","agencyAvailbility","countryId","stateId","ispaymentgatewayEnabled","organizationId"},
                        *@OA\Property(property="name", type="string"),
                        *@OA\Property(property="noOfRegisteredVehicles", type="integer"),
                        *@OA\Property(property="agencyCode", type="string"),
                        *@OA\Property(property="postalCode", type="string"),
                        *@OA\Property(property="address1", type="string"),
                        *@OA\Property(property="city", type="string"),
                        *@OA\Property(property="paymentGatewaySecretKey", type="string"),
                        *@OA\Property(property="paymentGatewayPublickey", type="string"),
                        *@OA\Property(property="agencyAvailbility", type="json", format="json", example={"mondayStartTime": "10:51:31","mondayEndTime": "12:51:31","tuesdayStartTime": "10:51:31","tuesdayEndTime": "","wednesdayStartTime": "10:51:31","wednesdayEndTime": "10:51:31","thursdayStartTime": "10:51:31","thursdayEndTime": "10:51:31","fridayStartTime": "","fridayEndTime": "10:51:31","saturdayStartTime": "10:51:31","saturdayEndTime": "10:51:31","sundayStartTime": "08:51:31","sundayEndTime": "13:51:31"}),
                        *@OA\Property(property="countryId", type="integer"),
                        *@OA\Property(property="stateId", type="integer"),
                        *@OA\Property(property="ispaymentgatewayEnabled", type="integer"),
                        *@OA\Property(property="organizationId", type="integer"),
                        
        *),
       
        *    ),
        *      @OA\Response(
        *          response=201,
        *          description="Agency data has been updated successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=200,
        *          description="Agency data has been updated successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=422,
        *          description="Unprocessable Entity",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        *      security={{"bearer_token":{}}} 
        * )
        */
    public function update(Request $request, $id)  
    {   try{
            $request = $request->all();
            $validator=Validator::make($request, [
                'name'=> "required|string|max:255",
                'noOfRegisteredVehicles'=> "required|integer|max:255",
                'agencyCode'=> "required|string|max:255",
                'postalCode'=> "required|string|max:255",
                'address1'=> "required|string|max:255",
                'city'=> "required|string|max:255",
                'paymentGatewaySecretKey'=> "string|max:255",
                'paymentGatewayPublickey'=> "string|max:255",
                'agencyAvailbility'=> "required|json",
                'countryId'=> "required|integer",
                'stateId'=> "required|integer",
                // 'publicName'=> "required|string|max:255",
                // 'stripeSecretKey'=> "required|string|max:255",
                // 'stripePublickey'=> "required|string|max:255",
                'ispaymentgatewayEnabled'=> "required|integer|max:255",
                'organizationId'=> "required|integer|max:255",
                // 'status'=> "required|string",
                // 'CreatedBy'=> "integer|max:255|nullable",
                // 'UpdatedBy'=> "integer|max:255|nullable",
            ]);
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" => $validator->errors(),
                ]); 
            }
            $agencies=Agencies::find($id);
        
            if(!empty($agencies)){
                $agencies->update($request);
                return response()->json([
                    "success" => true,
                    "message" => "Agency data has been updated successfully.",
                    "data" => $agencies,
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    }  
     /**
     * @OA\Delete(
     *      path="/api/delete_agency/{id}",
     *      operationId="Delete Agency",
     *      tags={"Agency"},
     *      summary="Delete Agency",
     *      description="Delete Agency",
     *      @OA\Parameter(
     *          name="id",
     *          description="Agency Id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Agency data has been deleted successfully.",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={{"bearer_token":{}}} 
     * )
     */ 
    public function delete($id)  
    {   
        try{
            $agencies=Agencies::find($id);
            if(!empty($agencies)){
                $agencies->status='0';
                $agencies->save();
                return response()->json([
                    "success" => true,
                    "message" => "Agency data has been deleted successfully.",
                    "data" => $agencies,
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    }

    /**
     * @OA\Get(
     *      path="/api/agency_dropDown/{orgId}",
     *      operationId="Agency DropDown",
     *      tags={"Agency"},
     *      summary="Get Agency DropDown",
     *      description="Returns Agency dropDown data",
     *      @OA\Parameter(
     *          name="orgId",
     *          description="orgId Id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Agency dropDown Data",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Agency dropDown Data"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={{"bearer_token":{}}} 
     * )
     */
    public function find_dropDown($orgId)  {
        try{
            $query = Agencies::query();
            $query->where('organizationId', $orgId);
            $agencies= $query->get();
            if(!empty($agencies)){
                return response()->json([
                    "success" => true,
                    "message" => "Agency dropDown List.",
                    "data" => $agencies,
                    "total_count"=>count($agencies),
                ]); 
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }  
    }
    public function upload_agency_files(Request $request)
    {   
        try{
            //https://www.itsolutionstuff.com/post/laravel-10-multiple-file-upload-tutorial-exampleexample.html
            $request->validate([
                'files' => 'required',
                'files.*' => 'required|mimes:pdf,xlx,jpg,png,csv|max:2048',
            ]);
        
            $files = [];
            if ($request->file('files')){
                foreach($request->file('files') as $key => $file)
                {   
                    $fileName = time().rand(1,99).'.'.$file->extension();  
                    $file->move(public_path('uploads/agency'), $fileName);
                    $files[]['name'] = $fileName;
                }
            }
            return response()->json([
                    "success" => true,
                    "message" => "You have successfully upload file.",
                
            ]); 
            // foreach ($files as $key => $file) {
            //     File::create($file);
            // }
        
            // return back()
            //         ->with('success','You have successfully upload file.');
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }      
     
    }

    /**
     * @OA\Get(
     *      path="/api/agency_by_organizationId/{id}",
     *      operationId="Agency by organizationId",
     *      tags={"Agency"},
     *      summary="Get Agency By OrganizationId",
     *      description="Returns Agency data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Organization Id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Agency Data",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Agency Data"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={{"bearer_token":{}}} 
     * )
     */
    public function agency_by_organizationId($id){
        try{
            $agencies = Agencies::where('organizationId', $id)->get();
            //$agencies = compact('agencies');
            if(!empty($agencies)){
                return response()->json([
                    "success" => true,
                    "message" => "Agency Data",
                    "data" => $agencies,
                    "total_count"=>count($agencies),
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }
    }
    
}
