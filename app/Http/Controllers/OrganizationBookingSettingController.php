<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\OrganizationBookingSetting;
use Validator;
use Illuminate\Support\Facades\Auth;

class OrganizationBookingSettingController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth:api');
        if(!auth()->check()){
                 $data = response()->json([
                    "success" => false,
                    "message" => "Authentication Fail.",
                ])->getContent();; 
                header("Content-type:application/json");
                echo $data;
                die();
        }else{
            $user_role = Auth::user()->role;
            if($user_role !='1' && $user_role !='2'){
                $data = response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
                header("Content-type:application/json");
                echo $data;
                return $data;
                die();
            }
        }
    }
    /**
        * @OA\Post(
        * path="/api/create_organizationBookingSetting",
        * operationId="Create Organization Booking Setting",
        * tags={"Organization-Booking-Setting"},
        * summary="Create Organization Booking Setting",
        * description="Create Organization Booking Setting here",
        *     @OA\RequestBody(
        *         @OA\JsonContent(
        *               required={"organizationId","informAboutRentalBeforeDays","minRentalPeriodInDays","rentalReserveBeforeMonths","allowOverBooking","allowEditPriceManually","allowPastDateReservation","isAutoGenerateBookingRef","authorizeOutsideReservationArea","createdBy","updatedBy"},
                       * @OA\Property(property="organizationId", type="integer"),
* @OA\Property(property="informAboutRentalBeforeDays", type="integer"),
* @OA\Property(property="minRentalPeriodInDays", type="integer"),
* @OA\Property(property="rentalReserveBeforeMonths", type="integer"),
* @OA\Property(property="allowOverBooking", type="integer"),
* @OA\Property(property="allowEditPriceManually", type="integer"),
* @OA\Property(property="allowPastDateReservation", type="integer"),
* @OA\Property(property="isAutoGenerateBookingRef", type="integer"),
* @OA\Property(property="authorizeOutsideReservationArea", type="integer"),
* @OA\Property(property="createdBy", type="integer"),
* @OA\Property(property="updatedBy", type="integer")
        *),
        *         @OA\MediaType(
        *            mediaType="multipart/form-data",
        *            @OA\Schema(
        *               type="object",
        *               required={"organizationId","informAboutRentalBeforeDays","minRentalPeriodInDays","rentalReserveBeforeMonths","allowOverBooking","allowEditPriceManually","allowPastDateReservation","isAutoGenerateBookingRef","authorizeOutsideReservationArea","createdBy","updatedBy"},
                        * @OA\Property(property="organizationId", type="integer"),
* @OA\Property(property="informAboutRentalBeforeDays", type="integer"),
* @OA\Property(property="minRentalPeriodInDays", type="integer"),
* @OA\Property(property="rentalReserveBeforeMonths", type="integer"),
* @OA\Property(property="allowOverBooking", type="integer"),
* @OA\Property(property="allowEditPriceManually", type="integer"),
* @OA\Property(property="allowPastDateReservation", type="integer"),
* @OA\Property(property="isAutoGenerateBookingRef", type="integer"),
* @OA\Property(property="authorizeOutsideReservationArea", type="integer"),
* @OA\Property(property="createdBy", type="integer"),
* @OA\Property(property="updatedBy", type="integer")
                        
        *            ),
        *        ),
        *    ),
        *      @OA\Response(
        *          response=201,
        *          description="Organization booking setting created successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=200,
        *          description="Organization booking setting created successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=422,
        *          description="Unprocessable Entity",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        *      security={{"bearer_token":{}}} 
        * )
        */
    public function create(Request $request){
        try {
            $input = $request->all();
            
            $validator = Validator::make($input, [
                "organizationId"=> "required|integer|max:255",
                "informAboutRentalBeforeDays"=> "required|integer|max:255",
                "minRentalPeriodInDays"=> "required|integer|max:255",
                "rentalReserveBeforeMonths"=> "required|integer|max:255",
                "allowOverBooking"=> "required|integer|max:255",
                "allowEditPriceManually"=> "required|integer|max:255",
                "allowPastDateReservation"=> "required|integer|max:255",
                "isAutoGenerateBookingRef"=> "required|integer|max:255",
                "authorizeOutsideReservationArea"=> "required|integer|max:255",
                "createdBy"=> "required|integer|max:255",
                "updatedBy"=> "required|integer|max:255",
            ]);
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" => $validator->errors(),
                ]); 
            }
            $organizationBookingSetting = OrganizationBookingSetting::create($input);
            return response()->json([
                "success" => true,
                "message" => "Organization booking setting created successfully.",
                "data" => $organizationBookingSetting
            ]);
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
    
        }
    }
      /**
    * @OA\Get(
    * path="/api/list_organizationBookingSetting",
    * operationId="Organization Booking Setting List",
    * tags={"Organization-Booking-Setting"},
    * summary="Organization Booking Setting List",
    * description="Organization Booking Setting List here",
    *      @OA\Response(
    *          response=201,
    *          description="Organization booking setting List.",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=200,
    *          description="Organization booking setting List.",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=422,
    *          description="Unprocessable Entity",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    *     security={{"bearer_token":{}}} 
    * )
    */
    public function list(Request $request){
        try{
            $input = $request->all();
        
            $query = OrganizationBookingSetting::query();
            // if(isset($input['searchKey']) && isset($input['searchVal']) && !empty($input['searchKey']) && !empty($input['searchVal'])){
            //     $query->where($request->input('searchKey'), 'like', '%' . $request->input('searchVal') . '%');
            // }
        
            // if (isset($input['sortBy']) && !empty($input['sortBy'])) {
            //     $query->orderBy($input['sortBy'], $input['orderBy']);
            // }
            // if(isset($input['page']) && !empty($input['page']) && isset($input['limit']) && !empty($input['limit'])){
            //     $count=$query->count();
            //     $input['page']= (floor($count/$input['limit']))-1;
            //     $query->offset($input['page'])->limit($input['limit'])->get();
            // }
            $organizationBookingSetting= $query->get();
            //print_r($vehiclesCategoryMst); die();
            if(!$organizationBookingSetting->isEmpty()){
                return response()->json([
                    "success" => true,
                    "message" => "Organization booking setting List.",
                    "data" => $organizationBookingSetting,
                    "total_count"=>count($organizationBookingSetting),
                ]); 
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }
    } 
     /**
     * @OA\Get(
     *      path="/api/organizationBookingSetting/{id}",
     *      operationId="Organization Booking Setting Find",
     *      tags={"Organization-Booking-Setting"},
     *      summary="Get Organization Booking Setting",
     *      description="Returns Organization Booking Setting Data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Organization Booking Setting Id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Organization booking setting Data",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Organization booking setting Data"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={{"bearer_token":{}}} 
     * )
     */
    public function find($id){
        try{
            $organizationBookingSetting = OrganizationBookingSetting::find($id);
            $organizationBookingSetting = compact('organizationBookingSetting');
            
            if(!empty($organizationBookingSetting['organizationBookingSetting'])){
                return response()->json([
                    "success" => true,
                    "message" => "Organization booking setting Data",
                    "data" => $organizationBookingSetting,
                    "total_count"=>count($organizationBookingSetting),
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }       
    }
   /**
        * @OA\Post(
        * path="/api/update_organizationBookingSetting/{id}",
        * operationId="Update Organization Booking Setting",
        * tags={"Organization-Booking-Setting"},
        * summary="Update Organization Booking Setting",
        * description="Update Organization Booking Setting here",
         *      @OA\Parameter(
        *          name="id",
        *          description="Organization Booking Setting Id",
        *          required=true,
        *          in="path",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *     @OA\RequestBody(
        *         @OA\JsonContent(
        *               required={"organizationId","informAboutRentalBeforeDays","minRentalPeriodInDays","rentalReserveBeforeMonths","allowOverBooking","allowEditPriceManually","allowPastDateReservation","isAutoGenerateBookingRef","authorizeOutsideReservationArea","createdBy","updatedBy"},
                       * @OA\Property(property="organizationId", type="integer"),
* @OA\Property(property="informAboutRentalBeforeDays", type="integer"),
* @OA\Property(property="minRentalPeriodInDays", type="integer"),
* @OA\Property(property="rentalReserveBeforeMonths", type="integer"),
* @OA\Property(property="allowOverBooking", type="integer"),
* @OA\Property(property="allowEditPriceManually", type="integer"),
* @OA\Property(property="allowPastDateReservation", type="integer"),
* @OA\Property(property="isAutoGenerateBookingRef", type="integer"),
* @OA\Property(property="authorizeOutsideReservationArea", type="integer"),
* @OA\Property(property="createdBy", type="integer"),
* @OA\Property(property="updatedBy", type="integer")
        *),
        *         @OA\MediaType(
        *            mediaType="multipart/form-data",
        *            @OA\Schema(
        *               type="object",
        *               required={"organizationId","informAboutRentalBeforeDays","minRentalPeriodInDays","rentalReserveBeforeMonths","allowOverBooking","allowEditPriceManually","allowPastDateReservation","isAutoGenerateBookingRef","authorizeOutsideReservationArea","createdBy","updatedBy"},
                        * @OA\Property(property="organizationId", type="integer"),
* @OA\Property(property="informAboutRentalBeforeDays", type="integer"),
* @OA\Property(property="minRentalPeriodInDays", type="integer"),
* @OA\Property(property="rentalReserveBeforeMonths", type="integer"),
* @OA\Property(property="allowOverBooking", type="integer"),
* @OA\Property(property="allowEditPriceManually", type="integer"),
* @OA\Property(property="allowPastDateReservation", type="integer"),
* @OA\Property(property="isAutoGenerateBookingRef", type="integer"),
* @OA\Property(property="authorizeOutsideReservationArea", type="integer"),
* @OA\Property(property="createdBy", type="integer"),
* @OA\Property(property="updatedBy", type="integer")
                        
        *            ),
        *        ),
        *    ),
        *      @OA\Response(
        *          response=201,
        *          description="Organization booking setting data has been updated successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=200,
        *          description="Organization booking setting data has been updated successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=422,
        *          description="Unprocessable Entity",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        *      security={{"bearer_token":{}}} 
        * )
        */
    public function update(Request $request, $id)  
    {   try{
            $request = $request->all();
            $validator=Validator::make($request, [
                "organizationId"=> "required|integer|max:255",
                "informAboutRentalBeforeDays"=> "required|integer|max:255",
                "minRentalPeriodInDays"=> "required|integer|max:255",
                "rentalReserveBeforeMonths"=> "required|integer|max:255",
                "allowOverBooking"=> "required|integer|max:255",
                "allowEditPriceManually"=> "required|integer|max:255",
                "allowPastDateReservation"=> "required|integer|max:255",
                "isAutoGenerateBookingRef"=> "required|integer|max:255",
                "authorizeOutsideReservationArea"=> "required|integer|max:255",
                "createdBy"=> "required|integer|max:255",
                "updatedBy"=> "required|integer|max:255",
            ]);
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" => $validator->errors(),
                ]); 
            }
            $organizationBookingSetting= OrganizationBookingSetting::find($id);
        
            if(!empty($organizationBookingSetting)){
                $organizationBookingSetting->update($request);
                return response()->json([
                    "success" => true,
                    "message" => "Organization booking setting data has been updated successfully.",
                    "data" => $organizationBookingSetting,
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    }  
     /**
     * @OA\Delete(
     *      path="/api/delete_organizationBookingSetting/{id}",
     *      operationId="Delete Organization Booking Setting",
     *      tags={"Organization-Booking-Setting"},
     *      summary="Delete Organization Booking Setting",
     *      description="Delete Organization Booking Setting",
     *      @OA\Parameter(
     *          name="id",
     *          description="Organization Booking Setting Id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Organization booking setting data has been deleted successfully.",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={{"bearer_token":{}}} 
     * )
     */ 
    public function delete($id)  
    {   
        try{
            $organizationBookingSetting=OrganizationBookingSetting::find($id);
            if(!empty($organizationBookingSetting)){
                $organizationBookingSetting->status='0';
                $organizationBookingSetting->save();
                return response()->json([
                    "success" => true,
                    "message" => "Organization booking setting data has been deleted successfully.",
                    "data" => $organizationBookingSetting,
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    }
     
}
