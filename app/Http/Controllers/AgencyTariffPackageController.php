<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AgencyTariffPackage;
use Validator;
use Illuminate\Support\Facades\Auth;


class AgencyTariffPackageController extends Controller
{
      public function __construct()
    {
        //$this->middleware('auth:api');
        if(!auth()->check()){
                 $data = response()->json([
                    "success" => false,
                    "message" => "Authentication Fail.",
                ])->getContent();; 
                header("Content-type:application/json");
                echo $data;
                die();
        }else{
            $user_role = Auth::user()->role;
            if($user_role !='1' && $user_role !='2'){
                $data = response()->json([
                    "success" => false,
                    "message" => "UNAUTHORIZED.",
                ]); 
                header("Content-type:application/json");
                echo $data;
                return $data;
                die();
            }
        }
    }
        /**
        * @OA\Post(
        * path="/api/create_agencyTariffPackage",
        * operationId="Create Agency Tariff Package",
        * tags={"Agency-tariff-package"},
        * summary="Create Agency Tariff Package",
        * description="Create agency tariff package here",
        *     @OA\RequestBody(
        *         @OA\JsonContent(
        
        *         ),
        *         @OA\MediaType(
        *                mediaType="multipart/form-data",
        *                @OA\Schema(
        *                      type="object",
        *                      required={"packageName","isStartMonday","isStartTuesday","isStartWednesday","isStartThursday","isStartFriday","isStartSaturday","isStartSunday","packageDurationInDays","mileageUnit","isUnlimitedMileage","mileageIncluded","amount","vehiclesCategoryId","vehicleTypeId"},
                              *@OA\Property(property="packageName", type="string"),
                              *@OA\Property(property="isStartMonday", type="boolean"),
                              *@OA\Property(property="isStartTuesday", type="boolean"),
                              *@OA\Property(property="isStartWednesday", type="boolean"),
                              *@OA\Property(property="isStartThursday", type="boolean"),
                              *@OA\Property(property="isStartFriday", type="boolean"),
                              *@OA\Property(property="isStartSaturday", type="boolean"),
                              *@OA\Property(property="isStartSunday", type="boolean"),
                              *@OA\Property(property="packageDurationInDays", type="integer"),
                              *@OA\Property(property="mileageUnit", type="string"),
                              *@OA\Property(property="isUnlimitedMileage", type="boolean"),
                              *@OA\Property(property="mileageIncluded", type="integer"),
                              *@OA\Property(property="amount", type="string"),
                              *@OA\Property(property="vehiclesCategoryId", type="integer"),
                              *@OA\Property(property="vehicleTypeId", type="integer"),
                              
        *               ),
        *         ),
        *   ),
        *   @OA\Response(
        *          response=201,
        *          description="Agency tariff package created successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=200,
        *          description="Agency tariff package created successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=422,
        *          description="Unprocessable Entity",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        *      security={{"bearer_token":{}}} 
        * )
        */
    public function create(Request $request){
        try {
            $input = $request->all();
          
            $input['isStartMonday'] = $input['isStartMonday']? "true":"false";
            $input['isStartTuesday'] = $input['isStartTuesday']? "true":"false";
            $input['isStartWednesday'] = $input['isStartWednesday']? "true":"false";
            $input['isStartThursday'] = $input['isStartThursday']? "true":"false";
            $input['isStartFriday'] = $input['isStartFriday']? "true":"false";
            $input['isStartSaturday'] = $input['isStartSaturday']? "true":"false";
            $input['isStartSunday'] = $input['isStartSunday'] ? "true":"false";
            $input['isUnlimitedMileage']= $input['isUnlimitedMileage'] ? "true":"false";
          
            $validator = Validator::make($input,[ 
                "packageName"=> "required|string|max:255",
                "isStartMonday"=> "required|in:true,false",
                "isStartTuesday"=> "required|in:true,false",
                "isStartWednesday"=> "required|in:true,false",
                "isStartThursday"=> "required|in:true,false",
                "isStartFriday"=> "required|in:true,false",
                "isStartSaturday"=> "required|in:true,false",
                "isStartSunday"=> "required|in:true,false",
                "packageDurationInDays"=> "required|integer",
                "mileageUnit"=> "required|string",
                "isUnlimitedMileage"=> "required|in:true,false",
                "mileageIncluded"=>"required|integer",
                "amount"=> "required|string",
                "vehiclesCategoryId"=> "required|integer",
                "vehicleTypeId"=> "required|integer",
               // "status"=> "string|nullable",
                //"createdBy"=> "integer|nullable",
                //"updatedBy"=> "integer|nullable",
            ]);
           
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" => $validator->errors(),
                ]); 
            }
            $agencyTariffPackage = AgencyTariffPackage::create($input);
            return response()->json([
                "success" => true,
                "message" => "Agency tariff package created successfully.",
                "data" => $agencyTariffPackage
            ]);
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
    
        }
    }
      /**
    * @OA\Get(
    * path="/api/list_agencyTariffPackage",
    * operationId="Agency Tariff Package List",
    * tags={"Agency-tariff-package"},
    * summary="Agency Tariff Package List",
    * description="Agency Tariff Package list here",
    *      @OA\Response(
    *          response=201,
    *          description="Agency tariff package List.",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=200,
    *          description="Agency tariff package List.",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=422,
    *          description="Unprocessable Entity",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    *     security={{"bearer_token":{}}} 
    * )
    */
    public function list(Request $request){
        try{
            $input = $request->all();
            $query = AgencyTariffPackage::query()->where('status','Active');
            // if(isset($input['searchKey']) && isset($input['searchVal']) && !empty($input['searchKey']) && !empty($input['searchVal'])){
            //     $query->where($request->input('searchKey'), 'like', '%' . $request->input('searchVal') . '%');
            // }
        
            // if (isset($input['sortBy']) && !empty($input['sortBy'])) {
            //     $query->orderBy($input['sortBy'], $input['orderBy']);
            // }
            // if(isset($input['page']) && !empty($input['page']) && isset($input['limit']) && !empty($input['limit'])){
            //     $count=$query->count();
            //     $input['page']= (floor($count/$input['limit']))-1;
            //     $query->offset($input['page'])->limit($input['limit'])->get();
            // }
            $agencyTariffPackage= $query->get();

            if(!empty($agencyTariffPackage)){
                return response()->json([
                    "success" => true,
                    "message" => "Agency tariff package List.",
                    "data" => $agencyTariffPackage,
                    "total_count"=>count($agencyTariffPackage),
                ]); 
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
            return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    } 
    /**
     * @OA\Get(
     *      path="/api/agencyTariffPackage/{id}",
     *      operationId="Agency Tariff Package Find",
     *      tags={"Agency-tariff-package"},
     *      summary="Agency Tariff Package",
     *      description="Returns Agency Tariff Package data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Agency Tariff Package Id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Agency tariff package Data",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Agency tariff package Data"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={{"bearer_token":{}}} 
     * )
     */
    public function find($id){
        try{
            $agencyTariffPackage = AgencyTariffPackage::find($id);
            $agencyTariffPackage = compact('agencyTariffPackage');
            if(!empty($agencyTariffPackage['agencyTariffPackage'])){
                return response()->json([
                    "success" => true,
                    "message" => "Agency tariff package Data",
                    "data" => $agencyTariffPackage,
                    "total_count"=>count($agencyTariffPackage),
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    }
    /**
        * @OA\Post(
        * path="/api/update_agencyTariffPackage/{id}",
        * operationId="Update Agency Tariff Package",
        * tags={"Agency-tariff-package"},
        * summary="Update Agency Tariff Package",
        * description="Update agency tariff package here",
          *      @OA\Parameter(
        *          name="id",
        *          description="Agency Tariff Package Id",
        *          required=true,
        *          in="path",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *     @OA\RequestBody(
        *         @OA\JsonContent(
        
        *         ),
        *         @OA\MediaType(
        *                mediaType="multipart/form-data",
        *                @OA\Schema(
       *                      type="object",
        *                      required={"packageName","isStartMonday","isStartTuesday","isStartWednesday","isStartThursday","isStartFriday","isStartSaturday","isStartSunday","packageDurationInDays","mileageUnit","isUnlimitedMileage","mileageIncluded","amount","vehiclesCategoryId","vehicleTypeId"},
                              *@OA\Property(property="packageName", type="string"),
                              *@OA\Property(property="isStartMonday", type="boolean"),
                              *@OA\Property(property="isStartTuesday", type="boolean"),
                              *@OA\Property(property="isStartWednesday", type="boolean"),
                              *@OA\Property(property="isStartThursday", type="boolean"),
                              *@OA\Property(property="isStartFriday", type="boolean"),
                              *@OA\Property(property="isStartSaturday", type="boolean"),
                              *@OA\Property(property="isStartSunday", type="boolean"),
                              *@OA\Property(property="packageDurationInDays", type="integer"),
                              *@OA\Property(property="mileageUnit", type="string"),
                              *@OA\Property(property="isUnlimitedMileage", type="boolean"),
                              *@OA\Property(property="mileageIncluded", type="integer"),
                              *@OA\Property(property="amount", type="string"),
                              *@OA\Property(property="vehiclesCategoryId", type="integer"),
                              *@OA\Property(property="vehicleTypeId", type="integer")
                             
        *               ),
        *         ),
        *   ),
        *   @OA\Response(
        *          response=201,
        *          description="Agency tariff package created successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=200,
        *          description="Agency tariff package created successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=422,
        *          description="Unprocessable Entity",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        *      security={{"bearer_token":{}}} 
        * )
        */
    public function update(Request $request, $id)  
    {   try{
            $request = $request->all();
            $request['isStartMonday'] = $request['isStartMonday']? "true":"false";
            $request['isStartTuesday'] = $request['isStartTuesday']? "true":"false";
            $request['isStartWednesday'] = $request['isStartWednesday']? "true":"false";
            $request['isStartThursday'] = $request['isStartThursday']? "true":"false";
            $request['isStartFriday'] = $request['isStartFriday']? "true":"false";
            $request['isStartSaturday'] = $request['isStartSaturday']? "true":"false";
            $request['isStartSunday'] = $request['isStartSunday'] ? "true":"false";
            $request['isUnlimitedMileage']= $request['isUnlimitedMileage'] ? "true":"false";
            
            $validator=Validator::make($request, [
                "packageName"=> "required|string|max:255",
                "isStartMonday"=> "required|in:true,false",
                "isStartTuesday"=> "required|in:true,false",
                "isStartWednesday"=> "required|in:true,false",
                "isStartThursday"=> "required|in:true,false",
                "isStartFriday"=> "required|in:true,false",
                "isStartSaturday"=> "required|in:true,false",
                "isStartSunday"=> "required|in:true,false",
                "packageDurationInDays"=> "required|integer",
                "mileageUnit"=> "required|string",
                "isUnlimitedMileage"=> "required|in:true,false",
                "mileageIncluded"=>"required|integer",
                "amount"=> "required|string",
                "vehiclesCategoryId"=> "required|integer",
                "vehicleTypeId"=> "required|integer",
                // "status"=> "string",
                // "createdBy"=> "integer|max:255",
                // "updatedBy"=> "integer|max:255",
            ]);
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" => $validator->errors(),
                ]); 
            }
            $agencyTariffPackage= AgencyTariffPackage::find($id);
        
            if(!empty($agencyTariffPackage)){
                $agencyTariffPackage->update($request);
                return response()->json([
                    "success" => true,
                    "message" => "Agency tariff package data has been updated successfully.",
                    "data" => $agencyTariffPackage,
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    }  

     /**
     * @OA\Delete(
     *      path="/api/delete_agencyTariffPackage/{id}",
     *      operationId="Delete Agency Tariff Package",
     *      tags={"Agency-tariff-package"},
     *      summary="Delete Agency Tariff Package",
     *      description="Delete Agency Tariff Package",
     *      @OA\Parameter(
     *          name="id",
     *          description="Agency Tariff Package Id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Agency tariff package data has been deleted successfully.",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={{"bearer_token":{}}} 
     * )
     */ 
    public function delete($id)  
    {   try{
            $agencyTariffPackage=AgencyTariffPackage::find($id);
            if(!empty($agencyTariffPackage)){
                $agencyTariffPackage->status='Deleted';
                $agencyTariffPackage->save();
                return response()->json([
                    "success" => true,
                    "message" => "Agency tariff package data has been deleted successfully.",
                    "data" => $agencyTariffPackage,
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    }
    
}
