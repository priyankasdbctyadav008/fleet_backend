<?php

namespace App\Http\Controllers;
use App\Models\Organizations;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\DB;
class OrganizationsController extends Controller
{
    public function __construct()
    {
       // $this->middleware('auth:api');
        if(!auth()->check()){
            $data = response()->json([
                "success" => false,
                "message" => "Authentication Fail.",
            ])->getContent(); 
            header("Content-type:application/json");
            echo $data;
            die();
        }else{
            $user_role = Auth::user()->role;
            if($user_role !='1' && $user_role !='2'){
                $data = response()->json([
                    "success" => false,
                    "message" => "UNAUTHORIZED.",
                ]); 
                header("Content-type:application/json");
                echo $data;
                return $data;
                die();
            }
        }
    }
     /**
        * @OA\Post(
        * path="/api/create_organization",
        * operationId="Create Organization",
        * tags={"Organization"},
        * summary="Create Organization",
        * description="Create Organization here",
        *     @OA\RequestBody(
        *         @OA\JsonContent(
        *           required={"name","address","city","postalCode","prefferedCurrencies","countryId","stateId","LogoUrl","userId"},
        *        @OA\Property(property="name", type="string"),
        *        @OA\Property(property="address", type="string"),
        *        @OA\Property(property="city", type="string"),
        *        @OA\Property(property="postalCode" , type="string"),
        *        @OA\Property(property="themeColor" , type="string"),
        *        @OA\Property(property="pageContent_TnC" , type="string"),
        *        @OA\Property(property="pageContent_PrivacyPolicy" , type="string"),
*                 @OA\Property(property="prefferedCurrencies" , type="json"),
*                 @OA\Property(property="countryId" , type="integer"),
*                 @OA\Property(property="stateId" , type="integer"),
*                 @OA\Property(property="LogoUrl" , type="string"),
*                 @OA\Property(property="userId" , type="integer")

        *),
        *         @OA\MediaType(
        *            mediaType="multipart/form-data",
        *            @OA\Schema(
        *               type="object",
        *                      required={"name","address","city","postalCode","prefferedCurrencies","phone","countryId","stateId","LogoUrl","userId"},
        *        @OA\Property(property="name", type="string"),
        *        @OA\Property(property="address", type="string"),
        *        @OA\Property(property="city", type="string"),
        *        @OA\Property(property="postalCode" , type="string"),
        *        @OA\Property(property="themeColor" , type="string"),
        *        @OA\Property(property="pageContent_TnC" , type="string"),
        *        @OA\Property(property="pageContent_PrivacyPolicy" , type="string"),
*                 @OA\Property(property="prefferedCurrencies" , type="json"),
*                 @OA\Property(property="countryId" , type="integer"),
*                 @OA\Property(property="stateId" , type="integer"),
*                 @OA\Property(property="LogoUrl" , type="string"),
*                 @OA\Property(property="userId" , type="integer")
        *            ),
        *        ),
        *    ),
        *      @OA\Response(
        *          response=201,
        *          description="Organizations created successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=200,
        *          description="Organizations created successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=422,
        *          description="Unprocessable Entity",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        *      security={{"bearer_token":{}}} 
        * )
        */
    public function create(Request $request){
        try{
            $input = $request->all();
        
            $validator = Validator::make($input, [
                "LogoUrl"=> "required|string|max:255",
                "name"=> "required|string|max:255",
                "userId"=>"required|integer|max:255",
                "address"=> "required|string|max:255",
                "city"=> "required|string|max:255",
                "postalCode" => "required|string|max:255",
                "themeColor" => "string|max:255",
                "prefferedCurrencies" => "required|json|max:255",
                "pageContent_TnC" => "string|max:255",
                "pageContent_PrivacyPolicy" => "string|max:255",
                "stateId" => "required|integer",
                "countryId" => "required|integer",
                // "createdBy" => "required|integer|max:255",
                // "updatedBy" => "required|integer|max:255",
                // "firstName" => "required|string|max:255",
                // "lastName" => "required|string|max:255",
                // "phone" => "required|string|max:255",
                // "email" => "required|string|max:255",
                // "password" => "required|string|max:255",
            ]);
            
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" => $validator->errors(),
                ]); 
            // return $this->sendError('Validation Error.', $validator->errors());       
            }
        
            $organization = Organizations::create($input);
            return response()->json([
                "success" => true,
                "message" => "Organizations created successfully.",
                "data" => $organization
            ]);
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }
    }
     /**
    * @OA\Get(
    * path="/api/list_organizations",
    * operationId="Organization List",
    * tags={"Organization"},
    * summary="Organization List",
    * description="Organization list here",
     * @OA\Parameter(
     *          name="page",
     *          description="Page no.",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *  @OA\Parameter(
     *          name="limit",
     *          description="Limit",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *  @OA\Parameter(
     *          name="sortBy",
     *          description="SortBy",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *  @OA\Parameter(
     *          name="orderBy",
     *          description="OrderBy",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *   @OA\Parameter(
     *          name="searchKey",
     *          description="SearchKey",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *   @OA\Parameter(
     *          name="searchVal",
     *          description="SearchVal",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     
    *      @OA\Response(
    *          response=201,
    *          description="Organization List.",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=200,
    *          description="Organization List.",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=422,
    *          description="Unprocessable Entity",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    *     security={{"bearer_token":{}}} 
    * )
    */
      
    public function list(Request $request){
        try{
            $input = $request->all();
        
            $query = Organizations::query();
            $query->select('org.*', 'users.first_name','users.last_name','users.email','users.role','users.status as user_status')->leftJoin('users', 'org.userId', '=', 'users.id')->from('organizations as org');
            if(isset($input['searchKey']) && isset($input['searchVal']) && !empty($input['searchKey']) && !empty($input['searchVal'])){
                $query->where('org.'. $request->input('searchKey'), 'like', '%' . $request->input('searchVal') . '%');
            }
            if (isset($input['sortBy']) && !empty($input['sortBy'])) {
                $query->orderBy('org.'. $input['sortBy'], $input['orderBy']);
            }
            if(isset($input['page']) && !empty($input['page']) && isset($input['limit']) && !empty($input['limit'])){
                $count=$query->count();
                $input['page']= (floor($count/$input['limit']))-1;
                $query->offset($input['page'])->limit($input['limit'])->get();
            }
            $query->where('org.status', '1');
            $organization= $query->get();
            if(!empty($organization)){
                return response()->json([
                    "success" => true,
                    "message" => "Organizations List.",
                    "data" => $organization,
                    "total_count"=>count($organization),
                ]); 
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    } 
    /**
     * @OA\Get(
     *      path="/api/organization/{id}",
     *      operationId="Organization Find",
     *      tags={"Organization"},
     *      summary="Get Organization",
     *      description="Returns Organization data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Organization Id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Organization Data",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Organization Data"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={{"bearer_token":{}}} 
     * )
     */
    public function find($id){
        try{
            $organization = Organizations::find($id);
            $organization = compact('organization');
        
            if(!empty($organization['organization'])){
                $user = User::find($organization['organization']['userId']);
                $user =!empty( $user)? $user:'';
                return response()->json([
                    "success" => true,
                    "message" => "Organization Data",
                    "data"=> array("organization"=>$organization["organization"],"user"=>$user),
                    "total_count"=>count($organization),
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    }
    /**
        * @OA\Post(
        * path="/api/update_organization/{id}",
        * operationId="Update Organization",
        * tags={"Organization"},
        * summary="Update Organization",
        * description="Update Organization here",
        *      @OA\Parameter(
        *          name="id",
        *          description="Organization Id",
        *          required=true,
        *          in="path",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *     @OA\RequestBody(
        *         @OA\JsonContent(
        *           required={"name","address","city","postalCode","prefferedCurrencies","firstName","lastName","phone","email","password","countryId","stateId","LogoUrl","userId"},
        *        @OA\Property(property="name", type="string"),
        *        @OA\Property(property="address", type="string"),
        *        @OA\Property(property="city", type="string"),
        *        @OA\Property(property="postalCode" , type="string"),
        *        @OA\Property(property="themeColor" , type="string"),
        *        @OA\Property(property="pageContent_TnC" , type="string"),
        *        @OA\Property(property="pageContent_PrivacyPolicy" , type="string"),
*                 @OA\Property(property="prefferedCurrencies" , type="json"),
*                
*                 @OA\Property(property="firstName" , type="string"),
*                 @OA\Property(property="lastName" , type="string"),
*                 @OA\Property(property="phone" , type="string"),
*                 @OA\Property(property="email" , type="string"),
*                 @OA\Property(property="password" , type="string"),
*                 @OA\Property(property="countryId" , type="integer"),
*                 @OA\Property(property="stateId" , type="integer"),
*                 @OA\Property(property="LogoUrl" , type="string"),
*                 @OA\Property(property="userId" , type="integer")

        *),
        *         @OA\MediaType(
        *            mediaType="multipart/form-data",
        *            @OA\Schema(
        *               type="object",
        *                      required={"name","address","city","postalCode","prefferedCurrencies","firstName","lastName","phone","email","password","countryId","stateId","LogoUrl","userId"},
        *        @OA\Property(property="name", type="string"),
        *        @OA\Property(property="address", type="string"),
        *        @OA\Property(property="city", type="string"),
        *        @OA\Property(property="postalCode" , type="string"),
        *        @OA\Property(property="themeColor" , type="string"),
        *        @OA\Property(property="pageContent_TnC" , type="string"),
        *        @OA\Property(property="pageContent_PrivacyPolicy" , type="string"),
*                 @OA\Property(property="prefferedCurrencies" , type="json"),
*                 
*                 @OA\Property(property="firstName" , type="string"),
*                 @OA\Property(property="lastName" , type="string"),
*                 @OA\Property(property="phone" , type="string"),
*                 @OA\Property(property="email" , type="string"),
*                 @OA\Property(property="password" , type="string"),
*                 @OA\Property(property="countryId" , type="integer"),
*                 @OA\Property(property="stateId" , type="integer"),
*                 @OA\Property(property="LogoUrl" , type="string"),
*                 @OA\Property(property="userId" , type="integer")
        *            ),
        *        ),
       
        *    ),
        *      @OA\Response(
        *          response=201,
        *          description="Organization data has been updated successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=200,
        *          description="Organization data has been updated successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=422,
        *          description="Unprocessable Entity",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        *      security={{"bearer_token":{}}} 
        * )
        */
    public function update(Request $request, $id)  
    {   try{
            $request = $request->all();
            $validator=Validator::make($request, [
                "LogoUrl"=> "required|string|max:255",
                "name"=> "required|string|max:255",
                "userId"=>"required|integer|max:255",
                "address"=> "required|string|max:255",
                "city"=> "required|string|max:255",
                "postalCode" => "required|string|max:255",
                "themeColor" => "string|max:255",
                "prefferedCurrencies" => "required|json|max:255",
                "pageContent_TnC" => "string|max:255",
                "pageContent_PrivacyPolicy" => "string|max:255",
                "stateId" => "required|integer",
                "countryId" => "required|integer",
            ]);
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" => $validator->errors(),
                ]); 
                //return $this->sendError('Validation Error.', $validator->errors());       
            }
            $organizations=Organizations::find($id);
        
            if(!empty($organizations)){
                $organizations->update($request);
                return response()->json([
                    "success" => true,
                    "message" => "Organization data has been updated successfully.",
                    "data" => $organizations,
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        
            // $organizations = Organizations::find();
            // $organizations->name=  $request->get('name');
            // $organizations->address=  $request->get('address');
            // $organizations->city=  $request->get('city');
            // $organizations->postalCode=  $request->get('postalCode');
            // $organizations->themeColor=  $request->get('themeColor');
            // $organizations->pageContent_TnC=  $request->get('pageContent_TnC');
            // $organizations->pageContent_PrivacyPolicy=  $request->get('');
            // $organizations->prefferedCurrencies=  $request->get('');
            // $organizations->createdBy=  $request->get('createdBy');
            // $organizations->updatedBy=  $request->get('updatedBy');
            // $organizations->firstName=  $request->get('firstName');
            // $organizations->lastName=  $request->get('lastName');
            // $organizations->phone=  $request->get('phone');
            // $organizations->email=  $request->get('email');
            // // $organizations->password=  $request->get('password');
            // $organizations->countryId=  $request->get('countryId');
            // $organizations->stateId=  $request->get('stateId');
            // $organizations->save();  
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    }
     /**
     * @OA\Delete(
     *      path="/api/delete_organization/{id}",
     *      operationId="Delete Organization",
     *      tags={"Organization"},
     *      summary="Delete Organization",
     *      description="Delete Organization",
     *      @OA\Parameter(
     *          name="id",
     *          description="Organization Id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Organization data has been deleted successfully.",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={{"bearer_token":{}}} 
     * )
     */   
    public function delete($id)  
    {    
        try{
            $organizations=Organizations::find($id);
            
            if(!empty($organizations)){
                $organizations->status='0';
                $organizations->save();
                return response()->json([
                    "success" => true,
                    "message" => "Organization data has been deleted successfully.",
                    "data" => $organizations,
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        } 
        
    }

    public function getUserOrganization($userId){
        try{
            $organization=Organizations::where('userId', $userId)->get();
            if(count($organization)>0){
                return response()->json([
                    "success" => true,
                    "message" => "Organization data",
                    "data" => $organization,
                    "total_count"=>count($organization),
                ]); 
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }     
    }

    public function upload_logo(Request $request)
    {   
        try{
            //https://www.itsolutionstuff.com/post/laravel-10-multiple-file-upload-tutorial-exampleexample.html
            $request->validate([
                'files' => 'required',
                'files.*' => 'required|mimes:pdf,xlx,jpg,png,csv|max:2048',
            ]);
            
            $files = [];
        
            if ($request->file('files')){
                if(!is_array($request->file('files'))){
                    return response()->json([
                        "success" => false,
                        "message" => "File upload fail.",
                    ]); 

                }else{
                foreach($request->file('files') as $key => $file)
                    {   
                        $fileName = time().rand(1,99).'.'.$file->extension();  
                        $file->move(public_path('uploads/logo_url'), $fileName);
                        $files[]['name'] = 'logo_url/'. $fileName;
                    }
                }
                
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "File upload fail.",
                ]); 
            }
            return response()->json([
                    "success" => true,
                    "message" => "You have successfully upload file.",
                    "data"=>$files,
                
            ]); 
            // foreach ($files as $key => $file) {
            //     File::create($file);
            // }
        
            // return back()
            //         ->with('success','You have successfully upload file.');
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }     
    }

    //as per priynka discussion 

    public function update_user_org(Request $request, $id)  
    {   
        try{
            $request = $request->all();
            $validator=Validator::make($request, [
                "LogoUrl"=> "required|string|max:255",
                "name"=> "required|string|max:255",
                "userId"=>"required|integer|max:255",
                "address"=> "required|string|max:255",
                "city"=> "required|string|max:255",
                "postalCode" => "required|string|max:255",
                "themeColor" => "string|max:255|nullable",
                "prefferedCurrencies" => "required|json|max:255",
                "pageContent_TnC" => "string|max:255|nullable",
                "pageContent_PrivacyPolicy" => "string|max:255|nullable",
                "stateId" => "required|integer",
                "countryId" => "required|integer",

                //user
                'first_name' => 'required|string|max:255',
                'last_name'=> 'required|string|max:255',
                'phone'=> 'required|string|max:18',
            // 'email' => 'required|string|email|max:255|unique:users',
                //'password' => 'required|string|min:6',
               // 'email_verified_at'=> 'string|max:255|nullable',
                'role'=> 'string|max:255|nullable',
                'status'=> 'integer|nullable',
                'createdBy'=> 'integer|nullable',
                'updatedBy'=> 'integer|nullable',  
            ]);
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" => $validator->errors(),
                ]); 
                //return $this->sendError('Validation Error.', $validator->errors());       
            }
            $organizations=Organizations::find($id);
        
            if(!empty($organizations)){
                DB::beginTransaction(); // Start the transaction
                $organ=$organizations->update([
                    "LogoUrl"=> $request['LogoUrl'],
                    "name"=> $request['name'],
                    "userId"=>$request['userId'],
                    "address"=> $request['address'],
                    "city"=> $request['city'],
                    "postalCode" => $request['postalCode'],
                    "themeColor" => $request['themeColor'],
                    "prefferedCurrencies" => $request['prefferedCurrencies'],
                    "pageContent_TnC" => $request['pageContent_TnC'],
                    "pageContent_PrivacyPolicy" => $request['pageContent_PrivacyPolicy'],
                    "stateId" => $request['stateId'],
                    "countryId" => $request['countryId'],
                ]);
                if($organ){
                $user=user::find($request['userId']);
                }
            }
            
            if(isset($user) && !empty($user)){
                $user = $user->update([
                    'first_name'=>$request['first_name'],
                    'last_name'=> $request['last_name'],
                    'phone'=> $request['phone'],
                    //'email'=> $request['email'],
                    //'password'=> Hash::make($request['password),
                   // 'email_verified_at'=> $request['email_verified_at'],
                    'role'=> (isset($request['role']) && !empty($request['role']))?$request['role'] : 2,
                    'status'=> (isset($request['status']) && !empty($request['status']))?$request['status'] : 0,
                    'createdBy'=> (isset($request['createdBy']) && !empty($request['createdBy'])) ?$request['createdBy'] :'1',
                    'updatedBy'=> (isset($request['updatedBy']) && !empty($request['updatedBy'])) ?$request['updatedBy'] :'1'
                ]);
                if($user){
                    DB::commit();
                }else{
                    DB::rollBack();
                        return response()->json([
                        "success" => false,
                        "message" => "Data Not Found.",
                    ]);
                }
                return response()->json([
                    "success" => true,
                    "message" => "Organization data has been updated successfully.",
                    "data" => $organizations,
                ]);
            }else{
                DB::rollBack();
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
    
        }
    }
}   
