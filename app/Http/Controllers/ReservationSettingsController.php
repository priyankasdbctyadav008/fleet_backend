<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ReservationSettings;
use Validator;
use Illuminate\Support\Facades\Auth;

class ReservationSettingsController extends Controller
{
       public function __construct()
    {     
        //$this->middleware('auth:api');
        if(!auth()->check()){
                 $data = response()->json([
                    "success" => false,
                    "message" => "Authentication Fail.",
                ])->getContent();; 
                header("Content-type:application/json");
                echo $data;
                die();
        }else{
            $user_role = Auth::user()->role;
            if($user_role !='1' && $user_role !='2'){
                $data = response()->json([
                    "success" => false,
                    "message" => "UNAUTHORIZED.",
                ]); 
                header("Content-type:application/json");
                echo $data;
                return $data;
                die();
            }
        }
    }
    public function create(Request $request){
        try {
            $input = $request->all();
           
            //$validated = $request->validated();
            $validator = Validator::make($input,[ 
                'informAboutRentalBeforeDays'=> "required|integer|max:255",
                'rentalReserveBeforeMonths'=> "required|integer|max:255",
                'minRentalPeriodInDays'=> "required|integer|max:255",
                'allowOverBooking'=> "required|integer|max:255",
                'authorizeOutsideReservationArea'=> "required|integer|max:255",
                'allowPastDateReservation'=> "required|integer|max:255",
                'isAutoGenerateBookingRef'=> "required|integer|max:255",
                'userId'=>"required|integer|max:255",
            ]);
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" => $validator->errors(),
                ]); 
            }
            $is_exist_reservationSetting=ReservationSettings::where('userId', $input['userId'])->get();
            if(count($is_exist_reservationSetting)>0){
               return response()->json([
                "success" => false,
                "message" => 'Setting already exist for this user',
               ]);
            }
       
            $reservationSettings = ReservationSettings::create($input);
            return response()->json([
                "success" => true,
                "message" => "Reservation-Setting created successfully.",
                "data" => $reservationSettings
            ]);
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
    
        }
    }


    public function list(Request $request){
        try{
            $input = $request->all();
        
            $query = ReservationSettings::query();
            $reservationSettings= $query->get();
            if(!$reservationSettings->isEmpty()){
                return response()->json([
                    "success" => true,
                    "message" => "Reservation-Setting List.",
                    "data" => $reservationSettings,
                    "total_count"=>count($reservationSettings),
                ]); 
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        } 
    } 

    public function find($id){
        try{
            $reservationSettings = ReservationSettings::find($id);
            $reservationSettings = compact('reservationSettings');
            if(!empty($reservationSettings['reservationSettings'])){
                return response()->json([
                    "success" => true,
                    "message" => "Reservation-Setting Data",
                    "data" => $reservationSettings,
                    "total_count"=>count($reservationSettings),
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }     
    }

    public function update(Request $request, $id)  
    {   try{
            $request = $request->all();
            $validator=Validator::make($request, [
                'informAboutRentalBeforeDays'=> "required|integer|max:255",
                    'rentalReserveBeforeMonths'=> "required|integer|max:255",
                    'minRentalPeriodInDays'=> "required|integer|max:255",
                    'allowOverBooking'=> "required|integer|max:255",
                    'authorizeOutsideReservationArea'=> "required|integer|max:255",
                    'allowPastDateReservation'=> "required|integer|max:255",
                    'isAutoGenerateBookingRef'=> "required|integer|max:255",
                    'userId'=>"required|integer|max:255",
            ]);
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" => $validator->errors(),
                ]); 
            }
            $reservationSettings=ReservationSettings::find($id);
        
            if(!empty($reservationSettings)){
                $reservationSettings->update($request);
                return response()->json([
                    "success" => true,
                    "message" => "Reservation-Setting data has been updated successfully.",
                    "data" => $reservationSettings,
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }     
    }
    
    
    public function delete($id)  
    {   
        try{
            $reservationSettings=ReservationSettings::find($id);
        
            if(!empty($reservationSettings)){
                $reservationSettings->status = '0';
                $reservationSettings->save();
                return response()->json([
                    "success" => true,
                    "message" => "Reservation-Setting data has been deleted successfully.",
                    "data" => $reservationSettings,
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }     
    }

    public function find_user_reservation_settings($userId){
        try{
        $reservationSetting=ReservationSettings::where('userId', $userId)->get();
            if(count($reservationSetting)>0){
                return response()->json([
                    "success" => true,
                    "message" => "User reservationSetting data",
                    "data" => $reservationSetting,
                    "total_count"=>count($reservationSetting),
                ]); 
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }     
    }

}
