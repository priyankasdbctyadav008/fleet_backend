<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SubscriptionPlan;
use Validator;
use Illuminate\Support\Facades\Auth;

class SubscriptionPlanController extends Controller
{
     public function __construct()
    {
        //$this->middleware('auth:api');
        if(!auth()->check()){
            $data = response()->json([
                "success" => false,
                "message" => "Authentication Fail.",
            ])->getContent();; 
            header("Content-type:application/json");
            echo $data;
            die();
        }else{
            $user_role = Auth::user()->role;
            if($user_role !='1' && $user_role !='2'){
                $data = response()->json([
                    "success" => false,
                    "message" => "UNAUTHORIZED.",
                ]); 
                header("Content-type:application/json");
                echo $data;
                return $data;
                die();
            }
        }
    }
      /**
        * @OA\Post(
        * path="/api/create_subscriptionPlan",
        * operationId="Create Subscription Plan",
        * tags={"Subscription-Plan"},
        * summary="Create Subscription Plan",
        * description="Create subscription plan here",
        *     @OA\RequestBody(
        *         @OA\JsonContent(
        *               required={"name","amount","stripePlanId","planType","createdBy","updatedBy","status"},
                        *@OA\Property(property="name", type="string"),
                        *@OA\Property(property="amount", type="string"),
                        *@OA\Property(property="stripePlanId", type="string"),
                        *@OA\Property(property="planType", type="integer"),
                        *@OA\Property(property="createdBy", type="integer"),
                        *@OA\Property(property="updatedBy", type="integer"),
                        *@OA\Property(property="status", type="integer"),
        *),
        *         @OA\MediaType(
        *            mediaType="multipart/form-data",
        *            @OA\Schema(
        *               type="object",
        *               required={"name","amount","stripePlanId","planType","createdBy","updatedBy","status"},
                        *@OA\Property(property="name", type="string"),
                        *@OA\Property(property="amount", type="string"),
                        *@OA\Property(property="stripePlanId", type="string"),
                        *@OA\Property(property="planType", type="integer"),
                        *@OA\Property(property="createdBy", type="integer"),
                        *@OA\Property(property="updatedBy", type="integer"),
                        *@OA\Property(property="status", type="integer"),
        *            ),
        *        ),
        *    ),
        *      @OA\Response(
        *          response=201,
        *          description="Subscription plan created successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=200,
        *          description="Subscription plan created successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=422,
        *          description="Unprocessable Entity",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        *      security={{"bearer_token":{}}} 
        * )
        */
    public function create(Request $request){
        try {
            
            $input = $request->all();
            
            $validator = Validator::make($input, [
                "name"=> "required|string|max:255",
                "amount"=> "required|string|max:255",
                "stripePlanId"=> "required|string|max:255",
                "planType"=> "required|string|max:255",
                "status"=> "required|integer|max:255",
                "createdBy"=> "required|integer|max:255",
                "updatedBy"=> "required|integer|max:255",
            ]);
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" => $validator->errors(),
                ]); 
            }
            $subscriptionPlan = SubscriptionPlan::create($input);
            return response()->json([
                "success" => true,
                "message" => "Subscription plan created successfully.",
                "data" => $subscriptionPlan
            ]);
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
    
        }
    }
      /**
    * @OA\Get(
    * path="/api/list_subscriptionPlan",
    * operationId="Subscription Plan List",
    * tags={"Subscription-Plan"},
    * summary="Subscription Plan List",
    * description="Subscription plan list here",
    *      @OA\Response(
    *          response=201,
    *          description="Subscription Plan List.",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=200,
    *          description="Subscription Plan List.",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=422,
    *          description="Unprocessable Entity",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    *     security={{"bearer_token":{}}} 
    * )
    */
    public function list(Request $request){
        try{
            $input = $request->all();
        
            $query = SubscriptionPlan::query();
            // if(isset($input['searchKey']) && isset($input['searchVal']) && !empty($input['searchKey']) && !empty($input['searchVal'])){
            //     $query->where($request->input('searchKey'), 'like', '%' . $request->input('searchVal') . '%');
            // }
        
            // if (isset($input['sortBy']) && !empty($input['sortBy'])) {
            //     $query->orderBy($input['sortBy'], $input['orderBy']);
            // }
            // if(isset($input['page']) && !empty($input['page']) && isset($input['limit']) && !empty($input['limit'])){
            //     $count=$query->count();
            //     $input['page']= (floor($count/$input['limit']))-1;
            //     $query->offset($input['page'])->limit($input['limit'])->get();
            // }
            $subscriptionPlan= $query->get();
        
            if(!$subscriptionPlan->isEmpty()){
                return response()->json([
                    "success" => true,
                    "message" => "Subscription Plan List.",
                    "data" => $subscriptionPlan,
                    "total_count"=>count($subscriptionPlan),
                ]); 
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }     
    } 

     /**
     * @OA\Get(
     *      path="/api/subscriptionPlan/{id}",
     *      operationId="Subscription Plan Find",
     *      tags={"Subscription-Plan"},
     *      summary="Get Subscription Plan",
     *      description="Returns subscription plan data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Subscription Plan Id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Subcription Plan Data",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Subcription Plan Data"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={{"bearer_token":{}}} 
     * )
     */
    public function find($id){
        try{
            $subscriptionPlan = SubscriptionPlan::find($id);
            $subscriptionPlan = compact('subscriptionPlan');
            if(!empty($subscriptionPlan['subscriptionPlan'])){
                return response()->json([
                    "success" => true,
                    "message" => "Subcription Plan Data",
                    "data" => $subscriptionPlan,
                    "total_count"=>count($subscriptionPlan),
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }     
    }
    /**
        * @OA\Post(
        * path="/api/update_subscriptionPlan/{id}",
        * operationId="Update Subscription Plan",
        * tags={"Subscription-Plan"},
        * summary="Update Subscription Plan",
        * description="Update subscription plan here",
        *      @OA\Parameter(
        *          name="id",
        *          description="Subscription Plan Id",
        *          required=true,
        *          in="path",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *     @OA\RequestBody(
        *         @OA\JsonContent(
        *                required={"name","amount","stripePlanId","planType","createdBy","updatedBy","status"},
                        *@OA\Property(property="name", type="string"),
                        *@OA\Property(property="amount", type="string"),
                        *@OA\Property(property="stripePlanId", type="string"),
                        *@OA\Property(property="planType", type="integer"),
                        *@OA\Property(property="createdBy", type="integer"),
                        *@OA\Property(property="updatedBy", type="integer"),
                        *@OA\Property(property="status", type="integer"),
        *),
        *         @OA\MediaType(
        *            mediaType="multipart/form-data",
        *            @OA\Schema(
        *               type="object",
        *               required={"name","amount","stripePlanId","planType","createdBy","updatedBy","status"},
                        *@OA\Property(property="name", type="string"),
                        *@OA\Property(property="amount", type="string"),
                        *@OA\Property(property="stripePlanId", type="string"),
                        *@OA\Property(property="planType", type="integer"),
                        *@OA\Property(property="createdBy", type="integer"),
                        *@OA\Property(property="updatedBy", type="integer"),
                        *@OA\Property(property="status", type="integer"),
                        
        *            ),
        *        ),
        *    ),
        *      @OA\Response(
        *          response=201,
        *          description="Subscription Plan Update successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=200,
        *          description="Subscription Plan Update successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=422,
        *          description="Unprocessable Entity",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        *      security={{"bearer_token":{}}} 
        * )
        */
    public function update(Request $request, $id)  
    {   try{
           
            $request = $request->all();
            $validator=Validator::make($request, [
                "name"=> "required|string|max:255",
                "amount"=> "required|string|max:255",
                "stripePlanId"=> "required|string|max:255",
                "planType"=> "required|string|max:255",
                "status"=> "required|integer|max:255",
                "createdBy"=> "required|integer|max:255",
                "updatedBy"=> "required|integer|max:255",
            ]);
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" => $validator->errors(),
                ]); 
            }
            $subscriptionPlan= SubscriptionPlan::find($id);
        
            if(!empty($subscriptionPlan)){
                $subscriptionPlan->update($request);
                return response()->json([
                    "success" => true,
                    "message" => "Subscription plan data has been updated successfully.",
                    "data" => $subscriptionPlan,
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
    
        }
    }  
    /**
     * @OA\Delete(
     *      path="/api/delete_subscriptionPlan/{id}",
     *      operationId="Delete Subscription Plan",
     *      tags={"Subscription-Plan"},
     *      summary="Delete Subscription Plan",
     *      description="Delete Subscription Plan",
     *      @OA\Parameter(
     *          name="id",
     *          description="Subscription Plan Id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Subscription plan data has been deleted successfully.",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={{"bearer_token":{}}} 
     * )
     */ 
    public function delete($id)  
    {  
        try{
            
            $subscriptionPlan=SubscriptionPlan::find($id);
            if(!empty($subscriptionPlan)){
                $subscriptionPlan->status='0';
                $subscriptionPlan->save();
                return response()->json([
                    "success" => true,
                    "message" => "Subscription plan data has been deleted successfully.",
                    "data" => $subscriptionPlan,
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
    
        }
    }

    /**
     * @OA\Get(
     *      path="/api/dropDown_vehiclesType",
     *      operationId="Vehicles Type DropDown",
     *      tags={"Vehicles-Type"},
     *      summary="Get Vehicles Type DropDown",
     *      description="Returns vehicles Type dropDown data",
    
     *      @OA\Response(
     *          response=200,
     *          description="Vehicles Type Data",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Vehicles Type Data Data"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={{"bearer_token":{}}} 
     * )
     */
    public function list_dropDown(Request $request){
        try{
            $input = $request->all();
        
            $query = SubscriptionPlan::query();
            $query->where('status', '1');
            $subscriptionPlan= $query->get();
            
            if(!$subscriptionPlan->isEmpty()){
                return response()->json([
                    "success" => true,
                    "message" => "Subscription plan List.",
                    "data" => $subscriptionPlan,
                    "total_count"=>count($subscriptionPlan),
                ]); 
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }     
    } 
}
