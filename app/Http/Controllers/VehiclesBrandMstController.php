<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\VehiclesBrandMst;
use Validator;
use Illuminate\Support\Facades\Auth;

class VehiclesBrandMstController extends Controller
{
     public function __construct()
    {
        //$this->middleware('auth:api');
        if(!auth()->check()){
            $data = response()->json([
                "success" => false,
                "message" => "Authentication Fail.",
            ])->getContent();; 
            header("Content-type:application/json");
            echo $data;
            die();
        }else{
            $user_role = Auth::user()->role;
            if($user_role !='1' && $user_role !='2'){
                $data = response()->json([
                    "success" => false,
                    "message" => "UNAUTHORIZED.",
                ]); 
                header("Content-type:application/json");
                echo $data;
                return $data;
                die();
            }
        }
    }
     /**
        * @OA\Post(
        * path="/api/create_vehiclesBrand",
        * operationId="Create Vehicles Brand",
        * tags={"Vehicles-Brand"},
        * summary="Create Vehicles Brand",
        * description="Create vehicles brand here",
        *     @OA\RequestBody(
        *         @OA\JsonContent(
        *               required={"name","vehiclesTypeId","status"},
                        *@OA\Property(property="name", type="string"),
                        *@OA\Property(property="vehiclesTypeId", type="string"),
                        *@OA\Property(property="status", type="integer"),
        *),
        *         @OA\MediaType(
        *            mediaType="multipart/form-data",
        *            @OA\Schema(
        *               type="object",
        *               required={"name","vehiclesTypeId","status"},
                        *@OA\Property(property="name", type="string"),
                        *@OA\Property(property="description", type="string"),
                        *@OA\Property(property="vehiclesTypeId", type="string"),
                        *@OA\Property(property="status", type="integer"),
                        
        *            ),
        *        ),
        *    ),
        *      @OA\Response(
        *          response=201,
        *          description="Vehicle brand created successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=200,
        *          description="Vehicle brand created successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=422,
        *          description="Unprocessable Entity",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        *      security={{"bearer_token":{}}} 
        * )
        */
    public function create(Request $request){
        try {
            $input = $request->all();
            $validator = Validator::make($input, [
                "name"=> "required|string|max:255",
                "description"=> "string|max:255",
                "vehiclesTypeId"=> "required|integer|max:255",
                "status"=> "required|integer|max:255",
            ]);
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" => $validator->errors(),
                ]); 
            }
            $vehiclesBrandMst = VehiclesBrandMst::create($input);
            return response()->json([
                "success" => true,
                "message" => "Vehicle brand created successfully.",
                "data" => $vehiclesBrandMst
            ]);
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
    
        }
    }
      /**
    * @OA\Get(
    * path="/api/list_vehiclesBrand",
    * operationId="Vehicles Brand List",
    * tags={"Vehicles-Brand"},
    * summary="Vehicles Brand List",
    * description="Vehicles brand list here",
     * @OA\Parameter(
     *          name="page",
     *          description="Page no.",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *  @OA\Parameter(
     *          name="limit",
     *          description="Limit",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *  @OA\Parameter(
     *          name="sortBy",
     *          description="SortBy",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *  @OA\Parameter(
     *          name="orderBy",
     *          description="OrderBy",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *   @OA\Parameter(
     *          name="searchKey",
     *          description="SearchKey",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *   @OA\Parameter(
     *          name="searchVal",
     *          description="SearchVal",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     
    *      @OA\Response(
    *          response=201,
    *          description="Vehicle Brand List.",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=200,
    *          description="Vehicle Brand List.",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=422,
    *          description="Unprocessable Entity",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    *     security={{"bearer_token":{}}} 
    * )
    */
    public function list(Request $request){
        try{
            $input = $request->all();
            $query = VehiclesBrandMst::query();
            if(isset($input['searchKey']) && isset($input['searchVal']) && !empty($input['searchKey']) && !empty($input['searchVal'])){
                $query->where($request->input('searchKey'), 'like', '%' . $request->input('searchVal') . '%');
            }
        
            if (isset($input['sortBy']) && !empty($input['sortBy'])) {
                $query->orderBy($input['sortBy'], $input['orderBy']);
            }
            if(isset($input['page']) && !empty($input['page']) && isset($input['limit']) && !empty($input['limit'])){
                $count=$query->count();
                $input['page']= (floor($count/$input['limit']))-1;
                $query->offset($input['page'])->limit($input['limit'])->get();
            }
            $vehiclesBrandMst= $query->get();

            //print_r($vehiclesCategoryMst); die();
            if(!$vehiclesBrandMst->isEmpty()){
                return response()->json([
                    "success" => true,
                    "message" => "Vehicle brand List.",
                    "data" => $vehiclesBrandMst,
                    "total_count"=>count($vehiclesBrandMst),
                ]); 
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }     
    } 

      /**
     * @OA\Get(
     *      path="/api/vehiclesBrand/{id}",
     *      operationId="vehicles Brand Find",
     *      tags={"Vehicles-Brand"},
     *      summary="Get Vehicles Brand",
     *      description="Returns vehicles Brand data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Vehicles Brand Id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Vehicle Brand Data",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Vehicle Brand Data"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={{"bearer_token":{}}} 
     * )
     */
    public function find($id){
        try{
            $vehiclesBrandMst = VehiclesBrandMst::find($id);
            $vehiclesBrandMst = compact('vehiclesBrandMst');
            if(!empty($vehiclesBrandMst['vehiclesBrandMst'])){
                return response()->json([
                    "success" => true,
                    "message" => "Vehicle brand Data",
                    "data" => $vehiclesBrandMst,
                    "total_count"=>count($vehiclesBrandMst),
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }     
    }
     /**
        * @OA\Post(
        * path="/api/update_vehiclesBrand/{id}",
        * operationId="Update Vehicles Brand",
        * tags={"Vehicles-Brand"},
        * summary="Update Vehicles Brand",
        * description="Update vehicles brand here",
        *      @OA\Parameter(
        *          name="id",
        *          description="Vehicle Brand Id",
        *          required=true,
        *          in="path",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *     @OA\RequestBody(
        *         @OA\JsonContent(
        *               required={"name","vehiclesTypeId","status"},
                        *@OA\Property(property="name", type="string"),
                        *@OA\Property(property="description", type="string"),
                        *@OA\Property(property="vehiclesTypeId", type="integer"),
                        *@OA\Property(property="status", type="integer"),
        *),
        *         @OA\MediaType(
        *            mediaType="multipart/form-data",
        *            @OA\Schema(
        *               type="object",
        *               required={"name","vehiclesTypeId","status"},
                        *@OA\Property(property="name", type="string"),
                        *@OA\Property(property="description", type="string"),
                        *@OA\Property(property="vehiclesTypeId", type="integer"),
                        *@OA\Property(property="status", type="integer"),
                        
        *            ),
        *        ),
        *    ),
        *      @OA\Response(
        *          response=201,
        *          description="Vehicle Brand Update successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=200,
        *          description="Vehicle Brand Update successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=422,
        *          description="Unprocessable Entity",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        *      security={{"bearer_token":{}}} 
        * )
        */
    public function update(Request $request, $id)  
    {   try{
            $request = $request->all();
            $validator=Validator::make($request, [
                "name"=> "required|string|max:255",
                "description"=> "string|max:255",
                "vehiclesTypeId"=> "required|integer|max:255",
                "status"=> "required|integer|max:255",
            ]);
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" => $validator->errors(),
                ]); 
            }
            $vehiclesBrandMst= VehiclesBrandMst::find($id);
        
            if(!empty($vehiclesBrandMst)){
                $vehiclesBrandMst->update($request);
                return response()->json([
                    "success" => true,
                    "message" => "Vehicle brand data has been updated successfully.",
                    "data" => $vehiclesBrandMst,
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
    
        }
    }  

     /**
     * @OA\Delete(
     *      path="/api/delete_vehiclesBrand/{id}",
     *      operationId="Delete VehiclesBrand",
     *      tags={"Vehicles-Type"},
     *      summary="Delete vehicles Brand",
     *      description="Delete vehicles Brand",
     *      @OA\Parameter(
     *          name="id",
     *          description="VehiclesBrand Id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Vehicles brand data has been deleted successfully.",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={{"bearer_token":{}}} 
     * )
     */ 
    public function delete($id)  
    {  try{
            $vehiclesBrandMst=VehiclesBrandMst::find($id);
            if(!empty($vehiclesBrandMst)){
            //  $vehiclesBrandMst->status='0';
                $vehiclesBrandMst->delete();
                return response()->json([
                    "success" => true,
                    "message" => "Vehicle brand data has been deleted successfully.",
                    "data" => $vehiclesBrandMst,
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
    
        }
    }
     /**
     * @OA\Get(
     *      path="/api/dropDown_vehiclesBrand",
     *      operationId="Vehicles Brand DropDown",
     *      tags={"Vehicles-Brand"},
     *      summary="Get Vehicles Brand DropDown",
     *      description="Returns vehicles Brand dropDown data",
    
     *      @OA\Response(
     *          response=200,
     *          description="Vehicles Brand Data",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Vehicles Brand Data Data"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={{"bearer_token":{}}} 
     * )
     */
    public function list_dropDown(Request $request){
        try{
            $input = $request->all();
        
            $query = VehiclesBrandMst::query();
            $query->where('status', '1');
            $vehiclesBrandMst= $query->get();
            
            if(!$vehiclesBrandMst->isEmpty()){
                return response()->json([
                    "success" => true,
                    "message" => "Vehicle brand List.",
                    "data" => $vehiclesBrandMst,
                    "total_count"=>count($vehiclesBrandMst),
                ]); 
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }     
    } 
}
