<?php
namespace App\Http\Controllers;


use App\Models\ModuleMst;
use Validator;
use Illuminate\Support\Facades\Auth;


use Illuminate\Http\Request;

class ModulesMstController extends Controller
{
     public function __construct()
    {
        //$this->middleware('auth:api');
        if(!auth()->check()){
            $data = response()->json([
                "success" => false,
                "message" => "Authentication Fail.",
            ])->getContent();; 
            header("Content-type:application/json");
            echo $data;
            die();
        }else{
            $user_role = Auth::user()->role;
            if($user_role !='1' && $user_role !='2'){
                $data = response()->json([
                    "success" => false,
                    "message" => "UNAUTHORIZED.",
                ]); 
                header("Content-type:application/json");
                echo $data;
                return $data;
                die();
            }
        }
    }
     /**
        * @OA\Post(
        * path="/api/create_moduleMst",
        * operationId="Create Module Mst",
        * tags={"Module-Mst"},
        * summary="Create Module Mst",
        * description="Create module mst here",
        *     @OA\RequestBody(
        *         @OA\JsonContent(
        *               required={"name"},
                        *@OA\Property(property="name", type="string"),
                        
        *),
        *         @OA\MediaType(
        *            mediaType="multipart/form-data",
        *            @OA\Schema(
        *               type="object",
        *               required={"name"},
                        *@OA\Property(property="name", type="string"),
        *            ),
        *        ),
        *    ),
        *      @OA\Response(
        *          response=201,
        *          description="Module-Mst created successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=200,
        *          description="Module-Mst created successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=422,
        *          description="Unprocessable Entity",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        *      security={{"bearer_token":{}}} 
        * )
        */
    public function create(Request $request){
        try {
            $input = $request->all();
            
            $validator = Validator::make($input, [
                "name"=> "required|string|max:255",
            ]);
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" => $validator->errors(),
                ]); 
            }
            $moduleMst = ModuleMst::create($input);
            return response()->json([
                "success" => true,
                "message" => "Module-Mst created successfully.",
                "data" => $moduleMst
            ]);
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
    
        }
    }
      /**
     * @OA\Get(
     * path="/api/list_moduleMst",
     * operationId="Module Mst List",
     * tags={"Module-Mst"},
     * summary="Module Mst List",
     * description="Module-Mst list here",
     *      @OA\Response(
     *          response=201,
     *          description="Module-Mst List.",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=200,
     *          description="Module-Mst List.",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *     security={{"bearer_token":{}}} 
     * )
     */
    public function list(Request $request){
        try{
            $query = ModuleMst::query();
            $moduleMst= $query->get();
        
            if(!$moduleMst->isEmpty()){
                return response()->json([
                    "success" => true,
                    "message" => "Module-Mst List.",
                    "data" => $moduleMst,
                    "total_count"=>count($moduleMst),
                ]); 
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    } 
     /**
     * @OA\Get(
     *      path="/api/moduleMst/{id}",
     *      operationId="Module Mst Find",
     *      tags={"Module-Mst"},
     *      summary="Module-Mst Type",
     *      description="Returns mdule-mst data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Module-Mst Id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Module-Mst Data",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Module-Mst Data"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={{"bearer_token":{}}} 
     * )
     */
    public function find($id){
        try{
            $moduleMst = ModuleMst::find($id);
            $moduleMst = compact('moduleMst');
            if(!empty($moduleMst['moduleMst'])){
                return response()->json([
                    "success" => true,
                    "message" => "Module-Mst Data",
                    "data" => $moduleMst,
                    "total_count"=>count($moduleMst),
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    }
      /**
        * @OA\Post(
        * path="/api/update_moduleMst/{id}",
        * operationId="Update Module-Mst",
        * tags={"Module-Mst"},
        * summary="Update Module-Mst",
        * description="Update Module-Mst here",
        *      @OA\Parameter(
        *          name="id",
        *          description="Module-Mst Type Id",
        *          required=true,
        *          in="path",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *     @OA\RequestBody(
        *         @OA\JsonContent(
        *               required={"name"},
                        *@OA\Property(property="name", type="string"),
                        
        *),
        *         @OA\MediaType(
        *            mediaType="multipart/form-data",
        *            @OA\Schema(
        *               type="object",
        *               required={"name"},
                        *@OA\Property(property="name", type="string"),
        *            ),
        *        ),
        *    ),
        *      @OA\Response(
        *          response=201,
        *          description="Module-Mst Update successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=200,
        *          description="Module-Mst Update successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=422,
        *          description="Unprocessable Entity",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        *      security={{"bearer_token":{}}} 
        * )
        */
    public function update(Request $request, $id)  
    {   try{
            $request = $request->all();
            $validator=Validator::make($request, [
                "name"=> "required|string|max:255",
            ]);
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" => $validator->errors(),
                ]); 
            }
            $moduleMst= ModuleMst::find($id);
        
            if(!empty($moduleMst)){
                $moduleMst->update($request);
                return response()->json([
                    "success" => true,
                    "message" => "Module-Mst data has been updated successfully.",
                    "data" => $moduleMst,
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    }  

    /**
     * @OA\Delete(
     *      path="/api/delete_moduleMst/{id}",
     *      operationId="Delete Module-Mst",
     *      tags={"Module-Mst"},
     *      summary="Delete Module-Mst",
     *      description="Delete Module-Mst",
     *      @OA\Parameter(
     *          name="id",
     *          description="ModuleMst Id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Module-Mst data has been deleted successfully.",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={{"bearer_token":{}}} 
     * )
     */ 
    public function delete($id)  
    {   try{
            $moduleMst=ModuleMst::find($id);
            if(!empty($moduleMst)){
            // $vehiclesTypeMst->status='0';
                $moduleMst->delete();
                return response()->json([
                    "success" => true,
                    "message" => "Module-Mst data has been deleted successfully.",
                    "data" => $moduleMst,
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    }
    
   
}
