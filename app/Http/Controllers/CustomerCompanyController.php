<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\CustomerCompany;
use Validator;
use Illuminate\Support\Facades\Auth;

class CustomerCompanyController extends Controller
{
   
       public function __construct()
    {     
        //$this->middleware('auth:api');
        if(!auth()->check()){
                 $data = response()->json([
                    "success" => false,
                    "message" => "Authentication Fail.",
                ])->getContent();; 
                header("Content-type:application/json");
                echo $data;
                die();
        }else{
            $user_role = Auth::user()->role;
            if($user_role !='1' && $user_role !='2'){
                $data = response()->json([
                    "success" => false,
                    "message" => "UNAUTHORIZED.",
                ]); 
                header("Content-type:application/json");
                echo $data;
                return $data;
                die();
            }
        }
    }
    public function create(Request $request){
        try {
            $input = $request->all();
           
            //$validated = $request->validated();
            $validator = Validator::make($input,[ 
                'name'=> "required|string|max:255",
                'company_email'=> "required|string|max:255",
                'company_phone'=> "required|string|max:18",
                'sirenno'=> "integer|max:255",
                'isProfessional'=>"required|string|max:255",
                'createdBy'=> "integer|nullable",
                'updatedBy'=> "integer|nullable",
                'company_address1'=> "required|string|max:255",
                //'address2'=> "required|string|max:255",
                'company_city'=> "required|string|max:255",
                'company_stateId'=> "required|integer",
                'company_countryId'=> "required|integer",
                'company_postalCode'=>"required|string|max:6",
            ]);
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" => $validator->errors(),
                ]); 
            }
            $customer = CustomerCompany::create($input);
            return response()->json([
                "success" => true,
                "message" => "Customer Company created successfully.",
                "data" => $customer
            ]);
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
    
        }
    }


    public function list(Request $request){
        try{
            $input = $request->all();
        
            $query = CustomerCompany::query()->where('status','1');
            if(isset($input['name'])  && !empty($input['name'])){
                $query->where('name', 'like', '%' . $input['name'] . '%');
            }
            if (isset($input['sortBy']) && !empty($input['sortBy'])) {
                $query->orderBy($input['sortBy'], $input['orderBy']);
            }
            if(isset($input['page']) && !empty($input['page']) && isset($input['limit']) && !empty($input['limit'])){
                $count=$query->count();
                $input['page']= (floor($count/$input['limit']))-1;
                $query->offset($input['page'])->limit($input['limit'])->get();
            }
            $customers= $query->get();
            if(!$customers->isEmpty()){
                return response()->json([
                    "success" => true,
                    "message" => "Customer Company List.",
                    "data" => $customers,
                    "total_count"=>count($customers),
                ]); 
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    } 

    public function find($id){
        try{
            $customer = CustomerCompany::find($id);
            $customer = compact('customer');
            if(!empty($customer['customer'])){
                return response()->json([
                    "success" => true,
                    "message" => "Company Data",
                    "data" => $customer,
                    "total_count"=>count($customer),
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    }

     public function update(Request $request, $id)  
    {   try{
            $request = $request->all();
            $validator=Validator::make($request, [
                'name'=> "required|string|max:255",
                'company_email'=> "required|string|max:255",
                'company_phone'=> "required|string|max:18",
                'sirenno'=> "integer|max:255",
                'isProfessional'=>"required|string|max:255",
                'createdBy'=> "integer|nullable",
                'updatedBy'=> "integer|nullable",
                'company_address1'=> "required|string|max:255",
                'company_city'=> "required|string|max:255",
                'company_stateId'=> "required|integer",
                'company_countryId'=> "required|integer",
                'company_postalCode'=>"required|string|max:6",
            ]);
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" => $validator->errors(),
                ]); 
            }
            $customerCompany=CustomerCompany::find($id);
        
            if(!empty($customer)){
                $customerCompany->update($request);
                return response()->json([
                    "success" => true,
                    "message" => "Customer Company data has been updated successfully.",
                    "data" => $customerCompany,
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    }
    
     public function delete($id)  
    {   
        try{
            $company=CustomerCompany::find($id);
        
            if(!empty($company)){
            
                $company->status = '0';
                $company->save();
                return response()->json([
                    "success" => true,
                    "message" => "Customer Company data has been deleted successfully.",
                    "data" => $company,
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    }
}
