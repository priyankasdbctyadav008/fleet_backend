<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CustomerBookingsController extends Controller
{
    public function __construct()
    {   
        try{
            //$this->middleware('auth:api');
            if(!auth()->check()){
                    $data = response()->json([
                        "success" => false,
                        "message" => "Authentication Fail.",
                    ])->getContent();; 
                    header("Content-type:application/json");
                    echo $data;
                    die();
            }else{
                $user_role = Auth::user()->role;
                if($user_role !='3'){
                    $data = response()->json([
                        "success" => false,
                        "message" => "UNAUTHORIZED.",
                    ]); 
                    header("Content-type:application/json");
                    echo $data;
                    return $data;
                    die();
                }
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    }
    /**
        * @OA\Post(
        * path="/api/create_customerBooking",
        * operationId="Create Customer Booking",
        * tags={"Customer-Booking"},
        * summary="Create Customer Booking",
        * description="Create Customer Booking here",
        *     @OA\RequestBody(
        *         @OA\JsonContent(
        *            required={"userId","vehicleCategoryId","vehicleTypeId","agencyVehicleId","agencyId","organizationId","agencyTariffPackageId","bookedOn","bookingstartDate","bookingendDate","status","paymentGateWayId","source","departureAgencyId","returnAgencyId","customerQuotationId","stateId","city","postalCode","productName","amount","quantity","paymentTypt","cardType","costReason","stripeTransactionDetails","type"},
                        *@OA\Property(property="userId", type= "integer"),
                        *@OA\Property(property="vehicleCategoryId", type= "integer"),
                        *@OA\Property(property="vehicleTypeId", type= "integer"),
                        *@OA\Property(property="agencyVehicleId", type= "integer"),
                        *@OA\Property(property="agencyId", type= "integer"),
                        *@OA\Property(property="organizationId", type= "integer"),
                        *@OA\Property(property="agencyTariffPackageId", type= "integer"),
                        *@OA\Property(property="bookedOn" , type= "string"),
                        *@OA\Property(property="bookingstartDate" , type="string"),
                        *@OA\Property(property="bookingendDate", type= "string"),
                        *@OA\Property(property="status", type="string"),
                        *@OA\Property(property="paymentGateWayId", type="integer"),
                        *@OA\Property(property="source", type="string"),
                        *@OA\Property(property="departureAgencyId", type="integer"),
                        *@OA\Property(property="returnAgencyId", type="integer"),
                        *@OA\Property(property="customerQuotationId", type="integer"),
                        *@OA\Property(property="stateId", type="integer"),
                        *@OA\Property(property="city", type="string"),
                        *@OA\Property(property="postalCode", type="integer"),
                        *@OA\Property(property="productName", type="string"),
                        *@OA\Property(property="amount", type="integer"),
                        *@OA\Property(property="quantity", type="integer"),
                        *@OA\Property(property="paymentTypt", type="string"),
                        *@OA\Property(property="cardType", type="string"),
                        *@OA\Property(property="costReason", type="string"),
                        *@OA\Property(property="stripeTransactionDetails", type="string"),
                        *@OA\Property(property="type", type="string")
        *),
        *         @OA\MediaType(
        *            mediaType="multipart/form-data",
        *            @OA\Schema(
        *               type="object",
        *               required={"userId","vehicleCategoryId","vehicleTypeId","agencyVehicleId","agencyId","organizationId","agencyTariffPackageId","bookedOn","bookingstartDate","bookingendDate","status","paymentGateWayId","source","departureAgencyId","returnAgencyId","customerQuotationId","stateId","city","postalCode","productName","amount","quantity","paymentTypt","cardType","costReason","stripeTransactionDetails","type"},
                        *@OA\Property(property="userId", type= "integer"),
                        *@OA\Property(property="vehicleCategoryId", type= "integer"),
                        *@OA\Property(property="vehicleTypeId", type= "integer"),
                        *@OA\Property(property="agencyVehicleId", type= "integer"),
                        *@OA\Property(property="agencyId", type= "integer"),
                        *@OA\Property(property="organizationId", type= "integer"),
                        *@OA\Property(property="agencyTariffPackageId", type= "integer"),
                        *@OA\Property(property="bookedOn" , type= "string"),
                        *@OA\Property(property="bookingstartDate" , type="string"),
                        *@OA\Property(property="bookingendDate", type= "string"),
                        *@OA\Property(property="status", type="string"),
                        *@OA\Property(property="paymentGateWayId", type="integer"),
                        *@OA\Property(property="source", type="string"),
                        *@OA\Property(property="departureAgencyId", type="integer"),
                        *@OA\Property(property="returnAgencyId", type="integer"),
                        *@OA\Property(property="customerQuotationId", type="integer"),
                        *@OA\Property(property="stateId", type="integer"),
                        *@OA\Property(property="city", type="string"),
                        *@OA\Property(property="postalCode", type="integer"),
                        *@OA\Property(property="productName", type="string"),
                        *@OA\Property(property="amount", type="integer"),
                        *@OA\Property(property="quantity", type="integer"),
                        *@OA\Property(property="paymentTypt", type="string"),
                        *@OA\Property(property="cardType", type="string"),
                        *@OA\Property(property="costReason", type="string"),
                        *@OA\Property(property="stripeTransactionDetails", type="string"),
                        *@OA\Property(property="type", type="string")
        *            ),
        *        ),
        *    ),
        *      @OA\Response(
        *          response=201,
        *          description="Customer booking created successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=200,
        *          description="Customer booking created successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=422,
        *          description="Unprocessable Entity",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        *      security={{"bearer_token":{}}} 
        * )
        */
    public function create(Request $request){
        try {
            $input = $request->all();
            
            $validator = Validator::make($input,[ 
                "userId"=> "required|integer|max:255",
                "vehicleCategoryId"=> "required|integer|max:255",
                "vehicleTypeId"=> "required|integer|max:255",
                "agencyVehicleId"=> "required|integer|max:255",
                "agencyId"=> "required|integer|max:255",
                "organizationId"=> "required|integer|max:255",
                "agencyTariffPackageId"=> "required|integer|max:255",
                "bookedOn" => "required|string|max:55",
                "bookingstartDate"=> "required|string",
                "bookingendDate"=> "required|string",
                "status"=> "required|string|max:255",
                "paymentGateWayId"=>"required|integer|max:255",
                "source"=>"required|string|max:255",
                "departureAgencyId"=> "required|integer|max:255",
                "returnAgencyId"=> "required|integer|max:255",
                "customerQuotationId"=>"required|integer|max:255",
                "stateId"=>"required|integer",
                "city"=> "required|string|max:255",
                "postalCode"=>"required|integer|max:255",
                "productName"=> "required|string|max:255",
                "amount"=> "required|integer",
                "quantity"=> "required|integer",
                "paymentTypt"=> "required|string",
                "cardType"=> "required|string",
                "costReason"=>"required|string",
                "stripeTransactionDetails"=> "required|string",
                "type"=> "required|string",
            ]);
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" => $validator->errors(),
                ]); 
            }
            $customerBookings = CustomerBookings::create($input);
            return response()->json([
                "success" => true,
                "message" => "Customer booking created successfully.",
                "data" => $customerBookings
            ]);
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
    
        }
    }
      /**
    * @OA\Get(
    * path="/api/list_customerBooking",
    * operationId="Customer Booking List.",
    * tags={"Customer-Booking"},
    * summary="Customer Booking List",
    * description="Customer booking list here",
    *      @OA\Response(
    *          response=201,
    *          description="Customer Booking List.",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=200,
    *          description="Customer Booking List.",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=422,
    *          description="Unprocessable Entity",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    *     security={{"bearer_token":{}}} 
    * )
    */
    public function list(Request $request){
        try{
            $input = $request->all();
        
            $query = CustomerBookings::query();
            // if(isset($input['searchKey']) && isset($input['searchVal']) && !empty($input['searchKey']) && !empty($input['searchVal'])){
            //     $query->where($request->input('searchKey'), 'like', '%' . $request->input('searchVal') . '%');
            // }
        
            // if (isset($input['sortBy']) && !empty($input['sortBy'])) {
            //     $query->orderBy($input['sortBy'], $input['orderBy']);
            // }
            // if(isset($input['page']) && !empty($input['page']) && isset($input['limit']) && !empty($input['limit'])){
            //     $count=$query->count();
            //     $input['page']= (floor($count/$input['limit']))-1;
            //     $query->offset($input['page'])->limit($input['limit'])->get();
            // }
            $customerBookings= $query->get();
            if(!$customerBookings->isEmpty()){
                return response()->json([
                    "success" => true,
                    "message" => "Customer Bookings List.",
                    "data" => $customerBookings,
                    "total_count"=>count($customerBookings),
                ]); 
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    } 
     /**
     * @OA\Get(
     *      path="/api/customerBooking/{id}",
     *      operationId="Customer Booking Find",
     *      tags={"Customer-Booking"},
     *      summary="Get Customer Booking",
     *      description="Returns customer booking data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Customer booking Id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Customer Booking Data",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Customer Booking Data"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={{"bearer_token":{}}} 
     * )
     */
    public function find($id){
        try{
            $customerBookings = CustomerBookings::find($id);
            $customerBookings = compact('customerBookings');
            if(!empty($customerQuotation['customerQuotation'])){
                return response()->json([
                    "success" => true,
                    "message" => "Customer quotation Data",
                    "data" => $customerQuotation,
                    "total_count"=>count($customerQuotation),
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    }
     /**
        * @OA\Post(
        * path="/api/update_customerBooking",
        * operationId="Update Customer Booking",
        * tags={"Customer-Booking"},
        * summary="Update Customer Booking",
        * description="Update Customer Booking here",
        *     @OA\RequestBody(
        *         @OA\JsonContent(
        *         required={"userId","vehicleCategoryId","vehicleTypeId","agencyVehicleId","agencyId","organizationId","agencyTariffPackageId","bookedOn","bookingstartDate","bookingendDate","status","paymentGateWayId","source","departureAgencyId","returnAgencyId","customerQuotationId","stateId","city","postalCode","productName","amount","quantity","paymentTypt","cardType","costReason","stripeTransactionDetails","type"},
                        *@OA\Property(property="userId", type= "integer"),
                        *@OA\Property(property="vehicleCategoryId", type= "integer"),
                        *@OA\Property(property="vehicleTypeId", type= "integer"),
                        *@OA\Property(property="agencyVehicleId", type= "integer"),
                        *@OA\Property(property="agencyId", type= "integer"),
                        *@OA\Property(property="organizationId", type= "integer"),
                        *@OA\Property(property="agencyTariffPackageId", type= "integer"),
                        *@OA\Property(property="bookedOn" , type= "string"),
                        *@OA\Property(property="bookingstartDate" , type="string"),
                        *@OA\Property(property="bookingendDate", type= "string"),
                        *@OA\Property(property="status", type="string"),
                        *@OA\Property(property="paymentGateWayId", type="integer"),
                        *@OA\Property(property="source", type="string"),
                        *@OA\Property(property="departureAgencyId", type="integer"),
                        *@OA\Property(property="returnAgencyId", type="integer"),
                        *@OA\Property(property="customerQuotationId", type="integer"),
                        *@OA\Property(property="stateId", type="integer"),
                        *@OA\Property(property="city", type="string"),
                        *@OA\Property(property="postalCode", type="integer"),
                        *@OA\Property(property="productName", type="string"),
                        *@OA\Property(property="amount", type="integer"),
                        *@OA\Property(property="quantity", type="integer"),
                        *@OA\Property(property="paymentTypt", type="string"),
                        *@OA\Property(property="cardType", type="string"),
                        *@OA\Property(property="costReason", type="string"),
                        *@OA\Property(property="stripeTransactionDetails", type="string"),
                        *@OA\Property(property="type", type="string")
        *),
        *         @OA\MediaType(
        *            mediaType="multipart/form-data",
        *            @OA\Schema(
        *               type="object",
        *               required={"userId","vehicleCategoryId","vehicleTypeId","agencyVehicleId","agencyId","organizationId","agencyTariffPackageId","bookedOn","bookingstartDate","bookingendDate","status","paymentGateWayId","source","departureAgencyId","returnAgencyId","customerQuotationId","stateId","city","postalCode","productName","amount","quantity","paymentTypt","cardType","costReason","stripeTransactionDetails","type"},
                        *@OA\Property(property="userId", type= "integer"),
                        *@OA\Property(property="vehicleCategoryId", type= "integer"),
                        *@OA\Property(property="vehicleTypeId", type= "integer"),
                        *@OA\Property(property="agencyVehicleId", type= "integer"),
                        *@OA\Property(property="agencyId", type= "integer"),
                        *@OA\Property(property="organizationId", type= "integer"),
                        *@OA\Property(property="agencyTariffPackageId", type= "integer"),
                        *@OA\Property(property="bookedOn" , type= "string"),
                        *@OA\Property(property="bookingstartDate" , type="string"),
                        *@OA\Property(property="bookingendDate", type= "string"),
                        *@OA\Property(property="status", type="string"),
                        *@OA\Property(property="paymentGateWayId", type="integer"),
                        *@OA\Property(property="source", type="string"),
                        *@OA\Property(property="departureAgencyId", type="integer"),
                        *@OA\Property(property="returnAgencyId", type="integer"),
                        *@OA\Property(property="customerQuotationId", type="integer"),
                        *@OA\Property(property="stateId", type="integer"),
                        *@OA\Property(property="city", type="string"),
                        *@OA\Property(property="postalCode", type="integer"),
                        *@OA\Property(property="productName", type="string"),
                        *@OA\Property(property="amount", type="integer"),
                        *@OA\Property(property="quantity", type="integer"),
                        *@OA\Property(property="paymentTypt", type="string"),
                        *@OA\Property(property="cardType", type="string"),
                        *@OA\Property(property="costReason", type="string"),
                        *@OA\Property(property="stripeTransactionDetails", type="string"),
                        *@OA\Property(property="type", type="string")
        *            ),
        *        ),
        *    ),
        *      @OA\Response(
        *          response=201,
        *          description="Customer booking has been updated successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=200,
        *          description="Customer booking has been updated successfully.",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=422,
        *          description="Unprocessable Entity",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        *      security={{"bearer_token":{}}} 
        * )
        */
    public function update(Request $request, $id)  
    {   try{
            $request = $request->all();
            $validator=Validator::make($request, [
                "userId"=> "required|integer|max:255",
                    "vehicleCategoryId"=> "required|integer|max:255",
                    "vehicleTypeId"=> "required|integer|max:255",
                    "agencyVehicleId"=> "required|integer|max:255",
                    "agencyId"=> "required|integer|max:255",
                    "organizationId"=> "required|integer|max:255",
                    "agencyTariffPackageId"=> "required|integer|max:255",
                    "bookedOn" => "required|string|max:55",
                    "bookingstartDate"=> "required|string",
                    "bookingendDate"=> "required|string",
                    "status"=> "required|string|max:255",
                    "paymentGateWayId"=>"required|integer|max:255",
                    "source"=>"required|string|max:255",
                    "departureAgencyId"=> "required|integer|max:255",
                    "returnAgencyId"=> "required|integer|max:255",
                    "customerQuotationId"=>"required|integer|max:255",
                    "stateId"=>"required|integer",
                    "city"=> "required|string|max:255",
                    "postalCode"=>"required|integer|max:255",
                    "productName"=> "required|string|max:255",
                    "amount"=> "required|integer",
                    "quantity"=> "required|integer",
                    "paymentTypt"=> "required|string",
                    "cardType"=> "required|string",
                    "costReason"=>"required|string",
                    "stripeTransactionDetails"=> "required|string",
                    "type"=> "required|string",
            ]);
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" => $validator->errors(),
                ]); 
            }
            $customerBookings= CustomerBookings::find($id);
        
            if(!empty($customerBookings)){
                $customerBookings->update($request);
                return response()->json([
                    "success" => true,
                    "message" => "Customer booking has been updated successfully.",
                    "data" => $customerBookings,
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }
    }  
     /**
     * @OA\Delete(
     *      path="/api/delete_customerBooking/{id}",
     *      operationId="Delete CustomerBooking",
     *      tags={"Customer-Booking"},
     *      summary="Delete Customer Booking",
     *      description="Delete customer booking",
     *      @OA\Parameter(
     *          name="id",
     *          description="CustomerBooking Id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Customer booking data has been deleted successfully.",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={{"bearer_token":{}}} 
     * )
     */ 
    public function delete($id)  
    {   
        try{
            $customerBookings=CustomerBookings::find($id);
            if(!empty($customerBookings)){
                $customerBookings->status='deleted';
                $customerBookings->save();
                return response()->json([
                    "success" => true,
                    "message" => "Customer booking data has been deleted successfully.",
                    "data" => $customerQuotation,
                ]);
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    }
}
