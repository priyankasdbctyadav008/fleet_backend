<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\ResetPasswordRequest;
use Illuminate\Support\Facades\Password;
use App\ApiCode;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Mail\sendEmail;
use Illuminate\Support\Facades\DB;
use App\Models\Organizations;
use App\Models\BusinessOwners;
use App\Http\Controllers\OrganizationsController;
use Illuminate\Support\Facades\Redirect;

class AuthController extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth:api', ['except' => ['reset','login','register','forgot','requestOtp','verifyOtp']]);
   
    }
     
     /**
        * @OA\Post(
        * path="/api/register",
        * operationId="Register",
        * tags={"Auth"},
        * summary="User Register",
        * description="User Register here",
        *     @OA\RequestBody(
        *         @OA\JsonContent(),
        *         @OA\MediaType(
        *            mediaType="multipart/form-data",
        *            @OA\Schema(
        *               type="object",
        *               required={"first_name","last_name","email","password","phone","name","address","city","postal_code","prefferedCurrencies","state_id","country_id","LogoUrl"},
                        *@OA\Property(property="first_name", type="string"),
                        *@OA\Property(property="last_name", type="string"),
                        *@OA\Property(property="email", type="string"),
                        *@OA\Property(property="email_verified_at",type="string"),
                        *@OA\Property(property="password", type="string"),
                        *@OA\Property(property="phone", type="string"),
                        *@OA\Property(property="LogoUrl", type="string"),
                        *@OA\Property(property="name", type="string"),
                        *@OA\Property(property="address", type="string"),
                        *@OA\Property(property="city", type="string"),
                        *@OA\Property(property="postalCode", type="integer"),
                        *@OA\Property(property="prefferedCurrencies", type="json"),
                        *@OA\Property(property="role", type="integer"),
                        *@OA\Property(property="stateId", type="integer"),
                        *@OA\Property(property="countryId", type="integer"),
                        *@OA\Property(property="themeColor", type="integer"),
                        *@OA\Property(property="pagecontent_TnC", type="string"),
                        *@OA\Property(property="pageContent_PrivacyPolicy", type="string"),
                        *@OA\Property(property="createdBy", type="integer"),
                        *@OA\Property(property="updatedBy", type="integer"),
                        *@OA\Property(property="status", type="integer")

        *            ),
        *        ),
        *    ),
        *      @OA\Response(
        *          response=201,
        *          description="Register Successfully",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=200,
        *          description="Register Successfully",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=422,
        *          description="Unprocessable Entity",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        * )
        */
    public function register(Request $request){
        
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|max:255',
            'last_name'=> 'required|string|max:255',
            'phone'=> 'required|string|max:18',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'email_verified_at'=> 'string|max:255|nullable',
            'role'=> 'string|max:255|nullable',
            'status'=> 'integer|nullable',
            'createdBy'=> 'integer|nullable',
            'updatedBy'=> 'integer|nullable',  

            // 'name'=> 'required|string|max:255',
            // 'address'=> 'required|string|max:255',
            // 'city'=> 'required|string|max:255',
            // 'postal_code'=> 'required|integer',
            // 'preffered_currencies'=> 'required|json|max:255',
            // 'role'=> 'required|integer|max:255',
            // 'state_id'=> 'required|integer|max:255',
            // 'country_id'=> 'required|integer|max:255',
            // 'theme_color'=> 'integer|nullable',
            // 'pagecontent_TnC'=> 'string|max:255|nullable',
            // 'pagecontent_privacypolicy'=> 'string|max:255|nullable',
            
            //organization
            "LogoUrl"=> "required|string|max:255",
            "name"=> "required|string|max:255",
            "address"=> "required|string|max:255",
            "city"=> "required|string|max:255",
            "postalCode" => "required|string|max:255",
            "themeColor" => "string|max:255",
            "prefferedCurrencies" => "required|json|max:255",
            "pageContent_TnC" => "string|max:255",
            "pageContent_PrivacyPolicy" => "string|max:255",
            "stateId" => "required|integer",
            "countryId" => "required|integer",
            
        ]);
        if ($validator->fails()){
            return response()->json([
                'status' => 'fail',
                'message' => $validator->errors(),
            ]);
        }
        DB::beginTransaction(); // Start the transaction

       try {
            $user = User::create([
                'first_name'=>$request->first_name,
                'last_name'=> $request->last_name,
                'phone'=> $request->phone,
                'email'=> $request->email,
                'password'=> Hash::make($request->password),
                'email_verified_at'=> $request->email_verified_at,
                'role'=> (isset($request->role) && !empty($request->role))?$request->role : 2,
                'status'=> (isset($request->status) && !empty($request->status))?$request->status : 0,
                'createdBy'=> (isset($request->createdBy) && !empty($request->createdBy)) ?$request->createdBy :'1',
                'updatedBy'=> (isset($request->updatedBy) && !empty($request->updatedBy)) ?$request->updatedBy :'1',

                // 'name'=> $request->name,
                // 'address'=> $request->address,
                // 'city'=> $request->city,
                // 'postal_code'=> $request->postal_code,
                // 'preffered_currencies'=> $request->preffered_currencies,
                // 'role'=> $request->role,
                // 'state_id'=> $request->state_id,
                // 'country_id'=> $request->country_id,
                // 'theme_color'=> $request->theme_color,
                // 'pagecontent_TnC'=> $request->pagecontent_TnC,
                // 'pagecontent_privacypolicy'=> $request->pagecontent_privacypolicy ,

                //organization
                
                
            ]);
            
            if($user && $user->role==2){
                $organization = Organizations::create([
                    "LogoUrl"=>$request->LogoUrl,
                    "name"=>$request->name,
                    "userId"=>$user->id,
                    "address"=> $request->address,
                    "city"=> $request->city,
                    "postalCode" => $request->postalCode,
                    "themeColor" => $request->themeColor,
                    "prefferedCurrencies" => $request->prefferedCurrencies,
                    "pageContent_TnC" => $request->pageContent_TnC,
                    "pageContent_PrivacyPolicy" => $request->pageContent_PrivacyPolicy,
                    "stateId" => $request->stateId,
                    "countryId" => $request->countryId
                ]);
            }
            DB::commit(); // Commit the transaction if all operations are successful
            if($user && $organization && $user->role==2) {
                $buiness_owners = BusinessOwners::create([
                    'UserId'=>$user->id,
                    'organizationId'=>$organization->id,
                ]);

            }
        } catch (Exception $e) {
            DB::rollBack(); 
            return response()->json([
                'status' => 'false',
                'message' => $e,
            ]);
        }
       
        
        if( $user){
            return response()->json([
            'status' => 'success',
            'message' => 'Register Successfully',
            'user' => $user,
        ]);
        }else{
            return response()->json([
                "success" => false,
                "message" => 'Fail To Register',
            ]); 
        }
        
    }
      /**
        * @OA\Post(
        * path="/api/login",
        * operationId="Login",
        * tags={"Auth"},
        * summary="User Login",
        * description="User Login here",
        *     @OA\RequestBody(
        *         @OA\JsonContent(
        *         ),
        *         @OA\MediaType(
        *            mediaType="multipart/form-data",
        *            @OA\Schema(
        *               type="object",
        *               required={"email","password"},
        *                @OA\Property(property="email", type="string"),
        *                @OA\Property(property="password", type="string")
        *            ),
        *        ),
        *    ),
        *      @OA\Response(
        *          response=201,
        *          description="Login Successfully",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=200,
        *          description="Login Successfully",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=422,
        *          description="Unprocessable Entity",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        * )
        */
    public function login(Request $request)
    {   
        try{
            // $request->validate([
            //     'email' => 'required|string|email',
            //     'password' => 'required|string',
            // ]);
            $validator=Validator::make($request->all(), [
                'email' => 'required|string|email',
                'password' => 'required|string',
            ]);
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" => $validator->errors(),
                ]); 
            }

            $credentials = $request->only('email', 'password');
                
            $token = Auth::attempt($credentials);
            if (!$token) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Unauthorized',
                ], 401);
            }

            $user = Auth::user();
            
            if($user->role==2){
                $OrganizationsController = new OrganizationsController();
                $data=$OrganizationsController->getUserOrganization($user->id);
                $data=json_decode($data->getContent(), true);
            }
        
            return response()->json([
                    'status' => 'success',
                    'message'=>"Login Successfully",
                    'user' => $user,
                    'organizations_details' =>isset($data) && $data['success']  ? $data['data']:'',
                    'authorisation' => [
                        'token' => $token,
                        'type' => 'bearer',
                    ]
                ]);
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    

    }
   
    /**
    * @OA\Post(
    * path="/api/forgot_password",
    * operationId="Forgot Password",
    * tags={"Auth"},
    * summary="Forgot Password",
    * description="Forgot Password here",
    *     @OA\RequestBody(
    *         @OA\JsonContent(),
    *         @OA\MediaType(
    *            mediaType="multipart/form-data",
    *            @OA\Schema(
    *               type="object",
    *               required={"email"},
    *                @OA\Property(property="email", type="string"),
    *            ),
    *        ),
    *    ),
    *      @OA\Response(
    *          response=201,
    *          description="Reset password link sent on your email id.",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=200,
    *          description="Reset password link sent on your email id.",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=422,
    *          description="Unprocessable Entity",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    * )
    */
    public function forgot(Request $request) {
        try{
            $request->validate([
                'email' => 'required|string|email',
                //'password' => 'required|string',
            ]);
            $credentials = $request->only('email');
            //https://dev.to/chandreshhere/laravel-7-forgot-password-apis-using-inbuilt-laravel-methods-4ij0
        // $credentials = request()->validate(['email' => 'required|email']);

            Password::sendResetLink($credentials);
            return response()->json([
                'success' => true,
                'message' => 'Reset password link sent on your email id.',
            ]);
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }     
     
    }

    // public function reset(ResetPasswordRequest $request) {
    //     $reset_password_status = Password::reset($request->validated(), function ($user, $password) {
    //         $user->password = $password;
    //         $user->save();
    //     });

    //     if ($reset_password_status == Password::INVALID_TOKEN) {
    //         return $this->respondBadRequest(ApiCode::INVALID_RESET_PASSWORD_TOKEN);
    //     }

    //     return $this->respondWithMessage("Password has been successfully changed");
    // }
     
    /**
    * @OA\Post(
    * path="/api/reset_password",
    * operationId="Reset Password",
    * tags={"Auth"},
    * summary="Reset Password",
    * description="Reset Password here",
    *     @OA\RequestBody(
    *         @OA\JsonContent(),
    *         @OA\MediaType(
    *            mediaType="multipart/form-data",
    *            @OA\Schema(
    *               type="object",
    *               required={"email","token","password"},
    *                @OA\Property(property="email", type="string"),
    *                @OA\Property(property="token", type="string"),
    *                @OA\Property(property="password", type="string"),
    *            ),
    *        ),
    *    ),
    *      @OA\Response(
    *          response=201,
    *          description="Password has been successfully changed.",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=200,
    *          description="Password has been successfully changed.",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=422,
    *          description="Unprocessable Entity",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    * )
    */
    public function reset(Request $request) {
        try{
            // $credentials = request()->validate([
            //     'email' => 'required|email',
            //     'token' => 'required|string',
            //     'password' => 'required|string|confirmed'
            // ]);
            
        $credentials = $request->only('email', 'password','token');
            $reset_password_status = Password::reset($credentials, function ($user, $password) {
                $user->password = Hash::make($password);
                $user->save();
            });
   
            //   print_r(Password::INVALID_TOKEN);die();
            if ($reset_password_status == Password::INVALID_TOKEN) {
                // return response()->json(["success" => false,"message" => "Invalid token provided"], 400);
                return Redirect::back();
               
            }
                 
            // return response()->json(["success"=> "true","message" => "Password has been successfully changed"]);
            return redirect('http://192.168.0.56:4200/auth/signin');
        }catch (Exception $e) {
            //  return response()->json([
            //     "success" => false,
            //     "message" => $e,
            // ]);
            return Redirect::back();
        }    
    }
    /**
    * @OA\Post(
    * path="/api/resendOtp",
    * operationId="Request OTP",
    * tags={"Auth"},
    * summary="Request OTP",
    * description="Request OTP here",
    *     @OA\RequestBody(
    *         @OA\JsonContent(),
    *         @OA\MediaType(
    *            mediaType="multipart/form-data",
    *            @OA\Schema(
    *               type="object",
    *               required={"email"},
    *                @OA\Property(property="email", type="string"),
    *            ),
    *        ),
    *    ),
    *      @OA\Response(
    *          response=201,
    *          description="OTP sent successfully",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=200,
    *          description="OTP sent successfully",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=422,
    *          description="Unprocessable Entity",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    * )
    */
    public function requestOtp(Request $request){
        try{
            $otp = rand(1000,9999);
            //Log::info("otp = ".$otp);
            $user = User::where('email','=',$request->email)->update(['email_otp' => $otp]);

            if($user){
                $mail_details = [
                    'subject' => 'Fleet Application OTP',
                    'body' => 'Your Fleet Application Email Verification OTP Is : '. $otp
                ];
            
                Mail::to($request->email)->send(new sendEmail($mail_details));
                return response()->json([
                    'success' => true,
                    'message' => 'OTP sent successfully.',
                ]);
                //return response(["status" => 200, "message" => "OTP sent successfully"]);
            }
            else{
                return response()->json([
                    'success' => false,
                    'message' => 'Invalid',
                ]);
            // return response(["status" => 401, 'message' => 'Invalid']);
            }
         }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    }
    /**
    * @OA\Post(
    * path="/api/verifyOtp",
    * operationId="VerifyOtp",
    * tags={"Auth"},
    * summary="Verify Otp",
    * description="Verify Otp here",
    *     @OA\RequestBody(
    *         @OA\JsonContent(),
    *         @OA\MediaType(
    *            mediaType="multipart/form-data",
    *            @OA\Schema(
    *               type="object",
    *               required={"email","otp"},
    *                @OA\Property(property="email", type="string"),
    *                @OA\Property(property="otp", type="string")
    *            ),
    *        ),
    *    ),
    *      @OA\Response(
    *          response=201,
    *          description="Otp verified successfully.",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=200,
    *          description="Otp verified successfully.",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(
    *          response=422,
    *          description="Unprocessable Entity",
    *          @OA\JsonContent()
    *       ),
    *      @OA\Response(response=400, description="Bad request"),
    *      @OA\Response(response=404, description="Resource Not Found"),
    * )
    */
    public function verifyOtp(Request $request){
        try{
            $user  = User::where([['email','=',$request->email],['email_otp','=',$request->otp]])->first();
            if($user){
                auth()->login($user, true);
                User::where('email','=',$request->email)->update(['email_otp' => null,'Isemail_verified'=>'1']);
                // $accessToken = auth()->user()->createToken('authToken')->accessToken;
                return response()->json([
                    'success' => true,
                    'message' => 'Otp verified successfully.',
                ]);
                //return response(["status" => 200, "message" => "Success", 'user' => auth()->user()]);
            }
            else{
                return response()->json([
                    'success' => false,
                    'message' => 'Invalid',
                ]);
                //return response(["status" => 401, 'message' => 'Invalid']);
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    }
      /**
        * @OA\Get(
        * path="/api/list_currency",
        * operationId="Currency",
        * tags={"Auth"},
        * summary="Currency List",
        * description="Currency list here",
        *      @OA\Response(
        *          response=201,
        *          description="Currency list",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=200,
        *          description="Currency list",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=422,
        *          description="Unprocessable Entity",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        * )
        */
    public function currency_list(){
        try{
            $common= DB::table('currency')->get();
            if(!$common->isEmpty()){
                return response()->json([
                    "success" => true,
                    "message" => "Currency List.",
                    "data" => $common,
                    "total_count"=>count($common),
                ]); 
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }
    }
    
     /**
        * @OA\Get(
        * path="/api/country",
        * operationId="Country",
        * tags={"Auth"},
        * summary="Country List",
        * description="Country list here",
        *      @OA\Response(
        *          response=201,
        *          description="Register Successfully",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=200,
        *          description="Register Successfully",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(
        *          response=422,
        *          description="Unprocessable Entity",
        *          @OA\JsonContent()
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        * )
        */
    public function country_list(){
        try{
            $country= DB::table('country_msts')->get();
            
            if(!$country->isEmpty()){
                return response()->json([
                    "success" => true,
                    "message" => "Country List.",
                    "data" => $country,
                    "total_count"=>count($country),
                ]); 
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    }
     /**
     * @OA\Get(
     *      path="/api/states/{id}",
     *      operationId="States",
     *      tags={"Auth"},
     *      summary="Get States",
     *      description="Returns states data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Country Id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function state_list($countryId){
        try{
            $states= DB::table('states_msts')->where("countryId",$countryId)->get();
            if(!$states->isEmpty()){
                return response()->json([
                    "success" => true,
                    "message" => "States List.",
                    "data" => $states,
                    "total_count"=>count($states),
                ]); 
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    }
    public function getUserByEmail($email){
        try{
            $user=User::where('email', $email)->get();
            if(count($user)>0){
                return response()->json([
                    "success" => true,
                    "message" => "User data",
                    "data" => $user,
                    "total_count"=>count($user),
                ]); 
            }else{
                return response()->json([
                    "success" => false,
                    "message" => "Data Not Found.",
                ]); 
            }
        }catch (Exception $e) {
             return response()->json([
                "success" => false,
                "message" => $e,
            ]);
        }    
    }
}