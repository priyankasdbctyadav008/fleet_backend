<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuotationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            "customerId"=> "required|integer|max:255",
            "startDate"=> "string|max:255",
            "endDate"=> "string|max:255",
            "source"=> "required|integer|max:255",
            "vehicleCategoryId"=> "required|integer|max:255",
            "vehicleTypeId"=> "required|integer|max:255",
            "amount"=> "required|integer",
            "departureAgencyId"=> "required|integer|max:255",
            "returnAgencyId"=> "required|integer|max:255",
            "quotationExpiryDate"=> "string",
            "status"=> "integer|max:255",
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'Email is required!',
            'name.required' => 'Name is required!',
            'password.required' => 'Password is required!'
        ];
    }
}
