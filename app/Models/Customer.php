<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;
    protected $table = 'customer';
    protected $fillable = [
    'firstName',
    'lastName',
    'email',
    'phone',
    'customer_email',
    'customer_phone',
    'address1',
    'city',
    'stateId',
    'countryId',
    'postalCode',
    'licenseNo',
    'dob',
    'licenseCreationDate',
    'licenseImgFrontUrl',
    'licenseImgBackUrl',
    'customerCompanyId',
    'createdBy',
    'updatedBy',
    'status',
    ];
} 
