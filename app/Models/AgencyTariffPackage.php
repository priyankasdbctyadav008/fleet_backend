<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AgencyTariffPackage extends Model
{
    use HasFactory;
    protected $fillable = [  
        "packageName",
        "isStartMonday",
        "isStartTuesday",
        "isStartWednesday",
        "isStartThursday",
        "isStartFriday",
        "isStartSaturday",
        "isStartSunday",
        "packageDurationInDays",
        "mileageIncluded",
        "mileageUnit",
        "isUnlimitedMileage",
        "vehiclesCategoryId",
        "vehicleTypeId",
        "amount",
        "status",
        "createdBy",
        "updatedBy"
    ];
}
