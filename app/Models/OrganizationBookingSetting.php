<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrganizationBookingSetting extends Model
{
    use HasFactory;
    protected $fillable = [
        "organizationId",
        "informAboutRentalBeforeDays",
        "minRentalPeriodInDays",
        "rentalReserveBeforeMonths",
        "allowOverBooking",
        "allowEditPriceManually",
        "allowPastDateReservation",
        "isAutoGenerateBookingRef",
        "authorizeOutsideReservationArea",
        "createdBy",
        "updatedBy"
    ];
}
