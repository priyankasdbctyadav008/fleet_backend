<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Agencies extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'noOfRegisteredVehicles',
        'agencyCode',
        'postalCode',
        'address1',
        'city',
        'paymentGatewaySecretKey',
        'paymentGatewayPublickey',
        'agencyAvailbility',
        'countryId',
        'stateId',
        'publicName',
        'stripeSecretKey',
        'stripePublickey',
        'ispaymentgatewayEnabled',
        'organizationId',                  
        'status',
        'createdBy',
        'updatedBy'
    ];
}
