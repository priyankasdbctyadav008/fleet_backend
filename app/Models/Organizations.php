<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Organizations extends Model
{
    use HasFactory;
    protected $fillable = [
        "LogoUrl",
        "userId",
        "name",
        "address",
        "city",
        "postalCode" ,
        "themeColor",
        "prefferedCurrencies",
        "pageContent_TnC",
        "pageContent_PrivacyPolicy",
        "stateId",
        "countryId",
    ];
}
