<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerBooking extends Model
{
    use HasFactory;
    protected $fillable = [
        "userId",
        "vehicleCategoryId",
        "vehicleTypeId",
        "agencyVehicleId",
        "agencyId",
        "organizationId",
        "agencyTariffPackageId",
        "bookedOn",
        "bookingstartDate",
        "bookingendDate",
        "status",
        "paymentGateWayId",
        "source",
        "departureAgencyId",
        "returnAgencyId",
        "customerQuotationId",
        "stateId",
        "city",
        "postalCode",
        "productName",
        "amount",
        "quantity",
        "paymentTypt",
        "cardType",
        "costReason",
        "stripeTransactionDetails",
        "type"
    ];
}
