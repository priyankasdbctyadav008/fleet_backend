<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerCompany extends Model
{
    use HasFactory;
    protected $table = 'customer_company';
    protected $fillable = [
        'name',
        'email',
        'phone',
        'sirenno',
        'createdBy',
        'updatedBy',
        'created_at',
        'updated_at',
        'status',
    ];
}
