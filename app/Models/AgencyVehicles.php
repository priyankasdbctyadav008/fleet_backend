<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AgencyVehicles extends Model
{
    use HasFactory;
    protected $fillable = [
        "vehicleCategoryId",
        "vehicleTypeId",
        "vehicleBrandId",
        "model",
        "noOfDoors",
        "agencyId",
        "yearOfRegistration",
        "motorization",
        "registrationNo",
        "gearBox",
        "noOfSeats",
        "mileage",
        "vehicleFeatures",
        "ratePerDay",
        "miniMumDay",
        "maxiMumDay",
        "image_url",
        "status",
        "lastInspectionDate",
        "lastRevisionDate",
        "createdBy",
        "updatedBy",
        "image_url1",
        "image_url2",
        "image_url3",
        "image_url4",
        "image_url5",
        "image_url6",
    ];
}
