<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReservationSettings extends Model
{
    use HasFactory;
    protected $fillable = [
        'userId',
        'informAboutRentalBeforeDays',
        'rentalReserveBeforeMonths',
        'minRentalPeriodInDays',
        'allowOverBooking',
        'authorizeOutsideReservationArea',
        'allowPastDateReservation',
        'isAutoGenerateBookingRef',
        'createdBy',
        'updatedBy',
    ];
}
