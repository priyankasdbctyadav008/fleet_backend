<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BusinessOwners extends Model
{
    use HasFactory;
    protected $fillable = [
    'Id',
    'UserId',
    'organizationId',
    'createdBy',
    'updatedBy',
    'created_at',
    'updated_at'];
}
