<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerQuotation extends Model
{
    use HasFactory;
    protected $fillable = [
        "customerId",
        "startDate",
        "endDate",
        "source",
        "vehicleCategoryId",
        "vehicleTypeId",
        "amount",
        "departureAgencyId",
        "returnAgencyId",
        "quotationExpiryDate",
        "status",
        "created_at",
        "updated_at"
    ];
}
