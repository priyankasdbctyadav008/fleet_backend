<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModuleMst extends Model
{
    use HasFactory;
    protected $table = 'module_mst';
    protected $fillable = [
        'name'
    ];
}
